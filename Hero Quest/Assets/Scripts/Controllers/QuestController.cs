﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

// Controla si un personaje tiene una misión, si puede darla o no y como responde a los posibles estados de esta.
public class QuestController : MonoBehaviour
{
    // Varaibles necesarias para obtener la misión
    public string questTypeName;
    // Misión
    public Quest quest;

    // Estado actual de la misión.
    public enum QuestState { Completed, ReadyToComplete, Incomplete, Startable, NotStartable };
    private QuestState currentQuestState;

    // Define el flujo de conversación mediante currentQueststate
    private int flow;

    // Contiene la base de datos de la conversación de la misión.
    public ConversationDatabase conversationDatabase;
    // Contiene el controlador de la conversación.
    public QuestConversationController questConversationController;
    // Contiene el personaje que tiene la misión.
    public CharacterStats characterStats;
    // Si no tiene personaje se mostrará este nombre.
    public string characterName;

    // Simbolos de exclamación e interrogación
    public GameObject exclamation;
    public GameObject interrogation;

    // Variables para el minimapa
    public Image minimapIcon;
    public Sprite exclamationSprite, interrogationSprite, friendlySprite;

    // Puede dar o recibir la misión?
    public bool isGiverQuest;
    public bool isGetterQuest;

    // Al finalizar se activa otra quest si no es null.
    public GameObject nextQuestController;

    // Controla si se ha inicializado o no el gameobject.
    private bool started = false;

    // La conversación ha finalizado?
    IEnumerator ConversationFinished;

    private void Start()
    {
        // Obtenemos la misión
        quest = QuestFactory.GetQuestByName(questTypeName) as Quest;

        // Verificamos el estado de la misión.
        CheckQuestState();

        // Añadimos que se actualice cuando una misión se complete
        EventManager.OnQuestCompleted += CheckQuestState;

        started = true;

    }

    private void OnEnable()
    {
        if (started)
            CheckQuestState();
    }

    // Verifica si una misión es o no cogible o entregable y su estado actual.
    public void CheckQuestState()
    {
        // Ha completado la quest?
        foreach (Quest completedQuest in GameManager.instance.Profile.CompletedQuests)
        {
            if (completedQuest.idQuest == quest.idQuest)
            {

                //Debug.Log("Completed quest: " + quest.EnTitle);
                currentQuestState = QuestState.Completed;
                UpdateSimbols();
                UpdateFlow();
                FinishInteraction();
                return;

            }
        }

        // Tiene la quest?
        foreach (Quest currentQuest in GameManager.instance.Profile.CurrentQuests)
        {
            if (currentQuest.idQuest == quest.idQuest)
            {

                // Si la tiene, està lista para completarse?
                if (currentQuest.completed)
                {
                    if (isGetterQuest)
                    {
                        currentQuestState = QuestState.ReadyToComplete;
                        UpdateSimbols();
                        UpdateFlow();
                        return;
                    }
                    else
                    {
                        currentQuestState = QuestState.Incomplete;
                        UpdateSimbols();
                        UpdateFlow();
                        return;
                    }
                }
                else
                {
                    currentQuestState = QuestState.Incomplete;
                    UpdateSimbols();
                    UpdateFlow();
                    return;
                }
            }
        }

        // Puede coger la quest?
        bool canTakeQuest = true;

        if (quest.idQuests != null)
            // Verificamos si las misiones necesarias de ser completadas han sido completadas.
            foreach (int idQuest in quest.idQuests)
            {
                bool iscompletedQuest = false;

                foreach (Quest completedQuest in GameManager.instance.Profile.CompletedQuests)
                {
                    if (idQuest == completedQuest.idQuest)
                    {
                        iscompletedQuest = true;
                    }
                }

                // La quest està completada?
                if (!iscompletedQuest)
                {
                    canTakeQuest = false;
                    break;
                }
            }

        if (canTakeQuest)
        {
            if (isGiverQuest)
            {
                //Debug.Log("Puede coger la misión. " + quest.EnTitle);
                currentQuestState = QuestState.Startable;
                UpdateSimbols();
            }
            else
            {
                currentQuestState = QuestState.NotStartable;
                UpdateSimbols();
            }
        }
        else
        {
            //Debug.Log("No puede coger la misión.");
            currentQuestState = QuestState.NotStartable;
            UpdateSimbols();
        }
        // Actualizamos el flujo
        UpdateFlow();
    }

    // Actualizamos simbolos físicos de exclamación, interrogación o si no tiene ninguno.
    private void UpdateSimbols()
    {
        switch (currentQuestState)
        {
            case QuestState.Completed:
                exclamation.SetActive(false);
                interrogation.SetActive(false);
                UpdateMinimapIcon(friendlySprite);
                break;
            case QuestState.ReadyToComplete:
                exclamation.SetActive(false);
                interrogation.SetActive(true);
                UpdateMinimapIcon(interrogationSprite);
                break;
            case QuestState.Incomplete:
                exclamation.SetActive(false);
                interrogation.SetActive(false);
                UpdateMinimapIcon(friendlySprite);
                break;
            case QuestState.Startable:
                exclamation.SetActive(true);
                interrogation.SetActive(false);
                UpdateMinimapIcon(exclamationSprite);
                break;
            case QuestState.NotStartable:
                exclamation.SetActive(false);
                interrogation.SetActive(false);
                UpdateMinimapIcon(friendlySprite);
                break;
        }

    }

    // Actualiza el icono del minimapa del personaje de misión con el sprite pasado
    private void UpdateMinimapIcon(Sprite newSprite)
    {
        if (minimapIcon != null)
            minimapIcon.sprite = newSprite;
    }

    // Actualiza el flujo
    public void UpdateFlow()
    {
        if (currentQuestState == QuestState.Completed)
            flow = 0;
        else if (currentQuestState == QuestState.ReadyToComplete)
            flow = 1;
        else if (currentQuestState == QuestState.Incomplete)
            flow = 2;
        else if (currentQuestState == QuestState.Startable)
            flow = 3;
        else if (currentQuestState == QuestState.NotStartable)
            flow = 4;
    }

    // Interactua con el personaje y empieza la conversación
    public void Interact()
    {

        QuestConversation conversation = null;

        // Obtenemos la conversación correcta.
        switch (flow)
        {
            case 0:
                conversation = conversationDatabase.completedQuestConversation;
                break;
            case 1:
                conversation = conversationDatabase.readyToCompleteQuestConversation;
                break;
            case 2:
                conversation = conversationDatabase.incompleteQuestConversation;
                break;
            case 3:
                conversation = conversationDatabase.startQuestConversation;
                break;
            case 4:
                conversation = conversationDatabase.NotStartableQuestConversation;
                break;
        }

        // Empezamos la conversación
        StartQuestConversation(conversation);
    }

    // Empieza una conversación
    private void StartQuestConversation(QuestConversation conversation)
    {
        // Es una conversación importante, el personaje no podrá moverse mientras se esté conversando.
        FindObjectOfType<InputPlayerController>().canMoveCharacter = false;

        // Activamos el QuestconversationController y le decimos que no ha acabado la conversación.
        questConversationController.gameObject.SetActive(true);
        questConversationController.isConversationFinished = false;

        // Empezamos la conversación, si el personaje de misión tiene characterstats se pasará el nombre de este, sino se usará el nombre alternativo.
        if (characterStats != null)
            questConversationController.StartConversation(characterStats.nameCharacter, conversation.sentences);
        else
            questConversationController.StartConversation(characterName, conversation.sentences);

        // Empezamos coroutina para saber si ha acabado la conversación o no.
        ConversationFinished = ConversationFinishedtrigger(conversation);
        StartCoroutine(ConversationFinished);
    }

    // Finaliza la conversación
    private void EndQuestConversation(QuestConversation conversation)
    {
        // Reactiva el movimiento del jugador y cierra el dialogo de conversación
        FindObjectOfType<InputPlayerController>().canMoveCharacter = true;
        questConversationController.gameObject.SetActive(false);

        // Si el flujo era de completar misión se completa, si era de aceptarla la acepta.
        switch (flow)
        {
            case 1:
                CompleteQuest();
                break;
            case 3:
                AcceptQuest();
                break;
        }

        // Si finalizar la conversación completaba un objetivo llamamos para que lo complete.
        if (conversation.completeObjectiveOnFinish)
            CompleteObjective(conversation.objectiveId);
    }

    // Detecta si la conversación ha finalizado y llama a EndQuestConversation si esta ha acabado.
    IEnumerator ConversationFinishedtrigger(QuestConversation conversation)
    {
        //Debug.Log("Esperando a que acabe la conversación...");
        yield return new WaitUntil(questConversationController.IsConversationFinished);
        //Debug.Log("Conversación finalizada.");

        EndQuestConversation(conversation);
    }

    // Finaliza la interacción con el personaje.
    public void FinishInteraction()
    {
        if (FindObjectOfType<InputPlayerController>() != null)
            FindObjectOfType<InputPlayerController>().canMoveCharacter = true;
        if (currentQuestState == QuestState.Completed && nextQuestController != null)
        {
            GetComponent<QuestInteractableTrigger>().DisableInteractableColor();
            ActivateNextQuest();
        }
    }

    // Acepta la misión
    public void AcceptQuest()
    {
        // Añadimos la misión en el array del perfil de misiones actuales.
        GameManager.instance.Profile.CurrentQuests.Add(quest);

        // Por cada misión inicializamos objetivos si no estan inicializados
        foreach (Quest q in GameManager.instance.Profile.CurrentQuests)
            if (q.idQuest == quest.idQuest)
                q.InitObjectives();

        // LLamamos al EventManager para hacer saber que se ha aceptado una misión.
        EventManager.AcceptQuest();

        // Se verifica el estado de la misión local.
        CheckQuestState();
    }

    // Completa la misión.
    public void CompleteQuest()
    {
        Debug.Log("Complete Quest");

        Quest completedQuest = null;

        // Completamos la misión
        foreach (Quest q in GameManager.instance.Profile.CurrentQuests)
            if (q.idQuest == quest.idQuest)
                completedQuest = q;

        // Damos recompensa
        completedQuest.GiveReward();
        // Quitamos eventlisteners y desactivamos objetivos.
        completedQuest.EndObjectives();

        // Añadimos la misión a completa y la borramos de actual.
        GameManager.instance.Profile.CompletedQuests.Add(completedQuest);
        GameManager.instance.Profile.CurrentQuests.Remove(completedQuest);

        // Informamos al EventManager de que se ha completado una misión.
        EventManager.QuestCompleted();

        // Mostramos texto de alerta al jugador de que ha completado una misión.
        CanvasController canvaController = FindObjectOfType<CanvasController>();
        canvaController.ShowAlertText(2, Color.yellow, "Quest completed: " + quest.GetCurrentLanguageTitle());


    }

    // Completa un objetivo de una misión mediante su id.
    public void CompleteObjective(int idObjective)
    {
        Debug.Log("CompleteObjective(): " + idObjective);

        Quest currentQuest = null;
        // Buscamos la misión actual.
        foreach (Quest q in GameManager.instance.Profile.CurrentQuests)
            if (q.idQuest == quest.idQuest)
                currentQuest = q;

        //Debug.Log("curQuest: " + currentQuest);

        // Buscamos  el objetivo con el id y lo completamos.
        foreach (Objective o in currentQuest.Objectives)
        {
            //Debug.Log("o: " + o);
            if (o.idObjective == idObjective)
            {
                o.CompleteObjective();
                // Informamos de que se ha completado un objetivo.
                EventManager.ObjectiveUpdated();
                break;
            }
        }
    }

    // Si hay más misiones en el mismo personaje de misión la activamos y desactivamos esta.
    private void ActivateNextQuest()
    {
        nextQuestController.SetActive(true);
        gameObject.SetActive(false);
    }

    // Quitamos llamadas de eventos desactivadas.
    private void OnDestroy()
    {
        EventManager.OnQuestCompleted -= CheckQuestState;
    }

}
