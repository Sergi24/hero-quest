﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Controla la conversación de misión con un personaje
public class QuestConversationController : MonoBehaviour
{
    // Textos del nombre del personaje y la sentencia del dialogo actual.
    public Text namecharacter, sentenceText;
    // Imagen animada de continuar al finalizar el muestreo de una sentencia.
    public GameObject continueImage;
    // Booleanos que indican si la conversación ha finalizado o si se ha acabado de muestrear una sentencia.
    public bool isConversationFinished, isTypeSentenceFinished;

    // Referencias a los IEnumerators para que no se superpongan
    IEnumerator showDialogue, typeSentence;

    // Empieza la conversación
    public void StartConversation(string character, List<ConversationString> sentences)
    {
        namecharacter.text = character;

        if (showDialogue != null)
            StopCoroutine(showDialogue);

        showDialogue = ShowDialogue(sentences);
        StartCoroutine(showDialogue);

    }

    // Muestra el dialogo y va llamando a las sentencia cuando esta finaliza.
    IEnumerator ShowDialogue(List<ConversationString> sentences)
    {
        for (int i = 0; i < sentences.Count; i++)
        {
            string s = sentences[i].GetTextByCurrentLanguage();

            if (typeSentence != null)
                StopCoroutine(typeSentence);

            isTypeSentenceFinished = false;
            typeSentence = TypeSentence(s);
            StartCoroutine(typeSentence);

            yield return new WaitUntil(CanShowNextSentence);
            continueImage.SetActive(false);

        }
        isConversationFinished = true;
    }

    // Por cada frame muestra un carácter de la sentencia hasta completarla e informar de ello.
    IEnumerator TypeSentence(string sentence)
    {
        sentenceText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            sentenceText.text += letter;
            yield return null;
        }
        isTypeSentenceFinished = true;
        continueImage.SetActive(true);
    }

    // Está la converación finalizada?
    public bool IsConversationFinished()
    {
        return isConversationFinished;
    }

    // Puede seguir a la proxima sentencia?
    public bool CanShowNextSentence()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isTypeSentenceFinished)
            return true;

        return false;
    }

}
