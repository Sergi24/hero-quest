﻿// Script: FootStepsController.cs
// Descripción: A partir de una base de datos del script FootStepsDatabase detecta materiales físicos  de los suelos no terrenos y la textura predominante en terrenos pisados y ejecuta un sonido.
// Autor: Sergi Martínez Balsells
// Fecha: 15/10/2018
// Licencia: Dominio público

using UnityEngine;

// Clase llamada mediante evento de animación que detecta la superficie donde un pie pisa y ejecuta un clip de sonido.
public class FootStepsController : MonoBehaviour
{
    // AudioSource con los sonidos de las pisadas
    public AudioSource stepsAudioSource;
    // ScriptableObject con clips de cada tipo de material físico y textura de terreno.
    public FootStepsDatabase footStepsDatabase;

    private Animator animator;



    // Pie izquierdo y derecho
    Transform lFoot;
    Transform rFoot;

    // Enum, qué pie está tocando el suelo?
    public enum Foot
    {
        left, right
    };

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    // Obtenemos los transform de los pies mediante HumanBodyBones
    void Start()
    {
        lFoot = animator.GetBoneTransform(HumanBodyBones.LeftFoot);
        rFoot = animator.GetBoneTransform(HumanBodyBones.RightFoot);
    }

    // Ejecuta sonido mediante material físico.
    private void PlaySoundStepByPhysicMaterial(PhysicMaterial physicMaterial)
    {
        // Filtramos el " (Instance)" del nombre del material instanciado.
        string s = physicMaterial.name.Replace(" (Instance)", "");

        // Por cada material físico en la base de datos...
        foreach (PhysicMaterialSoundSteps pMat in footStepsDatabase.physicMaterials)
        {
            // Si lo tenemos en la base de datos procedemos a reproducir un clip aleatorio de los que tiene.
            if (pMat.physicMaterial.name.Equals(s))
            {
                //Debug.Log("Material found: " + s);

                int n = UnityEngine.Random.Range(0, pMat.audioclips.Count);

                stepsAudioSource.clip = pMat.audioclips[n];
                stepsAudioSource.Play();
                return;
            }
        }
        Debug.LogWarning("No se ha encontrado el material físico.");
    }
    // Ejecuta sonido mediante la textura del terreno.
    private void PlaySoundStepByTerrainTexture(Texture texture)
    {
        // Miramos las texturas de la base de datos...
        foreach (TextureSoundSteps tSoundSteps in footStepsDatabase.terrainTextures)
        {
            if (tSoundSteps.texture.ToString().Equals(texture.ToString()))
            {
                //Debug.Log("Texture found: " + texture.ToString());

                int n = UnityEngine.Random.Range(0, tSoundSteps.audioclips.Count);

                stepsAudioSource.clip = tSoundSteps.audioclips[n];
                stepsAudioSource.Play();
                return;
            }
        }
        Debug.LogWarning("No se ha encontrado la textura.");
    }

    // Función llamada desde el evento de animación, detecta si el suelo es terreno o no y llama a la función correspondiente para reproducir el clip.
    private void PlayStepSound(Foot foot)
    {
        // Raycast del pie en cuestión
        RaycastHit rayCastHit;
        Vector3 pos;

        if (foot == Foot.left)
        {
            pos = lFoot.TransformPoint(Vector3.zero);
        }
        else
        {
            pos = rFoot.TransformPoint(Vector3.zero);
        }

        // Se hace el raycast
        if (Physics.Raycast(pos, -Vector3.up, out rayCastHit, 1))
        {

            // Se intenta recoger el terreno para saber si es terreno.
            Terrain terrain = rayCastHit.collider.gameObject.GetComponent<Terrain>();

            // Si no es null es un terreno.
            if (terrain != null)
            {
                Vector3 footPosition = rayCastHit.point;

                // Indice de la textura del terreno actual.
                int surfaceIndex = 0;

                // Obtenemos la textura main
                surfaceIndex = GetMainTexture(terrain, footPosition);

                // Llamamos al método de reproducción de clips de terreno.
                PlaySoundStepByTerrainTexture(terrain.terrainData.splatPrototypes[surfaceIndex].texture);

                return;
            }

            // Se intenta recoger el material físico.
            Collider pMat = rayCastHit.collider.gameObject.GetComponent<Collider>();

            // Si no es null tiene material físico y lo
            if (pMat != null)
            {
                // Llamamos al método de reproducción de clips de materiales físicos.
                PlaySoundStepByPhysicMaterial(pMat.material);
            }

        }
    }

    // Cortesia de https://answers.unity.com/questions/456973/getting-the-texture-of-a-certain-point-on-terrain.html

    // Obtiene la mezcla de texturas de una posición determinada del terreno.
    float[] GetTextureMix(TerrainData tData, Vector3 tPos, Vector3 footPosition)
    {
        // returns an array containing the relative mix of textures
        // on the main terrain at this world position.

        // The number of values in the array will equal the number
        // of textures added to the terrain.

        // calculate which splat map cell the worldPos falls within (ignoring y)
        int mapX = (int)(((footPosition.x - tPos.x) / tData.size.x) * tData.alphamapWidth);
        int mapZ = (int)(((footPosition.z - tPos.z) / tData.size.z) * tData.alphamapHeight);

        // get the splat data for this cell as a 1x1xN 3d array (where N = number of textures)
        float[,,] splatmapData = tData.GetAlphamaps(mapX, mapZ, 1, 1);

        // extract the 3D array data to a 1D array:
        float[] cellMix = new float[splatmapData.GetUpperBound(2) + 1];

        for (int i = 0; i < cellMix.Length; i++)
        {
            cellMix[i] = splatmapData[0, 0, i];
        }

        return cellMix;
    }

    // Devuelve la textura con mayor porcentaje en la posición designada.
    int GetMainTexture(Terrain terrain, Vector3 footPosition)
    {
        TerrainData tData = terrain.terrainData;
        Vector3 tPos = terrain.transform.position;

        // returns the zero-based index of the most dominant texture
        // on the main terrain at this world position.
        float[] mix = GetTextureMix(tData, tPos, footPosition);

        float maxMix = 0;
        int maxIndex = 0;

        // loop through each mix value and find the maximum
        for (int i = 0; i < mix.Length; i++)
        {
            if (mix[i] > maxMix)
            {
                maxIndex = i;
                maxMix = mix[i];
            }
        }

        return maxIndex;
    }

}



