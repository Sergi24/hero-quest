using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Animator))]

// Controla las animaciones del personaje del jugador.
public class PlayerAnimationController : CharacterControllerStandardAssets
{
    // Animaci�n de recoger objeto (No implementada)
    public void TakeItem()
    {
        animator.SetTrigger("TakeItem");
    }

    // Procede a hacer las animaciones de ataque.
    public new void Attack()
    {

        // Actualiza el animator dependiendo d�l n� de clics realizados para que haga los ataques en combo o no.
        if (animator.GetBool("Attack1") && animator.GetBool("Attack2") && animator.GetBool("Attack3"))
        {
            //Debug.Log("All attacks activated");
        }
        else if (animator.GetBool("Attack1") && animator.GetBool("Attack2"))
        {
            animator.SetBool("Attack3", true);
        }
        else if (animator.GetBool("Attack1"))
        {
            animator.SetBool("Attack2", true);
        }
        else
        {
            animator.SetBool("Attack1", true);
        }
    }

    // Devuelve si est� atacando o no.
    public override bool IsAttacking()
    {
        return animator.GetBool("Attack1");
    }

    // Arma o desarma el personaje (falta implementar correctamente el estado de desarmado)
    public void Armed()
    {
        if (animator.GetBool("Armed") == true)
        {
            animator.SetBool("Armed", false);
        }
        else
        {
            animator.SetBool("Armed", true);
        }
    }

}
