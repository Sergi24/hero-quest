using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Cameras;

[RequireComponent(typeof(PlayerAnimationController))]

// Controla las variables e intenta centralizar todo el input del jugador.
public class InputPlayerController : MonoBehaviour
{
    // Controlador de animaciones
    private PlayerAnimationController playerAnimationController;
    // Camara.
    private Transform cam;
    // Hacia donde mira la c�mara.
    private Vector3 camForward;
    // Movimiento del personaje.
    private Vector3 move;

    // Estados del personaje.
    public CharacterStats characterStats;

    // Define si se puede o no mover el personaje.
    public bool canMoveCharacter = true;

    // Define si se puede o no interactuar con objetos interactables.
    public bool canInteract = false;
    public Interactable interactable;

    // Est� atacando?
    public bool isAttacking;

    // Armamento, armas y escudo.
    public List<WeaponTrigger> weaponTriggers;
    public ShieldStats shieldStats;

    // Objetivos cercanos a los que ve mediante un overlapsphere para rotar hacia ellos al atacar haciendo m�s facil la jugabilidad.
    private List<GameObject> objectives;

    // Puede golpear?
    public bool canHit;

    private void Start()
    {
        cam = Camera.main.transform;

        // get the third person character ( this should never be null due to require component )
        playerAnimationController = GetComponent<PlayerAnimationController>();

        GameManager.instance.LockMouse(true);

        canHit = false;
    }

    private void Update()
    {
        // Abre el inventario
        if (Input.GetKeyDown(KeyCode.I))
        {

            InventoryUIController invUI = FindObjectOfType<InventoryUIController>();

            invUI.ShowInventory();
        }

        // Si el jugador est� jugando...
        if (GameManager.instance.isPlaying)
        {
            // Si el jugador puede mover el personaje...
            if (canMoveCharacter)
            {
                // Interacci�n
                if (Input.GetKeyDown(KeyCode.E) && canInteract)
                {
                    if (interactable != null)
                        interactable.Interact();
                }
                // Ataque
                if (Input.GetMouseButtonDown(0))
                {
                    Attack();
                }
                // Armed & unarmed
                else if (Input.GetKeyDown(KeyCode.X))
                {
                    playerAnimationController.Armed();
                }
            }
        }
    }

    // Fixed update is called in sync with physics
    private void FixedUpdate()
    {
        // Si el personaje puede moverse y no est� atacando...
        if (canMoveCharacter && !isAttacking)
        {
            // read inputs
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");

            // calculate move direction to pass to character
            if (cam != null)
            {
                // calculate camera relative direction to move:
                camForward = Vector3.Scale(cam.forward, new Vector3(1, 0, 1)).normalized;
                move = v * camForward + h * cam.right;
            }
            else
            {
                // we use world-relative directions in the case of no main camera
                move = v * Vector3.forward + h * Vector3.right;
            }

            // pass all parameters to the character control script
            playerAnimationController.Move(move);
        }
        else
            playerAnimationController.Move(Vector3.zero);
    }

    // El personaje ataca
    private void Attack()
    {

        // Detecta enemigos cercanos y rota hacia ellos para hacer la jugabilidad m�s sencilla.
        Collider[] hitColliders = Physics.OverlapSphere(transform.position + Vector3.up, 2);

        objectives = new List<GameObject>();

        foreach (Collider c in hitColliders)
        {
            CharacterStats cStats = c.GetComponent<CharacterStats>();

            if (cStats != null && cStats != GetComponent<PlayerStats>() && cStats.gameObject.layer == LayerMask.NameToLayer("Enemy"))
            {
                objectives.Add(cStats.gameObject);
            }

        }

        // filtramos el objetivo m�s cercano para rotar hacia el
        if (objectives.Count != 0)
        {
            float minDistance = 5;
            GameObject tempGO = objectives[0];

            foreach (GameObject gO in objectives)
            {
                float distance = Vector3.Distance(gO.transform.position, transform.position);
                if (minDistance > distance)
                    tempGO = gO;
            }

            Vector3 relativePos = tempGO.transform.position - transform.position;
            transform.rotation = Quaternion.LookRotation(relativePos, Vector3.up);

        }

        // Si tiene arma la reincia para que pueda golpear
        if (weaponTriggers.Count != 0)
        {
            weaponTriggers[0].RestartEnemyHits();
            weaponTriggers[0].canHit = true;
        }

        isAttacking = true;

        // Animaci�n de ataque
        playerAnimationController.Attack();

    }

    // Procede a llamar a la animaci�n de da�o mediante el controlador de animacion.
    public void Hurt()
    {
        playerAnimationController.Hurt();
    }

    // Procede a llamar a la animaci�n de bloqueo mediante el controlador de animacion.
    public void Blocked()
    {
        playerAnimationController.Blocked();
    }

    // Procede a llamar a la animaci�n de muerte mediante el controlador de animacion.
    public void Die()
    {
        canMoveCharacter = false;
        playerAnimationController.Die();
    }

}
