﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// Clase simple de AI que se basa en controlar el personaje del jugador para los cambios de escena a lo legend of zelda.
public class AIPlayerController : MonoBehaviour
{
    // Objetivo
    public Transform target;

    // Agente
    public NavMeshAgent agent;
    // Controlador de aniamciones
    public PlayerAnimationController character;

    // la velocidad a la que se mueve.
    public float agentSpeed;

    private void Start()
    {
        agent.updateRotation = false;
        agent.updatePosition = true;
    }

    private void Update()
    {
        Moving();
    }

    // Actualiza el objetivo con el trasnform pasado.
    public void SetTarget(Transform target)
    {
        this.target = target;
        agent.speed = agentSpeed;
    }

    // Movimiento del agente
    public void Moving()
    {

        // Move
        if (target != null)
            agent.SetDestination(target.position);

        if (agent.remainingDistance > agent.stoppingDistance)
        {
            character.Move(agent.desiredVelocity);
        }
        else
            character.Move(Vector3.zero);

    }
}
