using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Animator))]

// Variante simplificada de la clase controladora de animaciones del paquete standard assets.
public class CharacterControllerStandardAssets : MonoBehaviour
{
    [SerializeField]
    protected float turnSpeed = 360;
    [SerializeField]
    protected float stationaryTurnSpeed = 180;
    [SerializeField] float moveSpeedMultiplier = 1f;
    [SerializeField] float animationSpeedMultiplier = .5f;
    [SerializeField] float groundCheckDistance = 0.1f;

    protected Rigidbody characterRigidbody;
    protected Animator animator;
    protected float TurnAmount;
    protected float forwardAmount;
    protected Vector3 groundNormal;
    protected float capsuleHeight;
    protected Vector3 capsuleCenter;
    protected CapsuleCollider capsule;
    protected bool isGrounded;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        characterRigidbody = GetComponent<Rigidbody>();
        capsule = GetComponent<CapsuleCollider>();
    }

    void Start()
    {

        capsuleHeight = capsule.height;
        capsuleCenter = capsule.center;

        characterRigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
    }


    public void Move(Vector3 move)
    {

        // convert the world relative moveInput vector into a local-relative
        // turn amount and forward amount required to head in the desired
        // direction.
        if (move.magnitude > 1f) move.Normalize();
        move = transform.InverseTransformDirection(move);
        CheckGroundStatus();
        move = Vector3.ProjectOnPlane(move, groundNormal);
        TurnAmount = Mathf.Atan2(move.x, move.z);
        forwardAmount = move.z;

        ApplyExtraTurnRotation();

        // send input and other state parameters to the animator
        UpdateAnimator(move);
    }

    /*
    public void Rotate(Quaternion lookRotation, Transform target)
    {
        Debug.Log("Rotate()");

        Vector3 direction = target.position - transform.position;
        float turnDirection = AngleDir(transform.forward, direction, transform.up);

        //Debug.Log(turnDirection);

        animator.SetFloat("Turn", turnDirection);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * stationaryTurnSpeed);
    }
    */

    float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);

        if (dir > 0f)
        {
            return .5f;
        }
        else if (dir < 0f)
        {
            return -.5f;
        }
        else
        {
            return 0f;
        }
    }


    void UpdateAnimator(Vector3 move)
    {
        // update the animator parameters
        animator.SetFloat("Forward", forwardAmount, 0.1f, Time.deltaTime);
        animator.SetFloat("Turn", TurnAmount, 0.1f, Time.deltaTime);

        // the anim speed multiplier allows the overall speed of walking/running to be tweaked in the inspector,
        // which affects the movement speed because of the root motion.
        if (move.magnitude > 0)
        {
            animator.speed = animationSpeedMultiplier;
        }
        else
        {
            // don't use that while airborne
            animator.speed = 1;
        }
    }

    void ApplyExtraTurnRotation()
    {
        // help the character turn faster (this is in addition to root rotation in the animation)
        float turnSpeed = Mathf.Lerp(stationaryTurnSpeed, this.turnSpeed, forwardAmount);
        transform.Rotate(0, TurnAmount * turnSpeed * Time.deltaTime, 0);
    }

    // Este motodo es el que controla la moci�n del animator.
    public void OnAnimatorMove()
    {
        // we implement this function to override the default root motion.
        // this allows us to modify the positional speed before it's applied.
        if (isGrounded && Time.deltaTime > 0)
        {
            Vector3 v = (animator.deltaPosition * moveSpeedMultiplier) / Time.deltaTime;

            // we preserve the existing y part of the current velocity.
            v.y = characterRigidbody.velocity.y;
            characterRigidbody.velocity = v;
        }
    }

    // Verifica que el personaje est� en el suelo
    void CheckGroundStatus()
    {
        RaycastHit hitInfo;

        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, groundCheckDistance))
        {
            groundNormal = hitInfo.normal;
            isGrounded = true;
            animator.applyRootMotion = true;
        }
        else
        {
            isGrounded = false;
            groundNormal = Vector3.up;
            animator.applyRootMotion = false;
        }
    }

    // Devuelve 
    public virtual bool Attack()
    {
        bool b;
        int n = UnityEngine.Random.Range(0, 2);
        if (n == 0)
        {
            animator.SetTrigger("Attack1");
            b = false;
        }

        else
        {
            animator.SetTrigger("Attack2");
            b = true;
        }
        animator.SetBool("IsAttacking", true);
        GetComponent<AudioCharacterController>().PlayAttackClip();

        return b;
    }

    // Devuelve si est� o no atacando.
    public virtual bool IsAttacking()
    {
        return animator.GetBool("IsAttacking");
    }

    // Procede a hacer la animaci�n de muerte.
    public virtual void Die()
    {
        animator.SetTrigger("Die");
        // Reproduce el sonido de muerte.
        GetComponent<AudioCharacterController>().PlayDeadClip();
    }

    // Procede a hacer la animaci�n de recibir da�o.
    public virtual void Hurt()
    {
        animator.SetTrigger("Hurt");
        // Reprroduce el sonido de da�o
        GetComponent<AudioCharacterController>().PlayHurtClip();
    }

    // Procede a hacer la animaci�n de bloqueo
    public virtual void Blocked()
    {
        animator.SetTrigger("Blocked");
        // Reproduce el sonido de bloqueo
        GetComponent<AudioCharacterController>().PlayBlockedClip();
    }


}

