﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Animator))]
// Controla las animaciones de la IA armada con espada y escudo.
public class KnightCharacterController : CharacterControllerStandardAssets
{

    // Actualiza el animator de la ia armada con espada y escudo diciendo si está atacando y que animación de ataque utiliza.
    public new void Attack()
    {
        // Calculamos un aleatorio entero para decidir la animación de ataque.
        int n = Random.Range(0, 2);
        if (n == 0)
            animator.SetTrigger("Attack1");
        else
            animator.SetTrigger("Attack2");

        // Actualizamos el animator con la variable correspondiente.
        animator.SetBool("IsAttacking", true);
    }


}

