using System;
using UnityEngine;
using UnityEngine.AI;
using System.Collections;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(KnightCharacterController))]

// Controlador de IA basado en personajes con espada y escudo o simplemente espada de una mano.
public class AIKnightCharacterController : AICharacterController
{
    // Inicializamos el start de la clase heredada
    protected override void Start()
    {
        base.Start();
        character = GetComponent<KnightCharacterController>();
    }

    // Sirve para empezar una caceria cuando un objetivo es detectado. Se llama si el personaje es golpeado o bien ConecastDetector detecta un objetivo en su area de detecci�n.
    public override void StartHunting(Transform target)
    {
        GetComponent<CharacterStats>().StartCombat(target);
        returned = false;
        if (canStartHunting == true)
        {
            //Debug.Log("StartHunting");
            // Guardamos datos de antes de la caza para poder volver al estado anterior.
            if (startHuntingPositionTransform == null)
            {
                GameObject startHuntingPositionGameobject = new GameObject("Start Hunting Point");
                startHuntingPositionGameobject.transform.position = transform.position;
                startHuntingPositionGameobject.transform.rotation = transform.rotation;
                startHuntingPositionTransform = startHuntingPositionGameobject.transform;
            }
            iaStateBeforeHunting = currentIAState;
            targetBeforeHunting = this.target;
            // Actualizamos datos
            SetTarget(target);
            currentIAState = IAState.Hunting;
            // Configuramos el agent, nos aseguramos que parar� donde debe pararse.
            agent.stoppingDistance = huntingStoppingDistance;
            currentAgentSpeedState = maxAgentSpeedState;
        }
        else
        {
            //Debug.Log("Can't start a hunt");
        }
    }

    // Maneja los datos mientras el personaje est� cazando a su objetivo.
    public override void Hunting()
    {
        // Si el objetivo ha muerto...
        if (target.GetComponent<CharacterStats>().dead)
            StartReturning();

        // Si no ha muerto...
        if (Vector3.Distance(startHuntingPositionTransform.position, transform.position) > maxDistance)
        {
            StartReturning();
        }
        else if (Vector3.Distance(target.position, transform.position) <= minAttackDistance)
        {
            // El personaje est� atacando? Si no lo est� haciendo que ataque!
            if (character.IsAttacking() == false)
            {
                currentAgentSpeedState = AgentSpeedState.Idle;

                character.Attack();
                // Reiniciamos algunas variables para que weaponTrigger pueda golpear a su enemigo.
                weaponTriggers[0].RestartEnemyHits();
                weaponTriggers[0].canHit = true;

                // Empezamos coroutina de ataque para que el personaje mire al objetivo siempre que no est� atacando.
                isAttackingEnumerator = IsAttacking(weaponTriggers[0]);
                StartCoroutine(isAttackingEnumerator);
            }
        }
    }

    // Comprueba mediante el animator si el personaje est� atacando y rota el personaje hacia el objetivo.
    public override IEnumerator IsAttacking(WeaponTrigger weaponTrigger)
    {
        while (character.IsAttacking() == true && target != null)
        {
            if (target != null)
                RotateTowards(target.transform);
            yield return null;
        }
        weaponTrigger.canHit = false;
        currentAgentSpeedState = maxAgentSpeedState;
    }

}

