using System;
using UnityEngine;
using UnityEngine.AI;
using System.Collections;


[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(SlaveCharacterController))]

// Controlador de IA basado en el personaje Skeleton Slave.
public class AISlaveCharacterController : AICharacterController
{
    // arm 0 left arm 1 right
    private bool arm;

    // Inicializamos el start de la clase heredada
    protected override void Start()
    {
        base.Start();
        character = GetComponent<SlaveCharacterController>();
    }

    // Sirve para empezar una caceria cuando un objetivo es detectado. Se llama si el personaje es golpeado o bien ConecastDetector detecta un objetivo en su area de detecci�n.
    public override void StartHunting(Transform target)
    {
        // Empezamos combate con el objetivo
        GetComponent<CharacterStats>().StartCombat(target);
        returned = false;
        if (canStartHunting == true)
        {
            //Debug.Log("StartHunting");
            // Guardamos datos de antes de la caza para poder volver al estado anterior.
            if (startHuntingPositionTransform == null)
            {
                Debug.Log("startHuntingPositionTransform is null");
            }
            GameObject startHuntingPositionGameobject = new GameObject("Start Hunting Point");
            startHuntingPositionGameobject.transform.position = transform.position;
            startHuntingPositionGameobject.transform.rotation = transform.rotation;
            startHuntingPositionTransform = startHuntingPositionGameobject.transform;

            iaStateBeforeHunting = currentIAState;
            targetBeforeHunting = this.target;
            // Actualizamos datos
            SetTarget(target);
            currentIAState = IAState.Hunting;
            // Configuramos el agent, nos aseguramos que parar� donde debe pararse.
            agent.stoppingDistance = huntingStoppingDistance;

            // Hacemos una probabilidad entre 3 que haga un grito amenazador al objetivo.
            int n = UnityEngine.Random.Range(0, 3);
            if (n == 0)
            {
                currentAgentSpeedState = AgentSpeedState.Idle;
                ((SlaveCharacterController)character).Scream();
                StartCoroutine(IsScreaming());
            }
            else
                currentAgentSpeedState = maxAgentSpeedState;
        }
        else
        {
            Debug.Log("Can't start a hunt");
        }
    }

    // Controla el personaje mientras est� cazando a su objetivo.
    public override void Hunting()
    {
        // Si el objetivo ha muerto...
        if (target.GetComponent<CharacterStats>().dead)
        {
            Debug.Log("Objective dead, returning");
            StartReturning();
        }

        if (Vector3.Distance(startHuntingPositionTransform.position, transform.position) > maxDistance)
        {
            StartReturning();
        }
        else if (Vector3.Distance(target.position, transform.position) <= minAttackDistance)
        {
            // Si no est� atacando ataca...
            if (character.IsAttacking() == false)
            {
                currentAgentSpeedState = AgentSpeedState.Idle;

                // Escogemos la animaci�n con la que atacar� y atacamos.
                // false = left hand,  true = right hand
                arm = character.Attack();
                int n;
                if (arm)
                    n = 1;
                else
                    n = 0;

                // Reiniciamos weaponTriggers
                weaponTriggers[n].RestartEnemyHits();
                weaponTriggers[n].canHit = true;

                // Empezamos coroutina de grito.
                isAttackingEnumerator = IsAttacking(weaponTriggers[n]);
                StartCoroutine(isAttackingEnumerator);
            }
        }
    }


    // Controla si est� gritando o ha acabado de gritar el grito amenazador.
    public IEnumerator IsScreaming()
    {
        while (((SlaveCharacterController)character).IsScreaming() == true && target != null)
        {
            if (target != null)
                RotateTowards(target.transform);
            yield return null;
        }
        currentAgentSpeedState = maxAgentSpeedState;
    }


}

