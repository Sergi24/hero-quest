﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Animator))]
public class SlaveCharacterController : CharacterControllerStandardAssets
{
    // Procede a hacer la animación de ataque.
    public new bool Attack()
    {
        // Escogemos la animación de ataque del brazo derecho o izquierdo.
        bool b;
        int n = UnityEngine.Random.Range(0, 2);
        if (n == 0)
        {
            animator.SetTrigger("Attack1");
            b = false;
        }
        else
        {
            animator.SetTrigger("Attack2");
            b = true;
        }

        // Actualizamos animator
        animator.SetBool("IsAttacking", true);
        // Reproducimos el sonido de ataque.
        GetComponent<AudioCharacterController>().PlayAttackClip();

        // Devolvemos con que animación atacamos para que el controlador de ia pueda reiniciar el trigger en cuestión.
        return b;
    }

    // Procede a hacear la animación del grito amenazador.
    public void Scream()
    {
        // Actualizamos animator
        animator.SetTrigger("Scream");
        animator.SetBool("IsScreaming", true);
        // Reproducimos el sonido de grito
        GetComponent<AudioCharacterController>().PlayScreamClip();
    }

    // Devuelve si está o no gritando.
    public bool IsScreaming()
    {
        return animator.GetBool("IsScreaming");
    }

}

