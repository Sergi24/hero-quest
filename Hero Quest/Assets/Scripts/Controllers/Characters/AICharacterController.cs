﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// Clase base de la AI de los personajes
public abstract class AICharacterController : MonoBehaviour
{

    // clases necesarias, agente y contorlador de animaciones
    protected NavMeshAgent agent;
    protected CharacterControllerStandardAssets character;

    // Objetivo
    public Transform target;

    // armamento
    public List<WeaponTrigger> weaponTriggers;
    public ShieldStats shieldStats;

    // Puede golpear?
    public bool canHit;

    // IAState: Define el estado actual de la IA, quieto, patrullando, cazando, volviendo de la caza o muerto.
    public enum IAState { Idle, Patrolling, Hunting, Returning, Die };
    public IAState currentIAState = IAState.Patrolling;

    // PatrollingState: Define el estado de la patrulla actual: Yendo a u punto o bien en un punto de patrulla.
    protected enum PatrollingState { GoingToPoint, InAPoint }
    protected PatrollingState currentPatrollingState = PatrollingState.GoingToPoint;

    // SpeedAnimationState: Define si el personaje está andando o corriendo.
    public enum AgentSpeedState { Idle, Walking, Running }
    public AgentSpeedState currentAgentSpeedState;
    public AgentSpeedState NonCombatAgentSpeedState = AgentSpeedState.Walking;
    public AgentSpeedState maxAgentSpeedState = AgentSpeedState.Running;

    // Define la velocidad del personaje si está andando o corriendo.
    public float walkingSpeed = 0.6f;
    public float runningSpeed = 3f;

    // Variables necesarias para controlar la patrulla.
    public List<PatrolPoint> patrolPoints;
    protected int currentPatrolPoint;
    protected float TimeWaitedOnPoint = 0;

    // Variables necesarias para controlar la cacería.
    public float huntingStoppingDistance = 2;
    public float maxDistance;

    protected Transform startHuntingPositionTransform;
    protected IAState iaStateBeforeHunting;
    protected Transform targetBeforeHunting;
    protected bool canStartHunting = true;

    // Variables para controlar el ataque
    public float minAttackDistance = 2;
    protected IEnumerator isAttackingEnumerator;

    // Ha vuelto de cazar?
    public bool returned = true;

    protected virtual void Start()
    {
        // Cogemos los componentes que necesitamos.
        agent = GetComponentInChildren<NavMeshAgent>();
        character = GetComponent<CharacterControllerStandardAssets>();

        // Configramos como queremos el agent
        agent.updateRotation = false;
        agent.updatePosition = true;

        // Aleatorizamos si el personaje fuera del combate va corriendo o caminando.
        int n = Random.Range(0, 2);
        if (n == 0)
            NonCombatAgentSpeedState = AgentSpeedState.Walking;
        else
            NonCombatAgentSpeedState = maxAgentSpeedState;

        // Ponemos como objetivo el punto de patrulla si tiene y está en modo patrulla.
        if (currentIAState == IAState.Patrolling)
            StartPatrolling();

        // Preparamos el armamento (WeaponTriggers).
        if (weaponTriggers.Count > 0)
            foreach (WeaponTrigger wT in weaponTriggers)
                wT.canHit = false;

    }


    protected virtual void Update()
    {
        // Estado en el que simplemente está parado.
        if (currentIAState == IAState.Idle)
        {
            Motion();
        }
        // Estado en el que patrulla.
        else if (currentIAState == IAState.Patrolling)
        {
            Patrolling();
            Motion();
        }
        // Estado en el que persigue a un personaje.
        else if (currentIAState == IAState.Hunting)
        {
            Hunting();
            Motion();
        }
        // Estado en el que vuelve de perseguir un personaje al punto donde ha empezado a perseguirlo para retomar su tarea.
        else if (currentIAState == IAState.Returning)
        {
            Returning();
            Motion();
        }
        // Estado en el que ha recibido tanto daño que muere.
        else if (currentIAState == IAState.Die)
        {
        }

        // Actualizamos la velocidad de los diferentes estados de velocidad.
        if (currentAgentSpeedState == AgentSpeedState.Idle)
        {
            agent.speed = 0;
        }
        else if (currentAgentSpeedState == AgentSpeedState.Walking)
        {
            agent.speed = walkingSpeed;
        }
        else if (currentAgentSpeedState == AgentSpeedState.Running)
        {
            agent.speed = runningSpeed;
        }

    }

    // Actualizamos el objetivo
    public virtual void SetTarget(Transform target)
    {
        this.target = target;
    }

    // Movimiento del personaje
    public virtual void Motion()
    {

        // Move
        if (target != null)
            agent.SetDestination(target.position);

        if (agent.remainingDistance > agent.stoppingDistance)
        {
            character.Move(agent.desiredVelocity);
        }
        else
            character.Move(Vector3.zero);

    }

    // ////////////////////////////// PATROLLING ///////////////////////////////////////////////////

    // Empieza a patrullar configurando las variables para patrullar.
    public virtual void StartPatrolling()
    {
        agent.stoppingDistance = 0;
        SetTarget(patrolPoints[currentPatrolPoint].transform);
        patrolPoints[currentPatrolPoint].isCharacterComming = true;
        currentAgentSpeedState = NonCombatAgentSpeedState;
    }

    // Maneja los estados de la patrulla mientras se está en esta.
    public virtual void Patrolling()
    {
        // Si esta yendo a un punto...
        if (currentPatrollingState == PatrollingState.GoingToPoint)
        {
            if (Extns.FlatDistanceTo(transform.position, target.position) < .5f)
                currentPatrollingState = PatrollingState.InAPoint;
        }
        // Al estar en un punto revisa si se ha acabado el tiempo que debe permanecer en el punto.
        else if (currentPatrollingState == PatrollingState.InAPoint)
        {
            // Actualiza el tiempo esperado.
            if (patrolPoints[currentPatrolPoint].timeWaiting > TimeWaitedOnPoint)
            {
                TimeWaitedOnPoint += Time.deltaTime;
            }
            // Si ha hecho el tiempo requerido...
            else
            {                // se revisa si no hay nadie yendo al punto. Si lo hay espera.
                if (patrolPoints[GetNextPatrolPoint()].isCharacterComming)
                {
                    TimeWaitedOnPoint = 0;
                }
                // Si está libre pasa al siguiente punto.
                else
                {
                    patrolPoints[currentPatrolPoint].isCharacterComming = false;
                    NextPatrolPoint();
                    TimeWaitedOnPoint = 0;
                    currentPatrollingState = PatrollingState.GoingToPoint;
                }
            }
        }
    }

    // Activa el siguiente punto de patrulla.
    public virtual void NextPatrolPoint()
    {
        // Si el punto de patrulla actual es el último de la lista, volvará al inicial.
        if (patrolPoints.Count == currentPatrolPoint + 1)
        {
            currentPatrolPoint = 0;
            SetTarget(patrolPoints[currentPatrolPoint].transform);
        }
        // Si no es el último, pasará al siguiente.
        else
        {
            currentPatrolPoint++;
            SetTarget(patrolPoints[currentPatrolPoint].transform);
            patrolPoints[currentPatrolPoint].isCharacterComming = true;
        }
    }

    // Obtiene el siguiente punto de patrulla
    public virtual int GetNextPatrolPoint()
    {
        // Si el punto de patrulla actual es el último de la lista, volvará al inicial.
        if (patrolPoints.Count == currentPatrolPoint + 1)
        {
            return 0;
        }
        // Si no es el último, pasará al siguiente.
        else
        {
            return currentPatrolPoint + 1;
        }
    }

    // ////////////////////////////// HUNTING ///////////////////////////////////////////////////

    // Empieza la cacería 
    public virtual void StartHunting(Transform target)
    {
        if (canStartHunting == true)
        {
            Debug.Log("StartHunting");

            // Guardamos datos de antes de la caza para poder volver al estado anterior.
            if (startHuntingPositionTransform == null)
            {
                GameObject startHuntingPositionGameobject = new GameObject("Start Hunting Point");
                startHuntingPositionGameobject.transform.position = transform.position;
                startHuntingPositionGameobject.transform.rotation = transform.rotation;
                startHuntingPositionTransform = startHuntingPositionGameobject.transform;
            }
            iaStateBeforeHunting = currentIAState;
            targetBeforeHunting = this.target;
            // Actualizamos datos
            SetTarget(target);
            currentIAState = IAState.Hunting;
            // Entra en combate
            GetComponent<CharacterStats>().StartCombat(target);
            returned = false;
            // Nos aseguramos que parará donde debe pararse configurando correctamente el agente.
            agent.stoppingDistance = huntingStoppingDistance;
            currentAgentSpeedState = maxAgentSpeedState;
        }
        else
        {
            Debug.Log("Can't start a hunt");
        }
    }

    // Controla el estado de cazando
    public virtual void Hunting()
    {
        if (target != null)
            currentIAState = IAState.Idle;

        // Si el objetivo ha muerto...
        if (target.GetComponent<CharacterStats>().dead)
            StartReturning();

        // Si no ha muerto...
        // Si la distancia máxima a la que puede alejarse es superada...
        if (Vector3.Distance(startHuntingPositionTransform.position, transform.position) > maxDistance)
        {
            // Empieza a volver.
            StartReturning();
        }
        // Si aun está dentro de la distancia máxima sigue cazando
        else if (Vector3.Distance(target.position, transform.position) <= minAttackDistance)
        {
            //Debug.Log(character.IsAttacking());
            if (character.IsAttacking() == false)
            {
                currentAgentSpeedState = AgentSpeedState.Idle;

                // 0 = left,  1 = right
                character.Attack();

                weaponTriggers[0].RestartEnemyHits();
                weaponTriggers[0].canHit = true;

                isAttackingEnumerator = IsAttacking(weaponTriggers[0]);
                StartCoroutine(isAttackingEnumerator);
            }
        }
    }

    // Devuelve si puede o no empezar a cazar
    public virtual bool CanStartHunting()
    {
        return canStartHunting;
    }

    // ////////////////////////////// HURT ///////////////////////////////////////////////////

    // Llama al controlador de animaciones para proceder a ejecutar la animación de dañado.
    public void Hurt()
    {
        character.Hurt();
    }

    // ////////////////////////////// Blocked ///////////////////////////////////////////////////

    // Llama al controlador de animaciones para proceder a ejecutar la animación de bloqueo.
    public virtual void Blocked()
    {
        character.Blocked();
    }

    // ////////////////////////////// RETURNING ///////////////////////////////////////////////////

    // Empieza el retorno de la caceria.
    public virtual void StartReturning()
    {
        //Debug.Log("Start Returning");

        GetComponent<CharacterStats>().EndCombat(target);
        SetTarget(startHuntingPositionTransform);
        currentIAState = IAState.Returning;
        agent.stoppingDistance = 0;
        canStartHunting = false;
    }

    // Maneja las variables mientras se está volviendo a la posición inicial.
    public virtual void Returning()
    {
        // Arreglo...
        //if (target != null)
        //  currentIAState = IAState.Idle;

        if (Vector3.Distance(transform.position, target.position) < .5f)
        {
            Returned();
        }
    }

    // Al haber vuelto a la posición inicial se retoma la tarea que se estaba haciendo.
    public virtual void Returned()
    {
        //Debug.Log("Returned");
        // Actualizamos variables actuales con las variables de antes de la caceria.
        ResetRotate(startHuntingPositionTransform.rotation);
        // if (targetBeforeHunting != null)
        SetTarget(targetBeforeHunting);
        currentIAState = iaStateBeforeHunting;
        currentAgentSpeedState = NonCombatAgentSpeedState;
        Destroy(startHuntingPositionTransform.gameObject);
        StartCoroutine(WaitOneFrame());
    }

    // Intento de arreglar el comportamiento... Algo no funcionaba y daba error.
    IEnumerator WaitOneFrame()
    {
        yield return null;
        returned = true;
        canStartHunting = true;
    }

    // ////////////////////////////// DIE ///////////////////////////////////////////////////

    // Procede a hacer la muerte del personaje actualizando variables relacionadas.
    public virtual void Die()
    {
        GetComponent<CharacterStats>().dead = true;
        GetComponent<AICharacterController>().target = null;
        currentAgentSpeedState = AgentSpeedState.Idle;
        character.Die();
        if (startHuntingPositionTransform != null)
            Destroy(startHuntingPositionTransform.gameObject);
        Destroy(this.gameObject, 5f);
    }

    // Rota hacia el objetivo
    public virtual void RotateTowards(Transform targetTransform)
    {
        Vector3 direction = (targetTransform.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    // Enumerator de si está o no atacando
    public virtual IEnumerator IsAttacking(WeaponTrigger weaponTrigger)
    {
        while (character.IsAttacking() == true && target != null)
        {
            if (target != null)
                RotateTowards(target.transform);
            yield return null;
        }
        weaponTrigger.canHit = false;
        currentAgentSpeedState = maxAgentSpeedState;
    }

    // Reinicia la rotación del personaje
    public virtual void ResetRotate(Quaternion rotation)
    {
        transform.rotation = rotation;
    }

    // Devuelve si puede o no golpear
    public virtual bool CanHit()
    {
        return canHit;
    }

}
