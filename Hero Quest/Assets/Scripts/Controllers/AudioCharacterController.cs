﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Controla los clips de audio de cada personaje. Tiene referencias al archivo de cada uno.
public class AudioCharacterController : MonoBehaviour
{
    // Referencia a la pequeña base de datos de la voz del personaje.
    public VoiceCharacterDatabase voiceCharacterDatabase;

    // Referencia a los audiosource de voz y armas del personaje.
    public AudioSource voiceAudioSource, WeaponsAudiosource;

    // Variables para ejecutar sonidos usuales (fuera del combate) y de caceria (mientras caza a un objetivo)
    [SerializeField]
    private float minUsualTimeBetweenClips;
    [SerializeField]
    private float maxUsualTimeBetweenClips;
    [SerializeField]
    private float minHuntingTimeBetweenClips;
    [SerializeField]
    private float maxHuntingTimeBetweenClips;

    private float timeCount;
    private float timeForCurrentClip;

    // Algunos personajes como el del jugador no tienen los sonidos usuales y de caceria activos.
    [SerializeField]
    private bool isUpdateSoundsActive;

    // Si isUpdateSoundsActive está activo procede a ir haciendo clips de audio correspondientes en los intervalos definidos.
    private void Update()
    {
        if (GetComponent<CharacterStats>().dead == false && isUpdateSoundsActive == true)
        {
            // Hunting sounds
            if (GetComponent<CharacterStats>().inCombat == true)
            {
                if (timeForCurrentClip == 0)
                    timeForCurrentClip = Random.Range(minHuntingTimeBetweenClips, maxHuntingTimeBetweenClips);

                timeCount += Time.deltaTime;

                if (timeCount >= timeForCurrentClip)
                {
                    PlayHuntingClip();
                    timeCount = 0;
                    timeForCurrentClip = 0;
                }
            }
            // Usual sounds
            else if (GetComponent<AICharacterController>().returned == true)
            {
                if (timeForCurrentClip == 0)
                    timeForCurrentClip = Random.Range(minUsualTimeBetweenClips, maxUsualTimeBetweenClips);

                timeCount += Time.deltaTime;

                if (timeCount >= timeForCurrentClip)
                {
                    PlayUsualClip();
                    timeCount = 0;
                    timeForCurrentClip = 0;
                }

            }

        }
    }

    // Ejecuta un clip usual.
    public void PlayUsualClip()
    {
        if (!voiceAudioSource.isPlaying)
        {
            int n = Random.Range(0, voiceCharacterDatabase.usualClips.Count);
            voiceAudioSource.clip = voiceCharacterDatabase.usualClips[n];
            voiceAudioSource.Play();
        }
    }

    // Ejecuta un clip de ataque.
    public void PlayAttackClip()
    {

        int n = Random.Range(0, voiceCharacterDatabase.attackClips.Count);
        voiceAudioSource.clip = voiceCharacterDatabase.attackClips[n];
        voiceAudioSource.Play();
    }

    // Ejecuta un clip de cazando si no hay ninguno activo
    public void PlayHuntingClip()
    {
        if (!voiceAudioSource.isPlaying)
        {
            int n = Random.Range(0, voiceCharacterDatabase.huntingClips.Count);
            voiceAudioSource.clip = voiceCharacterDatabase.huntingClips[n];
            voiceAudioSource.Play();
        }
    }

    // Ejecuta un clip de dañado.
    public void PlayHurtClip()
    {
        int n = Random.Range(0, voiceCharacterDatabase.hurtClips.Count);
        voiceAudioSource.clip = voiceCharacterDatabase.hurtClips[n];
        voiceAudioSource.Play();
    }

    // Ejecuta un clip de muerte.
    public void PlayDeadClip()
    {
        int n = Random.Range(0, voiceCharacterDatabase.deadClips.Count);
        voiceAudioSource.clip = voiceCharacterDatabase.deadClips[n];
        voiceAudioSource.Play();
    }

    // Ejecuta un clip de grito amenazador.
    public void PlayScreamClip()
    {
        int n = Random.Range(0, voiceCharacterDatabase.screamClips.Count);
        voiceAudioSource.clip = voiceCharacterDatabase.screamClips[n];
        voiceAudioSource.Play();
    }

    // Ejecuta un clip de sonido de arma
    public void PlayWeaponSound()
    {
        // No se usa, no me gustó como quedaba. Se podria usar para paradas (colisiones entre armas)
        int n = Random.Range(0, SoundManager.instance.combatsoundsDatabase.swordAttackClips.Count);
        voiceAudioSource.clip = SoundManager.instance.combatsoundsDatabase.swordAttackClips[n];
        voiceAudioSource.Play();
    }

    // Ejecuta un clip de bloqueo de escudo.
    public void PlayBlockedClip()
    {
        int n = Random.Range(0, SoundManager.instance.combatsoundsDatabase.shieldBlockClips.Count);
        voiceAudioSource.clip = SoundManager.instance.combatsoundsDatabase.shieldBlockClips[n];
        voiceAudioSource.Play();
    }

}
