﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

// Clase padre de Interactabilidad
public class Interactable : MonoBehaviour
{
    // Color de interactable cuando puede ser interactuado
    private Color color = Color.yellow;

    // Colores originales
    private Color[] originalColoursMeshRenderer;
    private Color[] originalColoursSkinnedMeshRenderer;

    // Mallas del personaje interactuable
    public MeshRenderer meshRenderer;
    public SkinnedMeshRenderer skinnedMeshRenderer;

    protected void Init()
    {
        if (meshRenderer != null)
            originalColoursMeshRenderer = meshRenderer.materials.Select(x => x.color).ToArray();
        if (skinnedMeshRenderer != null)
            originalColoursSkinnedMeshRenderer = skinnedMeshRenderer.materials.Select(x => x.color).ToArray();
    }

    // Interactua
    public virtual void Interact()
    {
        //Debug.Log("Interacting with base Interaction class.");
    }

    // Muestra el color de interactuabilidad del objeto o personaje en cuestión
    public void ShowInteractableColor(bool b)
    {
        if (b)
        {
            if (meshRenderer != null)
            {
                foreach (Material mat in meshRenderer.materials)
                {
                    mat.color *= color;
                }
            }
            else if (skinnedMeshRenderer != null)
            {


                foreach (Material mat in skinnedMeshRenderer.materials)
                {
                    mat.color *= color;
                }
            }
        }
        else
        {
            if (meshRenderer != null)
            {
                for (int i = 0; i < originalColoursMeshRenderer.Length; i++)
                {
                    meshRenderer.materials[i].color = originalColoursMeshRenderer[i];
                }
            }
            else if (skinnedMeshRenderer != null)
            {
                for (int i = 0; i < originalColoursSkinnedMeshRenderer.Length; i++)
                {
                    skinnedMeshRenderer.materials[i].color = originalColoursSkinnedMeshRenderer[i];
                }
            }
        }
    }
}
