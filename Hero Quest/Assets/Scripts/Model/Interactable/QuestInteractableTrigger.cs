﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(QuestController))]
// Controla el trigger de los personajes de misión
public class QuestInteractableTrigger : Interactable
{
    private CanvasController canvasController;

    // Personaje de misión
    public CharacterStats character;
    // Se usa este nombre si character es null
    public string characterName;

    // Quest controller de referencia
    private QuestController questController;

    private void Start()
    {
        Init();
        canvasController = FindObjectOfType<CanvasController>();

        questController = GetComponent<QuestController>();

    }

    // Si se llama, se interactua con la misión
    public override void Interact()
    {
        base.Interact();
        questController.Interact();
    }

    // Cambia el color del objetivo interactable y cambia el infotext.
    private void OnTriggerEnter(Collider other)
    {
        ShowInteractableColor(true);

        if (character != null)
            canvasController.ShowInfoText("Speak with " + character.nameCharacter);
        else
            canvasController.ShowInfoText("Speak with " + characterName);
    }

    // Comprueba que si está null el interactable del jugador y en caso de estar null le da este y actualiza el info text.
    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponentInParent<InputPlayerController>().interactable == null)
        {
            other.GetComponentInParent<InputPlayerController>().interactable = this;
            ShowInteractableColor(true);

            if (character != null)
                canvasController.ShowInfoText("Speak with " + character.nameCharacter);
            else
                canvasController.ShowInfoText("Speak with " + characterName);
        }
    }

    // Deshabilita el color de interactabilidad
    private void OnTriggerExit(Collider other)
    {
        DisableInteractableColor();
    }

    public void DisableInteractableColor()
    {
        ShowInteractableColor(false);
        canvasController.HideInfoText();
    }

}
