﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Controla el trigger de los npc aliados interactables
public class NPCInteractable : Interactable
{
    private CanvasController canvasController;
    private CharacterStats characterStats;

    public string characterName;
    [SerializeField]
    public List<ConversationString> conversationStrings;

    private void Start()
    {
        base.Init();
        canvasController = FindObjectOfType<CanvasController>();
        characterStats = GetComponentInParent<CharacterStats>();
    }

    // Interactuación con el personaje
    public override void Interact()
    {

        List<string> s = new List<string>();

        foreach (ConversationString cString in conversationStrings)
            s.Add(cString.GetTextByCurrentLanguage());

        canvasController.ShowConversation(2, characterName, s);

    }

    // Si el trigger es dado por el jugador se actualiza el color
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            ShowInteractableColor(true);
            canvasController.ShowInfoText("Speak with " + characterStats.nameCharacter);
        }
    }

    // Si hay más triggers ocupando el foko de interactable no hace nada, si no hay más se pone él.
    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals("Player"))
            if (other.GetComponentInParent<InputPlayerController>().interactable == null)
            {
                other.GetComponentInParent<InputPlayerController>().interactable = this;
                ShowInteractableColor(true);
                canvasController.ShowInfoText("Speak with " + characterStats.nameCharacter);
            }
    }

    // Reinica colores y estado del trigger
    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            ShowInteractableColor(false);
            canvasController.HideInfoText();
        }
    }

}
