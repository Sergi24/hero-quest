﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Controla los items recogibles
public class PickUpItem : Interactable
{

    private CanvasController canvasController;

    // transform donde se pone la malla del prefab del item
    public Transform itemParentTransform;
    // Item en cuestión
    public Item item;   // Item to put in the inventory if picked up

    private void Start()
    {

        canvasController = FindObjectOfType<CanvasController>();

        GameObject gO = Instantiate(item.prefab);
        gO.transform.parent = itemParentTransform;
        gO.transform.position = itemParentTransform.position;
        gO.transform.rotation = itemParentTransform.rotation;

        meshRenderer = gO.GetComponent<MeshRenderer>();

        base.Init();
    }

    // Interactuación
    public override void Interact()
    {
        base.Interact();

        PickUp();
    }

    // Coge el item
    void PickUp()
    {
        canvasController.HideInfoText();
        // Añade al inventario
        InventoryManager.instance.Add(item);
        // Informa al EventManagere
        EventManager.ItemCollected(item);
        // Destruye el item.
        Destroy(gameObject);    // Destroy item from scene
    }

    private void OnTriggerEnter(Collider other)
    {
        ShowInteractableColor(true);
        canvasController.ShowInfoText(item.name);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponentInParent<InputPlayerController>().interactable == null)
        {
            other.GetComponentInParent<InputPlayerController>().interactable = this;
            ShowInteractableColor(true);
            canvasController.ShowInfoText(item.name);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        ShowInteractableColor(false);
        canvasController.HideInfoText();
    }

}
