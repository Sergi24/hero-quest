﻿using UnityEngine;

// Controla la interactabilidadd del jugador
public class PlayerInteractionTrigger : MonoBehaviour
{
    public InputPlayerController inputPlayerController;

    // Comprueba si el colisionador del trigger es un interactable.
    private void OnTriggerEnter(Collider other)
    {
        inputPlayerController.canInteract = true;

        inputPlayerController.interactable = other.GetComponent<Interactable>();
    }

    // Actualiza el interactable por si hay uno que se ha recogido 
    private void OnTriggerStay(Collider other)
    {
        if (inputPlayerController.interactable == null)
            inputPlayerController.interactable = other.GetComponent<Interactable>();
    }

    // Al salir, se vacia el interactable si no hay más interactables
    private void OnTriggerExit(Collider other)
    {
        inputPlayerController.interactable = null;
    }

}
