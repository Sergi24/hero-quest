﻿using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System;

[SerializeField]
[Serializable]
[XmlRoot("Profile")]
// Guarda toda la información del perfil del jugador actual para poder serializar el juego
public class Profile
{
    [XmlElement("idProfile")]
    public string idProfile;

    [XmlElement("PlayerInfo")]
    public PlayerInfo playerInfo;

    [XmlElement("CurrentScene")]
    public string CurrentScene;
    [XmlElement("LastScene")]
    public string LastScene;

    [XmlElement("CurrentZone")]
    public string CurrentZone;


    [XmlElement("TimePlayed")]
    public int TimePlayed;

    [XmlElement("InventoryInfo")]
    public InventoryInfo inventory;

    [XmlElement("EquipmentInfo")]
    public EquipmentInfo[] equipmentInfo;

    // Quests
    [XmlElement("CurrentQuests")]
    public List<Quest> CurrentQuests;

    [XmlElement("CompletedQuests")]
    public List<Quest> CompletedQuests;

    public Profile()
    {
        inventory = new InventoryInfo();
        CurrentQuests = new List<Quest>();
        CompletedQuests = new List<Quest>();
        playerInfo = new PlayerInfo();

        int numSlots = Enum.GetNames(typeof(EquipmentSlot)).Length;
        equipmentInfo = new EquipmentInfo[numSlots];

    }

    // Actualiza el perfil
    public void UpdateProfile()
    {
        // Update Player Stats
        UpdatePlayerInfo();

        // Update Equipment
        Equipment[] currentEquipment = EquipmentManager.instance.currentEquipment;

        bool b = false;
        foreach (Equipment e in currentEquipment)
            if (e != null)
                b = true;
        if (b)
            UpdateEquipment(currentEquipment);

        // Update Inventory
        Inventory inventory = InventoryManager.instance.inventory;
        if (inventory != null)
            UpdateInventory(InventoryManager.instance.inventory);

    }

    // Actualiza el inventario
    public void UpdateInventory(Inventory inventory)
    {
        this.inventory = new InventoryInfo
        {
            space = inventory.space
        };

        foreach (Item item in inventory.items)
        {

            ItemInfo itemInfo = new ItemInfo
            {
                idItem = item.idItem
            };

            Debug.Log("itemInfoSaved: " + itemInfo.idItem);

            this.inventory.items.Add(itemInfo);
        }
    }

    // Obtiene el inventario del perfil
    public Inventory GetInventory()
    {
        //Debug.Log("GetInventory()");

        Inventory inventory = new Inventory();

        Item[] items = (Item[])Resources.FindObjectsOfTypeAll(typeof(Item));

        foreach (ItemInfo itemInfo in this.inventory.items)
        {
            //Debug.Log("ItemInfo: " + itemInfo.idItem);

            foreach (Item item in items)
            {
                //Debug.Log("Item: " + item.idItem);
                if (itemInfo.idItem == item.idItem)
                {
                    //Debug.Log("Item found: " + item.idItem);
                    inventory.items.Add(item);

                }

            }
        }

        return inventory;
    }

    // Actualiza el equipamiento
    public void UpdateEquipment(Equipment[] currentEquipment)
    {

        int numSlots = System.Enum.GetNames(typeof(EquipmentSlot)).Length;
        this.equipmentInfo = new EquipmentInfo[numSlots];

        for (int i = 0; i < currentEquipment.Length; i++)
        {
            if (currentEquipment[i] != null)
                equipmentInfo[i] = new EquipmentInfo
                {

                    idItem = currentEquipment[i].idItem
                };
        }
    }

    // Obtiene el equipamiento del perfil
    public Equipment[] GetcurrentEquipment()
    {
        //Debug.Log("GetcurrentEquipment()");

        int numSlots = Enum.GetNames(typeof(EquipmentSlot)).Length;
        Equipment[] currentEquipment = new Equipment[numSlots];

        if (equipmentInfo != null)
            if (equipmentInfo.Length > 0)
            {
                for (int i = 0; i < this.equipmentInfo.Length; i++)
                {
                    if (equipmentInfo[i] != null)
                    {
                        //Debug.Log("EquipmentInfo: " + equipmentInfo[i].idItem);
                        Equipment equipment = (Equipment)ResourcesHelper.GetItemById(equipmentInfo[i].idItem);
                        currentEquipment[i] = equipment;
                    }
                }
            }

        return currentEquipment;
    }

    // Actualiza la información del personaje en el perfil
    public void UpdatePlayerInfo()
    {
        PlayerStats playerStats = Transform.FindObjectOfType<PlayerStats>();
        playerInfo.namePlayer = playerStats.nameCharacter;
        playerInfo.CurrentHealth = playerStats.CurrentHealth;
        playerInfo.currentMana = playerStats.currentMana;
        playerInfo.Level = playerStats.Level;
        playerInfo.currentExperience = playerStats.currentExperience;
    }

    // Obtiene la información del personaje del perfil.
    public PlayerInfo GetPlayerInfo()
    {
        return playerInfo;
    }

}
