﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
[SerializeField]
// Guarda los items del inventario para poder serializarlos
public class InventoryInfo
{
    public int space;

    public List<ItemInfo> items;

    public InventoryInfo()
    {
        space = 20;
        items = new List<ItemInfo>();
    }

}