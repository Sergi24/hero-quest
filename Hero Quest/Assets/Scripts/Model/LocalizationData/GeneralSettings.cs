﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[SerializeField]
[Serializable]
// Guarda todos los datos generales como el volumen de los audiomixers, opciones gráficas y el lenguaje actual
public class GeneralSettings
{
    public string Language;

    public int Quality;
    public int Resolution;
    public bool FullScreen;

    public float MainVolume;
    public float MusicVolume;
    public float SfxVolume;

    public GeneralSettings()
    {
    }

    public void Init()
    {
        Debug.Log("Init General Settings");

        // Initialize Graphics
        SetQuality(Quality);
        SetFullScreen(FullScreen);
        SetResolution(Resolution);

    }

    // Actualiza la calidad gráfica
    public void SetQuality(int qualityIndex)
    {
        this.Quality = qualityIndex;
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    // Activa/Desactiva el modo Pantalla completa
    public void SetFullScreen(bool isFullScreen)
    {
        this.FullScreen = isFullScreen;
        Screen.fullScreen = isFullScreen;
    }

    // Actualiaz la resolución
    public void SetResolution(int resolutionIndex)
    {
        this.Resolution = resolutionIndex;

        Resolution[] resolutions = Screen.resolutions;
        Resolution resolution = resolutions[resolutionIndex];

        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

}
