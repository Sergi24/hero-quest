﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[SerializeField]
[Serializable]
// Guarda información del jugador para poder serializarlo.
public class PlayerInfo
{
    public string namePlayer;

    public int CurrentHealth;
    public int currentMana;

    public int Level;

    public int currentExperience;

}
