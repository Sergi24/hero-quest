﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
[SerializeField]
// Guarda el id del item del inventario para poder serializarlo
public class ItemInfo
{
    public int idItem;
}
