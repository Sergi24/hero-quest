﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[SerializeField]
[Serializable]
// Objetivo de asesinato de un tipo de objetivo.
public class KillObjective : Objective
{
    // id del objetivo
    public int idCharacter;

    public KillObjective()
    {

    }

    public KillObjective(int idQuest, int idObjective, int enemyID, string enDescription, string esDescription, string caDescription, bool completed, int currentAmount, int requiredAmount)
    {
        this.idQuest = idQuest;
        this.idObjective = idObjective;
        this.idCharacter = enemyID;
        this.EnDescription = enDescription;
        this.EsDescription = esDescription;
        this.CaDescription = caDescription;
        this.Completed = completed;
        this.CurrentAmount = currentAmount;
        this.RequiredAmount = requiredAmount;
    }

    // Inicialización de objetivo
    public override void Init()
    {
        base.Init();
        // Añadimos el listener al que queremos escuchar
        if (Completed == false)
            EventManager.OnCharacterDead += ObjectiveDead;
    }

    // Verifica si cuando un objetivo muere es el id objetivo y si lo es aumenta el contador de asesinatos del objetivo.
    void ObjectiveDead(GameObject characterDead, CharacterStats killerCharacterStats)
    {
        Debug.Log("ObjectiveDead()");
        if (characterDead.GetComponent<CharacterStats>().idCharacter == this.idCharacter && killerCharacterStats.tag.Equals("Player"))
        {
            Debug.Log("Quest enemy dead: " + idCharacter);
            CurrentAmount++;
            if (CurrentAmount >= RequiredAmount)
                CurrentAmount = RequiredAmount;

            if (Evaluate())
            {
                // Finalizamos el listener aqui 
                EventManager.OnCharacterDead -= ObjectiveDead;
            }

            EventManager.ObjectiveUpdated();

            CanvasController canvasController = GameObject.FindObjectOfType<CanvasController>();
            canvasController.ShowAlertText(2, Color.yellow, characterDead.GetComponent<CharacterStats>().nameCharacter + ": " + CurrentAmount + "/" + RequiredAmount);
        }
    }

    // Finaliza el objetivo
    public override void FinishObjective()
    {
        Debug.Log("FinishKillObjective()");
    }

}
