﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Xml.Serialization;
using Lean.Localization;

[XmlInclude(typeof(KillObjective))]
[XmlInclude(typeof(CollectionObjective))]
[SerializeField]
[Serializable]
// Clase padre de objetivo
public class Objective
{
    // id de la misión
    [SerializeField]
    public int idQuest;

    // id del objetivo
    public int idObjective;

    // descripciones
    [TextArea]
    public string EnDescription, EsDescription, CaDescription;

    // Está completado?
    public bool Completed;

    // Montón necesario y actual
    public int CurrentAmount;
    public int RequiredAmount;

    public Objective()
    {

    }

    public Objective(int quest, int idObjective, string enDescription, string esDescription, string caDescription, bool completed, int currentAmount, int requiredAmount)
    {
        idQuest = quest;
        this.idObjective = idObjective;
        EnDescription = enDescription;
        EsDescription = esDescription;
        CaDescription = caDescription;
        Completed = completed;
        CurrentAmount = currentAmount;
        RequiredAmount = requiredAmount;
    }

    public virtual void Init()
    {

    }

    // Comprueba si el objetivo está completado
    public bool Evaluate()
    {
        if (CurrentAmount >= RequiredAmount)
        {
            Complete();
            return true;
        }
        return false;
    }

    // Verifica si la misión está completa
    public void Complete()
    {
        Quest quest = null;

        foreach (Quest currentQuest in GameManager.instance.Profile.CurrentQuests)
        {
            if (idQuest == currentQuest.idQuest)
            {
                quest = currentQuest;
            }
        }

        Completed = true;
        quest.CheckObjectives();
        //Debug.Log("Objective marked as completed.");
    }

    // Finaliza el objetivo
    public virtual void FinishObjective()
    {
        Debug.Log("BASE: FinishObjective()");
    }

    // Devuelve la descripción en el idioma actual
    public string GetDescriptionByLanguage()
    {
        string language = LeanLocalization.CurrentLanguage;

        if (language.Equals("English"))
            return EnDescription;
        else if (language.Equals("Spanish"))
            return EsDescription;
        else if (language.Equals("Catalan"))
            return CaDescription;

        return null;
    }

    // Completa el objetivo
    public void CompleteObjective()
    {
        CurrentAmount = RequiredAmount;
        Complete();
    }

}
