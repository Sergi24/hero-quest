﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Xml.Serialization;
using Lean.Localization;

// Necesario para serializar todas las misiones...
////////////////////////////////////////

[XmlInclude(typeof(SpeakWithTheGoddess))]
[XmlInclude(typeof(FindPumpkinsQuest))]
[XmlInclude(typeof(LarryQuest))]
[XmlInclude(typeof(KillEsqueletonQuest))]

////////////////////////////////////////

[SerializeField]
[Serializable]
// Clase padre para hacer misiones
public abstract class Quest
{
    // id de la misión
    public int idQuest;

    // Titulos y descripciones
    public string EnTitle;
    public string EnDescription;

    public string EsTitle;
    public string EsDescription;

    public string CaTitle;
    public string CaDescription;

    // Recompensas
    public int ExperienceReward;
    public List<ItemInfo> ItemsReward;

    // id de las misiones necesarias para desbloquear esta
    public List<int> idQuests;

    // Objetivos
    public List<Objective> Objectives;

    // Está la misión completada?
    public bool completed = false;

    public Quest()
    {

    }

    // Inicializa los objetivos de la misión
    public void InitObjectives()
    {
        foreach (Objective o in Objectives)
            o.Init();
    }

    // Verifica si los objetivos están cumplidos
    public void CheckObjectives()
    {
        bool questCompleted = true;

        // Si todos los objetivos han sido cumplidos...
        foreach (Objective objective in Objectives)
        {
            //Debug.Log("Objective " + objective.EnDescription + ": " + objective.Completed);
            if (objective.Completed == false)
            {
                questCompleted = false;
                break;
            }
        }

        // Se completa o no la misión y se informa al EventManager.
        if (questCompleted)
        {
            this.completed = true;
            EventManager.QuestCompleted();
        }
        else
            this.completed = false;

    }

    // Finaliza los objetivos
    public void EndObjectives()
    {
        foreach (Objective o in Objectives)
        {
            o.FinishObjective();
        }
    }

    // Da la recompensa de misión
    public void GiveReward()
    {
        Debug.Log("GiveReward()");
        if (ItemsReward.Count > 0)
        {
            foreach (ItemInfo itemInfo in ItemsReward)
            {
                Item item = ResourcesHelper.GetItemById(itemInfo.idItem);
                InventoryManager.instance.Add(item);
            }
        }

        PlayerStats playerStats = Transform.FindObjectOfType<PlayerStats>();

        playerStats.GetExperience(null, ExperienceReward);

    }

    // Devuelve el titulo en el idioma actual
    public string GetCurrentLanguageTitle()
    {
        string language = LeanLocalization.CurrentLanguage;

        if (language.Equals("English"))
            return EnTitle;
        else if (language.Equals("Spanish"))
            return EsTitle;
        else if (language.Equals("Catalan"))
            return CaTitle;

        return null;
    }

    // Devuelve la descripción en el idioma actual
    public string GetCurrentDescriptionTitle()
    {
        string language = LeanLocalization.CurrentLanguage;

        if (language.Equals("English"))
            return EnDescription;
        else if (language.Equals("Spanish"))
            return EsDescription;
        else if (language.Equals("Catalan"))
            return CaDescription;

        return null;
    }

}
