﻿using UnityEngine;

// Objetivo de recolección, hereda de objetivo y mediante un id de item controla la obtención del nº de item necesario para completar el objetivo.
public class CollectionObjective : Objective
{
    // id del item objetivo
    public int idItem;

    public CollectionObjective()
    {

    }

    public CollectionObjective(int idQuest, int idObjective, int idItem, string enDescription, string esDescription, string caDescription, bool completed, int currentAmount, int requiredAmount)
    {
        this.idQuest = idQuest;
        this.idObjective = idObjective;
        this.idItem = idItem;
        this.EnDescription = enDescription;
        this.EsDescription = esDescription;
        this.CaDescription = caDescription;
        this.Completed = completed;
        this.CurrentAmount = currentAmount;
        this.RequiredAmount = requiredAmount;
    }

    // Inicializamos el objetivo
    public override void Init()
    {
        base.Init();
        // Avisamos al eventmanager que queremos ser avisados en caso de que cambie un item del inventario.
        EventManager.OnItemChanged += ItemChanged;
        // Verificamos si tenemos el item y la cantidad de este.
        CheckItemAmount();
    }

    // Checkea el item objetivo en nuestro inventario comprobando si tenemos los necesarios para completar el objetivo.
    public void CheckItemAmount()
    {
        CurrentAmount = 0;

        foreach (Item i in InventoryManager.instance.inventory.items)
        {
            if (i.idItem == idItem)
            {
                CurrentAmount++;
                if (CurrentAmount == RequiredAmount)
                {
                    break;
                }
            }

        }
        // Avisamos a EventManager de que hay un objetivo actualizado
        EventManager.ObjectiveUpdated();
    }

    // En caso de que un item cambie en el inventario, verifica si es le item objetivo y comprueba el montón, si tenemos el suficiente completa el objetivo
    void ItemChanged(Item item)
    {
        //Debug.Log("CollectionObjective item collected ids: " + item.idItem + ", Needed:" + idItem);
        if (item.idItem == idItem)
        {
            // Debug.Log("Quest item collected: " + item.name);

            CheckItemAmount();

            if (Evaluate())
            {

            }
            // Mostramos mensajito de que tenemos x/y items de misión
            CanvasController canvasController = GameObject.FindObjectOfType<CanvasController>();
            canvasController.ShowAlertText(2, Color.yellow, item.name + ": " + CurrentAmount + "/" + RequiredAmount);
        }
    }

    // Finaliza el objetivo borrando de la lista los eventlisteners que ya no se usarán.
    public override void FinishObjective()
    {
        Debug.Log("FinishObjective()");

        EventManager.OnItemChanged -= ItemChanged;

        // Borramos la cantidad de items necesarios para finalizar la misión.
        Item itemToReclaim = ResourcesHelper.GetItemById(idItem);

        for (int i = 0; i < RequiredAmount; i++)
        {
            InventoryManager.instance.Remove(itemToReclaim);
        }

    }



}
