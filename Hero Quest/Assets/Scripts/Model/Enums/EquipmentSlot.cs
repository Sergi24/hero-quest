﻿// enum que define el tipo de slots equipables que tiene el personaje del jugador.
public enum EquipmentSlot
{
    weapon,
    shield
}