﻿// Enum que define los diferentes tipos de magia que hay

public enum MagicTypes
{
    Fire,
    Water,
    Life,
    Light,
    Shadows,
    Normal
}
