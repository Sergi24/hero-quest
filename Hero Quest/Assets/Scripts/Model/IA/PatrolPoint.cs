﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
// Punto de patrulla que utiliza las IA al patrullar
public class PatrolPoint : MonoBehaviour
{
    // Hay alguien viniendo?
    public bool isCharacterComming;
    // Tiempo de espera en punto antes de proseguir al siguiente punto.
    public float timeWaiting;

}
