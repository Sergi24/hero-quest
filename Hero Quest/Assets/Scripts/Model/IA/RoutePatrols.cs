﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
// Conjunto de Patrol points que forman una ruta de patrulla.
public class RoutePatrols
{
    public List<PatrolPoint> route;
}
