﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Advice", menuName = "Advices/New Advice")]
// ScriptableObject que guarda la información de un consejo del menú de carga
public class Advice : ScriptableObject
{
    // Imagen de preferencia del consejo
    public Sprite preferredSprite;

    // Titulos y descripciones del consejo
    public string enTitle;
    [TextArea]
    public string enText;

    public string esTitle;
    [TextArea]
    public string esText;

    public string caTitle;
    [TextArea]
    public string caText;

}
