﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
// Guarda la conversación de uno de los tipos de conversación de una conversación de misión.
public class QuestConversation
{
    // Define si se completa algún objetivo al completar la conversación.
    public bool completeObjectiveOnFinish = false;
    // id del objetivo a completar si se completa uno.
    public int objectiveId;
    // Sentencias de la conversación
    public List<ConversationString> sentences;
}
