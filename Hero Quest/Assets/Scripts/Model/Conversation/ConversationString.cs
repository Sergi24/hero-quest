﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Localization;
using System;

[Serializable]
[SerializeField]
// Guarda las strings de los diferentes idiomas.
public class ConversationString
{
    [TextArea]
    public string enText;
    [TextArea]
    public string esText;
    [TextArea]
    public string caText;

    public string GetTextByCurrentLanguage()
    {
        string currentLanguage = LeanLocalization.CurrentLanguage;

        if (currentLanguage.Equals("English"))
            return enText;
        else if (currentLanguage.Equals("Spanish"))
            return esText;
        else if (currentLanguage.Equals("Catalan"))
            return caText;

        return null;
    }

}
