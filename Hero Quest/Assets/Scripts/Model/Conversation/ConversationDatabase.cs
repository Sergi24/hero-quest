﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Conversation Database", menuName = "Database/Conversation Database")]
// Guarda todas las conversaciones de unua misión
public class ConversationDatabase : ScriptableObject
{

    public QuestConversation startQuestConversation;
    public QuestConversation NotStartableQuestConversation;
    public QuestConversation incompleteQuestConversation;
    public QuestConversation readyToCompleteQuestConversation;
    public QuestConversation completedQuestConversation;

}
