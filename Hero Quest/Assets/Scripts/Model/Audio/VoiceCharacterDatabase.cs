﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "New Voice Character Database", menuName = "Database/Voices Character Database")]

// Clase ScriptableObject que guarda los clips de voz de un personaje determinado. Se controlan mediante AudioCharacterController
public class VoiceCharacterDatabase : ScriptableObject
{
    // Clips de cada estado del personaje.
    public List<AudioClip> usualClips;
    public List<AudioClip> attackClips;
    public List<AudioClip> huntingClips;
    public List<AudioClip> hurtClips;
    public List<AudioClip> deadClips;
    public List<AudioClip> screamClips;

}