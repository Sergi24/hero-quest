﻿// Script: FootStepsDatabase.cs
// Descripción: Guarda sonidos para ser reproducidos por FootStepsController.cs
// Autor: Sergi Martínez Balsells
// Fecha: 15/10/2018
// Licencia: Dominio público

using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "New FootStepsDatabase", menuName = "Database/Foot Steps Database")]
// Guarda los sonidos de pisadas en materiales físicos y terrenos de textura.
public class FootStepsDatabase : ScriptableObject
{

    public List<PhysicMaterialSoundSteps> physicMaterials;
    public List<TextureSoundSteps> terrainTextures;

}

[Serializable]
[SerializeField]
public class PhysicMaterialSoundSteps
{
    public PhysicMaterial physicMaterial;
    public List<AudioClip> audioclips;
}

[Serializable]
public class TextureSoundSteps
{
    public Texture texture;
    public List<AudioClip> audioclips;
}