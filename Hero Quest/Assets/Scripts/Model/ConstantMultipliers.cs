﻿// Variable con constantes  que se aplican a diferentes partes del juego. Sobretodo en el combate y estados
public static class ConstantMultipliers
{
    // Player exp
    public const int baseExperienceLevel = 100;
    public const float extraExperienceNeededByLevelMultiplier = 2.5f;

    // Monsters exp
    public const int baseExperienceLevelGiven = 100;
    public const float extraExperienceByLevelMultiplier = 1.5f;

    // Character Attributes
    public const int strenghExtraByLevelPoint = 3;
    public const int agilityExtraByLevelPoint = 2;
    public const int intelligenceExtraByLevelPoint = 1;

    public const float damageExtraByStrenghPointn = 1;
    public const float lifeExtraByStrenghPoint = 3;
    public const float armorExtraByAgilityPoint = 2;
    public const float manaExtraByIntelligencePoint = 1;

    public const float extraLifeRegenerationByStrenghPoint = .08f;
    public const float extraManaRegenerationByStrenghPoint = .05f;

    // Damage TYPES
    // Life
    public const float lifeOverFireMultiplier = .5f;
    public const float lifeOverWaterMultiplier = 2f;
    public const float lifeOverLifeMultiplier = .5f;
    public const float lifeOverOthersMultiplier = 1f;

    // Fire
    public const float fireOverLifeMultiplier = 2f;
    public const float fireOverWaterMultiplier = .5f;
    public const float fireOverFireMultiplier = .5f;
    public const float fireOverOthersMultiplier = 1f;

    // Water
    public const float waterOverLifeMultiplier = .5f;
    public const float waterOverWaterMultiplier = .5f;
    public const float waterOverFireMultiplier = 2f;
    public const float waterOverOthersMultiplier = 1f;

    // Light
    public const float lightOverLightMultiplier = .5f;
    public const float lightOverShadowsMultiplier = 2f;
    public const float lightOverOthersMultiplier = .5f;

    // Shadows
    public const float shadowsOverLightMultiplier = 2f;
    public const float shadowsOverShadowsMultiplier = .5f;
    public const float shadowsOverOthersMultiplier = 1f;

    // Normal
    public const float normalOverEverything = 1f;

}
