﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[SerializeField]
[Serializable]
public class CharacterStats : MonoBehaviour
{

    protected CanvasController canvasController;
    protected InputPlayerController inputPlayerController;
    protected AICharacterController aiCharacterController;

    // id del personaje
    public int idCharacter;
    // nombre del personaje
    public string nameCharacter;
    // nivel del personaje
    public int Level;

    // vida y mana actuales
    public int CurrentHealth;
    public int currentMana;

    // Stats del personaje
    public Stat maxHealth;
    public Stat maxMana;

    public Stat strengh;
    public Stat agility;
    public Stat intelligence;

    public Stat damage;
    public Stat armor;

    public Stat lifeRegeneration;
    public Stat manaRegeneration;

    // Not implemented
    public MagicTypes typeDamage;

    // Localización del transform de armado y desarmado del personaje.
    public GameObject unarmedWeapon;
    public GameObject armedWeapon;

    public GameObject unarmedShield;
    public GameObject armedShield;

    // Contador de tiempo para la regeneración de vida y mana
    protected float timeCounter;

    // Estado del personaje, si se le ignora o está muerto o en combate
    public bool ignoreCharacter = false;
    public bool dead;
    public bool inCombat;
    public bool isAddedToCombatMusic = false;

    // Level anterior
    protected int oldLevel;
    // Experiencia que proporciona el personaje al morir
    public int experience = 100;

    // Prefab e items que puede droppear el personaje al morir
    public GameObject PickUpItemPrefab;
    public List<DropItem> droppableItems;

    private void Awake()
    {

    }

    protected void Start()
    {
        canvasController = FindObjectOfType<CanvasController>();
        inputPlayerController = GetComponent<InputPlayerController>();
        aiCharacterController = GetComponent<AICharacterController>();

        timeCounter = 0;
        Init();
    }

    // Inicializa los stats del personaje
    public void Init()
    {

        maxHealth.RemoveModifier((int)(strengh.GetValue() * ConstantMultipliers.lifeExtraByStrenghPoint));
        maxMana.RemoveModifier((int)(intelligence.GetValue() * ConstantMultipliers.manaExtraByIntelligencePoint));

        lifeRegeneration.RemoveModifier((int)(strengh.GetValue() * ConstantMultipliers.extraLifeRegenerationByStrenghPoint));
        manaRegeneration.RemoveModifier((int)(intelligence.GetValue() * ConstantMultipliers.extraManaRegenerationByStrenghPoint));

        damage.RemoveModifier((int)(strengh.GetValue() * ConstantMultipliers.damageExtraByStrenghPointn));
        armor.RemoveModifier((int)(agility.GetValue() * ConstantMultipliers.armorExtraByAgilityPoint));

        strengh.RemoveModifier(oldLevel * ConstantMultipliers.strenghExtraByLevelPoint);
        agility.RemoveModifier(oldLevel * ConstantMultipliers.agilityExtraByLevelPoint);
        intelligence.RemoveModifier(oldLevel * ConstantMultipliers.intelligenceExtraByLevelPoint);

        // Add modifiers

        strengh.AddModifier(Level * ConstantMultipliers.strenghExtraByLevelPoint);
        agility.AddModifier(Level * ConstantMultipliers.agilityExtraByLevelPoint);
        intelligence.AddModifier(Level * ConstantMultipliers.intelligenceExtraByLevelPoint);

        maxHealth.AddModifier((int)(strengh.GetValue() * ConstantMultipliers.lifeExtraByStrenghPoint));

        lifeRegeneration.AddModifier((int)(strengh.GetValue() * ConstantMultipliers.extraLifeRegenerationByStrenghPoint));

        damage.AddModifier((int)(strengh.GetValue() * ConstantMultipliers.damageExtraByStrenghPointn));
        armor.AddModifier((int)(agility.GetValue() * ConstantMultipliers.armorExtraByAgilityPoint));

        if (!GetType().Equals(typeof(PlayerStats)))
        {
            CurrentHealth = maxHealth.GetValue();
        }


        if (maxMana.baseValue != 0)
        {
            maxMana.AddModifier((int)(intelligence.GetValue() * ConstantMultipliers.manaExtraByIntelligencePoint));
            manaRegeneration.AddModifier((int)(intelligence.GetValue() * ConstantMultipliers.extraManaRegenerationByStrenghPoint));
            currentMana = maxMana.GetValue();
        }

    }

    protected void Update()
    {
        timeCounter += Time.deltaTime;
        if (timeCounter >= 1 && !dead)
        {
            if (CurrentHealth + lifeRegeneration.GetValue() > maxHealth.GetValue())
                CurrentHealth = maxHealth.GetValue();
            else
                CurrentHealth += lifeRegeneration.GetValue();

            if (currentMana + manaRegeneration.GetValue() > currentMana)
                currentMana = maxMana.GetValue();
            else
                currentMana += manaRegeneration.GetValue();

            timeCounter = 0;
        }
        PreventXZAxisRotation();
    }


    // Ejecuta la obtención de un daño por medio de un enemigo
    public void TakeDamage(CharacterStats characterStatsOrigin)
    {
        // si se golpea a un ignorecharacter por casualidad se ignora el ataque
        if (!ignoreCharacter)
        {
            // Se verifica si tiene escudo y si el objetivo es un npc o el jugador.
            ShieldStats shieldStats = null;
            if (inputPlayerController != null)
                shieldStats = inputPlayerController.shieldStats;
            else if (aiCharacterController != null)
            {
                shieldStats = aiCharacterController.shieldStats;
                if (aiCharacterController.currentIAState == AICharacterController.IAState.Hunting)
                    aiCharacterController.SetTarget(characterStatsOrigin.transform);
                else
                    aiCharacterController.StartHunting(characterStatsOrigin.transform);
            }

            // Si tiene escudo se verifica si ha bloqueado el ataque...
            if (shieldStats != null)
            {
                float n = UnityEngine.Random.Range(0, 100);
                //Debug.Log("The character have shield: " + shieldStats.blockProbability + ", " + n);
                if (shieldStats.blockProbability > n)
                {
                    canvasController.ShowFloatingText(transform, Color.white, "Blocked!");
                    if (GetComponent<InputPlayerController>() != null)
                        GetComponent<InputPlayerController>().Blocked();
                    else if (GetComponent<AICharacterController>() != null)
                        GetComponent<AICharacterController>().Blocked();
                }
                // Si no lo bloquea se obtiene el daño
                else
                {
                    GetDamage(characterStatsOrigin);
                }
            }
            else
            {
                GetDamage(characterStatsOrigin);
            }
        }
    }

    // Procede a restar la salud pertinente y llamar al controlador de animaciones pertinente para ejecutar la animación de daño o muerte
    private void GetDamage(CharacterStats characterStatsOrigin)
    {
        int finalDamage = CombatManager.instance.GetFinalDamage(characterStatsOrigin, this);

        canvasController.ShowFloatingText(transform, Color.white, finalDamage + "");

        CurrentHealth -= finalDamage;



        if (CurrentHealth <= 0)
        {
            CurrentHealth = 0;

            Die(characterStatsOrigin);
        }
        else
        {
            if (GetComponent<InputPlayerController>() != null)
                GetComponent<InputPlayerController>().Hurt();
            else if (GetComponent<AICharacterController>() != null)
                GetComponent<AICharacterController>().Hurt();
        }
    }

    // Se ejecuta cuando el personaje ha muerto
    public virtual void Die(CharacterStats characterStatsKiller)
    {

        // Local Character
        if (GetComponent<InputPlayerController>() != null)
        {
            //Debug.Log("LOCAL: InputPlayerController: " + GetComponent<AICharacterController>());

            GetComponent<InputPlayerController>().Die();
            GetComponent<Rigidbody>().useGravity = false;
            GetComponent<Collider>().enabled = false;
            GetComponent<CharacterStats>().dead = true;
            canvasController.ActivateLoseMenu();
        }
        else if (GetComponent<AICharacterController>() != null)
        {
            //Debug.Log("LOCAL: AICharacterController: " + GetComponent<AICharacterController>());
            GetComponent<AICharacterController>().Die();
            GetComponent<Rigidbody>().useGravity = false;
            GetComponent<Collider>().enabled = false;
            GetComponent<AICharacterController>().currentIAState = AICharacterController.IAState.Die;
            GetComponent<AICharacterController>().target = null;
            EndCombat(characterStatsKiller.transform);
            DropDroppableItems();
        }

        // Killer Character
        if (characterStatsKiller.GetComponent<AICharacterController>() != null)
        {
            characterStatsKiller.GetComponent<AICharacterController>().StartReturning();
        }
        if (characterStatsKiller.GetComponent<InputPlayerController>() != null)
        {
            int finalExperience = (int)(experience * Level * ConstantMultipliers.extraExperienceByLevelMultiplier);
            characterStatsKiller.GetComponent<PlayerStats>().GetExperience(transform, finalExperience);
        }

        // Se avisa al EventManager de que ha muerto un personaje
        EventManager.CharacterDead(gameObject, characterStatsKiller);
    }

    // Verifica si hay items droppeables en el perosnaje y si los hay mira si sale el porcentaje para dropearlo.
    public void DropDroppableItems()
    {
        //Debug.Log("DropDroppableItems()");
        if (droppableItems.Count != 0)
        {
            foreach (DropItem dropItem in droppableItems)
            {
                float n = UnityEngine.Random.Range(0, 100);

                if (dropItem.percentage > n)
                {

                    GameObject newPickUpItem = Instantiate(PickUpItemPrefab, transform.position + Vector3.up, Quaternion.identity);
                    newPickUpItem.GetComponent<PickUpItem>().item = dropItem.item;

                }

            }
        }
    }

    // Empieza un combate, la musica de combate aumenta su marcador si el objetivo es player
    public void StartCombat(Transform target)
    {
        //Debug.Log("StartCombat(): " + transform.tag);
        if (target.tag.Equals("Player") && !isAddedToCombatMusic)
        {
            SoundManager.instance.charactersAttackingPlayer++;
            inCombat = true;
            isAddedToCombatMusic = true;
        }
    }
    // Finaliza un combate, la musica de combate disminuye un marcador si el objetivo es player
    public void EndCombat(Transform target)
    {
        //Debug.Log("StartCombat(): " + transform.tag);
        if (target.tag.Equals("Player"))
        {
            SoundManager.instance.charactersAttackingPlayer--;
            inCombat = false;
            isAddedToCombatMusic = false;
        }
    }

    // Previene de rotación indeseada
    private void PreventXZAxisRotation()
    {
        Quaternion q = transform.rotation;
        q.eulerAngles = new Vector3(0, q.eulerAngles.y, 0);
        transform.rotation = q;
    }

}
