﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
[SerializeField]
// Guarda stats como fuerza, agilidad y demás con la posibilidad de aumentarlos o disminuirlos mediante modificadores.
public class Stat
{
    // Valor base
    [SerializeField]
    public int baseValue;

    // Modificadores
    private List<int> modifiers = new List<int>();

    // Devuelve el valor con modificadores añadidos
    public int GetValue()
    {
        int finalValue = baseValue;
        modifiers.ForEach(x => finalValue += x);
        return finalValue;
    }
    // Añade un modificador
    public void AddModifier(int modifier)
    {
        if (modifier != 0)
        {
            modifiers.Add(modifier);
        }
    }

    // Borra un modificador
    public void RemoveModifier(int modifier)
    {
        if (modifier != 0)
        {
            modifiers.Remove(modifier);
        }
    }

}