﻿using UnityEngine;

// Contiene las estadísticas del escudo
public class ShieldStats : MonoBehaviour
{
    // Probabilidad de bloquear un ataque
    [Range(0, 100)]
    public float blockProbability = 20;

}
