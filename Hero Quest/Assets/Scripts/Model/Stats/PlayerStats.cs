﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[SerializeField]
[Serializable]
public class PlayerStats : CharacterStats
{
    // Experiencia actual y necesaria para subir de nivel
    public int neededExperience;
    public int currentExperience;

    protected new void Start()
    {
        Debug.Log("Start playerStats");
        PlayerInfo playerInfo = GameManager.instance.Profile.playerInfo;
        nameCharacter = playerInfo.namePlayer;
        CurrentHealth = playerInfo.CurrentHealth;
        currentMana = playerInfo.currentMana;
        Level = playerInfo.Level;
        currentExperience = playerInfo.currentExperience;

        canvasController = FindObjectOfType<CanvasController>();
        inputPlayerController = GetComponent<InputPlayerController>();
        aiCharacterController = GetComponent<AICharacterController>();

        timeCounter = 0;

        EventManager.OnEquipmentChanged += OnEquipmentChanged;
        Init();
        EquipmentManager.instance.Init();
    }

    private new void Init()
    {
        oldLevel = Level;

        currentExperience -= neededExperience;
        neededExperience = (int)(ConstantMultipliers.baseExperienceLevel * Level * ConstantMultipliers.extraExperienceNeededByLevelMultiplier);
    }

    private new void Update()
    {
        base.Update();

        if (currentExperience >= neededExperience)
        {
            UpdateLevel();
            LevelUPUpdateModifiers();
        }

        if (Input.GetKeyDown(KeyCode.T))
            Testing();
    }

    // Actualiza level
    private void UpdateLevel()
    {
        int oldLevel = Level++;
        Debug.Log("Update level: old level: " + oldLevel + ", new level: " + Level);
        currentExperience -= neededExperience;
        neededExperience = (int)(ConstantMultipliers.baseExperienceLevel * Level * ConstantMultipliers.extraExperienceNeededByLevelMultiplier);
    }

    private void Testing()
    {
        currentExperience += 200;
    }

    // Actualiza modificadores al subir de nivel
    protected void LevelUPUpdateModifiers()
    {
        Debug.Log("LevelUPUpdateModifiers()");
        Color c = Color.yellow;
        canvasController.ShowAlertText(3, c, "Congratulations! You are now lv " + Level);
        RemoveOldModifiers();

        oldLevel = Level;

        AddNewModifiers();

        CurrentHealth = maxHealth.GetValue();
        currentMana = maxMana.GetValue();

        if (CurrentHealth > maxHealth.GetValue())
            CurrentHealth = maxHealth.GetValue();

        if (currentMana > maxMana.GetValue())
            currentMana = maxMana.GetValue();

    }

    // Borra modificadores antiguos
    private void RemoveOldModifiers()
    {
        maxHealth.RemoveModifier((int)(strengh.GetValue() * ConstantMultipliers.lifeExtraByStrenghPoint));
        maxMana.RemoveModifier((int)(intelligence.GetValue() * ConstantMultipliers.manaExtraByIntelligencePoint));

        lifeRegeneration.RemoveModifier((int)(strengh.GetValue() * ConstantMultipliers.extraLifeRegenerationByStrenghPoint));
        manaRegeneration.RemoveModifier((int)(intelligence.GetValue() * ConstantMultipliers.extraManaRegenerationByStrenghPoint));

        damage.RemoveModifier((int)(strengh.GetValue() * ConstantMultipliers.damageExtraByStrenghPointn));
        armor.RemoveModifier((int)(agility.GetValue() * ConstantMultipliers.armorExtraByAgilityPoint));

        strengh.RemoveModifier(oldLevel * ConstantMultipliers.strenghExtraByLevelPoint);
        agility.RemoveModifier(oldLevel * ConstantMultipliers.agilityExtraByLevelPoint);
        intelligence.RemoveModifier(oldLevel * ConstantMultipliers.intelligenceExtraByLevelPoint);
    }

    // Añade nuevos modificadores
    private void AddNewModifiers()
    {

        strengh.AddModifier(Level * ConstantMultipliers.strenghExtraByLevelPoint);
        agility.AddModifier(Level * ConstantMultipliers.agilityExtraByLevelPoint);
        intelligence.AddModifier(Level * ConstantMultipliers.intelligenceExtraByLevelPoint);

        maxHealth.AddModifier((int)(strengh.GetValue() * ConstantMultipliers.lifeExtraByStrenghPoint));
        maxMana.AddModifier((int)(intelligence.GetValue() * ConstantMultipliers.manaExtraByIntelligencePoint));

        lifeRegeneration.AddModifier((int)(strengh.GetValue() * ConstantMultipliers.extraLifeRegenerationByStrenghPoint));
        manaRegeneration.AddModifier((int)(intelligence.GetValue() * ConstantMultipliers.extraManaRegenerationByStrenghPoint));

        damage.AddModifier((int)(strengh.GetValue() * ConstantMultipliers.damageExtraByStrenghPointn));
        armor.AddModifier((int)(agility.GetValue() * ConstantMultipliers.armorExtraByAgilityPoint));

    }

    // Actualiza el armamento
    public void OnEquipmentChanged(Equipment newItem, Equipment oldItem)
    {

        RemoveOldModifiers();

        if (newItem != null)
        {
            maxHealth.AddModifier(newItem.maxHealthModifier);
            maxMana.AddModifier(newItem.maxManaModifier);

            armor.AddModifier(newItem.armorModifier);
            damage.AddModifier(newItem.damageModifier);

            lifeRegeneration.AddModifier(newItem.lifeRegenerationModifier);
            manaRegeneration.AddModifier(newItem.manaRegenerationModifier);

            strengh.AddModifier(newItem.strenghModifier);
            agility.AddModifier(newItem.agilityModifier);
            intelligence.AddModifier(newItem.intelligenceModifier);
        }

        if (oldItem != null)
        {
            maxHealth.RemoveModifier(oldItem.maxHealthModifier);
            maxMana.RemoveModifier(oldItem.maxManaModifier);

            armor.RemoveModifier(oldItem.armorModifier);
            damage.RemoveModifier(oldItem.damageModifier);

            lifeRegeneration.RemoveModifier(oldItem.lifeRegenerationModifier);
            manaRegeneration.RemoveModifier(oldItem.manaRegenerationModifier);

            strengh.RemoveModifier(oldItem.strenghModifier);
            agility.RemoveModifier(oldItem.agilityModifier);
            intelligence.RemoveModifier(oldItem.intelligenceModifier);
        }

        AddNewModifiers();

    }

    // Obtiene experiencia
    public void GetExperience(Transform transformEnemyDied, int experience)
    {
        this.currentExperience += experience;
        if (transformEnemyDied != null)
            canvasController.ShowFloatingText(transformEnemyDied, Color.magenta, "+" + experience + " XP");
        else
            canvasController.ShowFloatingText(transform, Color.magenta, "+" + experience + " XP");
    }

    // Consume un item consumible
    public void Consume(Consumable consumable)
    {
        CurrentHealth += consumable.healAmount;
        currentMana += consumable.manaAmount;
    }

    private void OnDestroy()
    {
        // Libera el listener
        EventManager.OnEquipmentChanged -= OnEquipmentChanged;
    }

}
