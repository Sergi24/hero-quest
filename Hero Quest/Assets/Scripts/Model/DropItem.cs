﻿using System;

[Serializable]
// Guarda el item droppeable y su probabilida de que se dropee o no
public class DropItem
{

    public Item item;
    public float percentage;

}
