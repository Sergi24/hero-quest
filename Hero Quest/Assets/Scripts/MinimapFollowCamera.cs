﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Controla que el minimapa no rote con el jugador sino que todo el rato esté en el mismo eje de rotación.
public class MinimapFollowCamera : MonoBehaviour
{
    public bool staticmap;

    private Camera cam;

    void Awake()
    {
        cam = GetComponent<Camera>();
    }

    void Update()
    {

        if (staticmap)
            cam.transform.rotation = Quaternion.Euler(new Vector3(90, 0, 0));


    }


}
