﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Controla el spawn de un personaje y le añade un material de la lista.
public class CharacterSpawner : MonoBehaviour
{
    // prefabs disponibles de los que elegir
    public GameObject[] charactersPrefab;

    // Materiales que se pueden aplicar a los prefabs
    public Material[] materials;

    // Spawned character
    public GameObject spawnedCharacter;

    // Level mínimo y máximo de spawneo del personaje
    public int minLevel;
    public int maxLevel;

    // Contador
    private float count;
    // Tiempo para volver a spawnear un personaje
    public int timeForNewSpawn;

    // Rutas disponibles para aplicar al personaje
    public List<RoutePatrols> patrolPoints;

    private void Start()
    {
        SpawnCharacter();
    }

    private void Update()
    {
        if (spawnedCharacter == null)
        {
            if (count > timeForNewSpawn)
            {
                SpawnCharacter();
            }
            count += Time.deltaTime;
        }

    }
    // Spawnea un personaje del listado de prefabs añadiendole un material aleatorio y si hay una ruta mínimo se la aplica y cambia estado a patrullando.
    private void SpawnCharacter()
    {
        int n = Random.Range(0, materials.Length);
        int level = Random.Range(minLevel, maxLevel + 1);
        int patrol = Random.Range(0, patrolPoints.Count);

        Material material = materials[n];

        n = Random.Range(0, charactersPrefab.Length);

        spawnedCharacter = Instantiate(charactersPrefab[n], transform.position, transform.rotation);

        spawnedCharacter.GetComponent<CharacterStats>().Level = level;
        spawnedCharacter.GetComponent<CharacterStats>().Init();
        if (patrolPoints.Count > 0)
        {
            spawnedCharacter.GetComponent<AICharacterController>().patrolPoints = patrolPoints[patrol].route;
            spawnedCharacter.GetComponent<AICharacterController>().currentIAState = AICharacterController.IAState.Patrolling;
        }

        spawnedCharacter.GetComponentInChildren<SkinnedMeshRenderer>().material = material;

        EventManager.NewCharacter(spawnedCharacter.GetComponent<CharacterStats>());

    }

}
