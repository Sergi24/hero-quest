﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Proporciona algunos métodos para el fácil acceso a los recursos del proyecto.
public static class ResourcesHelper
{
    // Rutas
    private const string ConsumableItemsPath = "Items/Consumables";
    private const string EquipmentItemsPath = "Items/Equipments";
    private const string QuestItemsPath = "Items/QuestItems";

    // Devuelve una lista con todos los items
    public static List<Item> GetAllItems()
    {
        Object[] o1 = Resources.LoadAll(ConsumableItemsPath);
        Object[] o2 = Resources.LoadAll(EquipmentItemsPath);
        Object[] o3 = Resources.LoadAll(QuestItemsPath);

        List<Item> items = new List<Item>();

        foreach (Object o in o1)
        {
            items.Add((Item)o);
        }
        foreach (Object o in o2)
        {
            items.Add((Item)o);
        }
        foreach (Object o in o3)
        {
            items.Add((Item)o);
        }

        return items;
    }

    // Devuelve una lista con todos los items consumibles
    public static List<Consumable> GetAllConsumableItems()
    {
        Object[] objects = null;

        objects = Resources.LoadAll(ConsumableItemsPath);

        List<Consumable> consumables = new List<Consumable>();

        foreach (Object o in objects)
        {
            consumables.Add((Consumable)o);
        }

        return consumables;
    }

    // Devuelve una lista con todos los items equipables.
    public static List<Equipment> GetAllEquipmentItems()
    {
        Object[] objects = null;

        objects = Resources.LoadAll(EquipmentItemsPath);

        List<Equipment> equipments = new List<Equipment>();

        foreach (Object o in objects)
        {
            equipments.Add((Equipment)o);
        }

        return equipments;
    }

    // Devuelve una lista con todos los items de misión.
    public static List<QuestItem> GetAllQuestItems()
    {
        Object[] objects = null;

        objects = Resources.LoadAll(QuestItemsPath);

        List<QuestItem> consumables = new List<QuestItem>();

        foreach (Object o in objects)
        {
            consumables.Add((QuestItem)o);
        }

        return consumables;
    }

    // Devuelve un item mediante su id (El más utilizado)
    public static Item GetItemById(int id)
    {
        Object[] o1 = Resources.LoadAll(ConsumableItemsPath);
        Object[] o2 = Resources.LoadAll(EquipmentItemsPath);
        Object[] o3 = Resources.LoadAll(QuestItemsPath);

        foreach (Object o in o1)
        {
            if (((Item)o).idItem == id)
            {
                return (Item)o;
            }
        }
        foreach (Object o in o2)
        {
            if (((Item)o).idItem == id)
            {
                return (Item)o;
            }
        }
        foreach (Object o in o3)
        {
            if (((Item)o).idItem == id)
            {
                return (Item)o;
            }
        }

        return null;
    }

}
