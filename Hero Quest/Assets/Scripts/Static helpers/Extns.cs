﻿using UnityEngine;

// Clase de ayuda con algun método general
public static class Extns
{
    public static Vector2 xz(this Vector3 vv)
    {
        return new Vector2(vv.x, vv.z);
    }

    // Devuelve la distancia en 2 dimensiones (de piso)
    public static float FlatDistanceTo(this Vector3 from, Vector3 unto)
    {
        Vector2 a = from.xz();
        Vector2 b = unto.xz();
        return Vector2.Distance(a, b);
    }

    // Devuelve si el objetivo está o no enfrente.
    public static bool IsInFront(Transform mainCharacter, Transform target)
    {
        var heading = target.position - mainCharacter.position;
        float dot = Vector3.Dot(heading, mainCharacter.forward);
        Debug.Log(dot);
        if (dot > 0)
            return true;

        return false;
    }

}