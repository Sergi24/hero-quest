﻿using UnityEngine;

// Controla que un gameobject esté mirando a un objetivo
public class LookAtTarget : MonoBehaviour
{

    public Transform target;

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(target);
    }
}
