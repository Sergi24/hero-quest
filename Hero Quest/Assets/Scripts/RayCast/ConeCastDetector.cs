﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AICharacterController))]
// Detecta si hay enemigos en el cono de visibilidad y avisa para que empiece la caceria
public class ConeCastDetector : MonoBehaviour
{

    private AICharacterController aICharacterScript;
    public List<GameObject> targets = new List<GameObject>();

    public LayerMask layerMask;

    public float fieldOfViewDegrees;
    public float visibilityDistance;

    public string[] targetTags = { "Player", "Citizen" };

    private void Start()
    {

        aICharacterScript = transform.GetComponent<AICharacterController>();

        // Hacemos las busquedas y agregamos los gameobjects a la lista de targets.
        foreach (string tag in targetTags)
        {
            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag(tag);
            for (int i = 0; i < gameObjects.Length; i++)
            {
                targets.Add(gameObjects[i]);
            }
        }

        // Añade los listeners
        EventManager.OnCharacterNewInWorld += AddTargetToTargetsArray;
        EventManager.OnCharacterDead += RemoveTargetToTargetsArray;

    }

    void Update()
    {
        // Si no está siguiente ningún objetivo...
        if (!IsFollowingATarget())
        {
            // Revisa si ve alguno de los posibles objetivos
            foreach (GameObject target in targets)
            {
                if (target != null)
                    if (CanSeeTarget(target))
                    {
                        CharacterStats targetcharacterStats = target.GetComponent<CharacterStats>();

                        if (targetcharacterStats != null)
                        {

                            // Si puede cazar...
                            if (aICharacterScript.CanStartHunting() && !targetcharacterStats.dead && !targetcharacterStats.ignoreCharacter && !GetComponent<CharacterStats>().dead)
                            {
                                //Debug.Log(name + ": Puedo cazar! " + targetcharacterStats.dead);
                                // Caza al objetivo
                                aICharacterScript.StartHunting(target.transform);
                                break;
                            }
                        }

                    }
            }
        }
    }

    // Verifica si puede ver o no al objetivo
    public bool CanSeeTarget(GameObject target)
    {
        RaycastHit hit;
        Vector3 rayDirection = target.transform.position - transform.position - Vector3.down;

        if ((Vector3.Angle(rayDirection, transform.forward)) <= fieldOfViewDegrees * 0.5f)
        {
            // Detecta si alguno de los posibles objetivos está en el rango de visión y es de una de las máscaras objetivo.
            Debug.DrawRay(transform.position, rayDirection, Color.green, 2, false);
            if (Physics.Raycast(transform.position, rayDirection, out hit, visibilityDistance, layerMask))
            {
                Debug.DrawRay(transform.position, rayDirection, Color.red, 2, false);

                foreach (string targetTag in targetTags)
                {
                    if (hit.transform.CompareTag(targetTag))
                        return true;
                }

                return false;
            }
        }

        return false;
    }

    // Verifica si ya está cazando a un objetivo...
    public bool IsFollowingATarget()
    {
        if (aICharacterScript.target != null)
        {
            // Miramos si está cazando a un objetivo de la lista.
            foreach (GameObject target in targets)
            {
                if (aICharacterScript.target.gameObject == target)
                {

                    return true;
                }
            }
        }
        return false;
    }

    // Añade un objetivo al array de objetivos
    private void AddTargetToTargetsArray(CharacterStats newCharacterStatsInWorld)
    {
        foreach (string tag in targetTags)
            if (newCharacterStatsInWorld.gameObject.tag.Equals(tag))
            {
                targets.Add(newCharacterStatsInWorld.gameObject);
                return;
            }
    }

    // Si un objetivo muere, se quita del array de posibles objetivos a detectar
    public void RemoveTargetToTargetsArray(GameObject character, CharacterStats diedCharacterStatsCharacter)
    {
        foreach (GameObject gO in targets)
        {
            if (character == gO)
            {
                targets.Remove(gO);
                break;
            }
        }
    }

    // Quitamos listeners cuando muere
    private void OnDestroy()
    {
        EventManager.OnCharacterNewInWorld -= AddTargetToTargetsArray;
        EventManager.OnCharacterDead -= RemoveTargetToTargetsArray;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, visibilityDistance);

    }

}


