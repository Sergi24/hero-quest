﻿using UnityEngine;

// Controla la inicialización del menu principal
public class MainMenuStarter : MonoBehaviour
{

    public int idMusic;

    private void Start()
    {
        GameManager.instance.isMainMenu = true;
        SoundManager.instance.PlayMusicClip(idMusic);

        // Inicializa la configuración general
        GameManager.instance.GeneralSettings.Init();
    }

}
