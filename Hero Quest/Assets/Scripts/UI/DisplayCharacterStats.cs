﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Muestra la información del personaje objetivo
public class DisplayCharacterStats : MonoBehaviour
{
    // Personaje objetivo
    public CharacterStats characterStats;

    // Elementos de canvas donde se muestrea en World Space.
    public Text characterNameText, healthText, manaText, levelText;
    public Slider healthSlider, manaSlider;

    // Ultimo valor de la vida y mana
    private int lastHealthValue, lastManaValue;

    private void Start()
    {

        characterNameText.text = characterStats.nameCharacter;

        UpdateHealthBar(characterStats.maxHealth.GetValue(), characterStats.CurrentHealth);

        if (characterStats.maxMana.GetValue() > 0)
        {
            UpdateManaBar(characterStats.maxMana.GetValue(), characterStats.currentMana);
        }
        else
            manaSlider.gameObject.SetActive(false);

        levelText.text = characterStats.Level + "";

    }

    private void Update()
    {
        if (lastHealthValue != characterStats.CurrentHealth)
        {
            UpdateHealthBar(characterStats.maxHealth.GetValue(), characterStats.CurrentHealth);
        }
        if (lastManaValue != characterStats.currentMana)
        {
            UpdateManaBar(characterStats.maxMana.GetValue(), characterStats.currentMana);
        }
    }

    // Actualiza el slider de vida
    public void UpdateHealthBar(int maxHealth, int currentHealth)
    {
        //Debug.Log("Update currentHealth: " + healthSlider.value + ", after: " + currentHealth);
        healthSlider.maxValue = maxHealth;
        healthSlider.value = currentHealth;
        lastHealthValue = currentHealth;

        healthText.text = currentHealth.ToString();
    }

    // Actualiza el slider de mana
    public void UpdateManaBar(int maxMana, int currentMana)
    {
        manaSlider.maxValue = maxMana;
        manaSlider.value = currentMana;
        lastManaValue = currentMana;

        manaText.text = currentMana.ToString();
    }

}
