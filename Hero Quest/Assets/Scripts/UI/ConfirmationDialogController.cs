﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Dialogo que confirma si quiere de verdad borrar un perfil.
public class ConfirmationDialogController : MonoBehaviour
{
    public ProfileButton ProfileButton { get; set; }
    public GameObject BackgroundDialog;

    private void OnEnable()
    {
        BackgroundDialog.SetActive(true);
    }

    private void OnDisable()
    {
        BackgroundDialog.SetActive(false);
    }

    public void NoRemoveProfile()
    {
        gameObject.SetActive(false);
    }

    // Procede a borrar el perfil
    public void YesRemoveProfile()
    {
        DAOManager.RemoveProfile(ProfileButton.idProfile);

        ProfileButton.CheckProfile();

        gameObject.SetActive(false);

    }

}
