﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Controla la mayor parte del cambas interactable
public class CanvasController : MonoBehaviour
{
    // Referencias a los elementos del canvas
    public GameObject pauseMenu, LoseMenu;
    public GameObject textPrefab;

    public GameObject conversationPanel;
    public Text titleText, subTitleText, alertText, conversationCharacterText, conversationText, infoText;

    // Coroutinas de los diferentes textos interactables del canvas para no duplicar threads.
    private IEnumerator titleTextCoroutine, subTitleTextCoroutine, alertTextCoroutine, infoTextCoroutine, conversationCoroutine;

    // Abre el menú de pausa o lo cierra
    public void Pause()
    {
        if (pauseMenu.activeSelf)
            pauseMenu.SetActive(false);
        else
            pauseMenu.SetActive(true);
    }

    // Activa el menu de perdición
    public void ActivateLoseMenu()
    {
        GameManager.instance.LockMouse(false);
        LoseMenu.SetActive(true);
    }

    // Muestra un texto de alerta durante x segundos con el color y
    public void ShowAlertText(int seconds, Color color, string alertString)
    {
        alertText.color = color;
        alertText.text = alertString;
        alertText.gameObject.SetActive(true);

        if (alertTextCoroutine != null)
            StopCoroutine(alertTextCoroutine);

        alertTextCoroutine = ShowText(alertText, seconds);
        StartCoroutine(alertTextCoroutine);
    }

    // Muestra un titulo por x segundos
    public void ShowTitle(int seconds, string title)
    {
        titleText.text = title;
        titleText.gameObject.SetActive(true);

        if (titleTextCoroutine != null)
            StopCoroutine(titleTextCoroutine);

        titleTextCoroutine = ShowText(titleText, seconds);
        StartCoroutine(titleTextCoroutine);
    }

    // Muestra un subtitulo por x segundos
    public void ShowSubtitle(int seconds, string subtitle)
    {
        subTitleText.text = subtitle;
        subTitleText.gameObject.SetActive(true);

        if (subTitleTextCoroutine != null)
            StopCoroutine(subTitleTextCoroutine);

        subTitleTextCoroutine = ShowText(subTitleText, seconds);
        StartCoroutine(subTitleTextCoroutine);
    }

    // Muestra una conversación no importante de un orador por x segundos por cada sentencia
    public void ShowConversation(int seconds, string characterName, List<string> strings)
    {
        conversationPanel.gameObject.SetActive(true);
        conversationCharacterText.text = characterName;

        if (conversationCoroutine != null)
            StopCoroutine(conversationCoroutine);

        conversationCoroutine = ShowConversationTexts(seconds, characterName, strings);
        StartCoroutine(conversationCoroutine);
    }

    // Muestra el info text
    public void ShowInfoText(string infoText)
    {
        this.infoText.text = infoText;
        this.infoText.gameObject.SetActive(true);
    }

    // Esconde el info text
    public void HideInfoText()
    {
        infoText.gameObject.SetActive(false);
    }

    // Muestra un texto flotante encima del personaje objetivo con el color designado
    public void ShowFloatingText(Transform transformCharacter, Color color, string text)
    {
        Camera cam = Camera.main;

        if (Vector3.Distance(cam.transform.position, transformCharacter.position) < 10)
        {

            Vector3 screenPos = cam.WorldToScreenPoint(transformCharacter.TransformPoint(transformCharacter.GetComponent<Rigidbody>().centerOfMass));

            GameObject textGO = Instantiate(textPrefab, new Vector2(screenPos.x, screenPos.y), Quaternion.identity);
            textGO.GetComponent<RectTransform>().SetParent(GetComponent<RectTransform>());
            textGO.GetComponent<Text>().color = color;
            textGO.GetComponent<Text>().text = text;
        }
    }

    // Muestra el texto durante x segundos
    IEnumerator ShowText(Text textField, int seconds)
    {
        yield return new WaitForSeconds(seconds);
        textField.gameObject.SetActive(false);
    }

    // Va mostrando las sentencias de conversación cada x segundos hasta finalizar
    IEnumerator ShowConversationTexts(int seconds, string characterName, List<string> strings)
    {
        foreach (string s in strings)
        {
            conversationText.text = s;
            yield return new WaitForSeconds(seconds);
        }
        conversationPanel.gameObject.SetActive(false);
    }

}
