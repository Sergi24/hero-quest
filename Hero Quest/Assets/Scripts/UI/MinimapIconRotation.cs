﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Se encarga de rotar el icono de minimapa en la dirección correcta para que no esté girado
public class MinimapIconRotation : MonoBehaviour
{

    public void Update()
    {
        Vector3 eulers = transform.eulerAngles;
        eulers.y = 0;
        eulers.z = 180;
        transform.eulerAngles = eulers;
    }

}
