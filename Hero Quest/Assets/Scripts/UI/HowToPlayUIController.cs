﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Controla el menú how to play
public class HowToPlayUIController : MonoBehaviour
{

    public GameObject returnMenu;

    public void OnReturnMenu()
    {
        returnMenu.SetActive(true);
        gameObject.SetActive(false);
    }

}
