﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Controla el movimiento del texto flotante al dañarse un personaje o recibir experiencia
public class FloatingText : MonoBehaviour
{
    // Duración de vida del texto
    public float seconds = 2;
    // Velocidad de movimiento ascendiente
    public float speed = 5;
    // Elemento UI de texto
    Text text;

    void Start()
    {
        text = GetComponent<Text>();
        Destroy(gameObject, seconds);
    }

    void Update()
    {
        text.transform.Translate(new Vector2(0, speed * Time.deltaTime));
    }
}
