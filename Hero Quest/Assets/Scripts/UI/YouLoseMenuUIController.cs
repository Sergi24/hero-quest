﻿using UnityEngine;

// Controla la ventana de derrota
public class YouLoseMenuUIController : MonoBehaviour
{

    public GameObject Loader;

    // Carga el perfil actual con el ultim punto de guardado
    public void LoadGame()
    {
        Loader.SetActive(true);
        LevelLoader levelLoader = Loader.GetComponent<LevelLoader>();
        levelLoader.LoadLevel("CharactersTest");
    }

    // Sale al menu principal
    public void ExitToMainMenu()
    {
        Loader.SetActive(true);
        LevelLoader levelLoader = Loader.GetComponent<LevelLoader>();
        levelLoader.LoadLevel("MainMenu");
    }

}
