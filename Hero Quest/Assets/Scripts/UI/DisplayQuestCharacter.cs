﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Muestra la información de un personaje de misión
public class DisplayQuestCharacter : MonoBehaviour
{
    // Personaje en cuestión
    public CharacterStats character;
    // Nombre alternativo
    public Text characterNameText;

    private void Start()
    {
        characterNameText.text = character.nameCharacter;
    }

}
