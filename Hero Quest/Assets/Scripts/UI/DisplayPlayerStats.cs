﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Muestra la inforamción del jugador en el canvas
public class DisplayPlayerStats : MonoBehaviour
{
    // Objetivo jugador
    private PlayerStats playerStats;

    // Elementos del canvas para el muestreo
    public Text characterNameText, healthText, manaText, levelText, experienceText;
    public Slider healthSlider, manaSlider, expSlider;

    // Variables a ir comprobando
    private int lastLevel, lastHealthValue, lastManaValue, lastLevelValue;
    private int lastExpValue = int.MaxValue, lastMaxExpValue = int.MaxValue;
    private string lastNameCharacter;

    private void Start()
    {
        playerStats = FindObjectOfType<PlayerStats>();
    }

    private void Update()
    {
        if (playerStats != null)
        {

            if (lastHealthValue != playerStats.CurrentHealth)
            {
                UpdateHealthBar(playerStats.maxHealth.GetValue(), playerStats.CurrentHealth);
            }
            if (lastManaValue != playerStats.currentMana)
            {
                UpdateManaBar(playerStats.maxMana.GetValue(), playerStats.currentMana);
            }
            if (lastLevelValue != playerStats.Level)
                UpdateLevel(playerStats.Level);
            if (!playerStats.nameCharacter.Equals(lastNameCharacter))
            {
                lastNameCharacter = playerStats.nameCharacter;
                characterNameText.text = lastNameCharacter;
            }
            if (lastExpValue != playerStats.currentExperience || lastMaxExpValue != playerStats.neededExperience)
            {
                // Slider
                expSlider.maxValue = playerStats.neededExperience;
                expSlider.value = playerStats.currentExperience;

                experienceText.text = playerStats.currentExperience + " / " + playerStats.neededExperience;

                lastExpValue = playerStats.currentExperience;
                lastMaxExpValue = playerStats.neededExperience;
            }
        }
        else
        {
            playerStats = FindObjectOfType<PlayerStats>();
        }


    }

    // Actualiza la barra de vida
    public void UpdateHealthBar(int maxHealth, int currentHealth)
    {
        healthSlider.maxValue = maxHealth;
        healthSlider.value = currentHealth;

        healthText.text = currentHealth.ToString();

        lastHealthValue = currentHealth;
    }

    // Actualiza la barra de maná
    public void UpdateManaBar(int maxMana, int currentMana)
    {
        manaSlider.maxValue = maxMana;
        manaSlider.value = currentMana;

        manaText.text = currentMana.ToString();

        lastManaValue = currentMana;
    }

    // Actualiza el nivel
    public void UpdateLevel(int newLevel)
    {
        levelText.text = newLevel + "";
        lastLevelValue = newLevel;
    }

}
