﻿using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

// Controla la introducción del nombre al crear el perfil
public class EnterYourNameWindow : MonoBehaviour
{
    // Botón de perfil
    public ProfileButton profileButton;

    // Inputfield donde se pone el nombre
    public InputField inputField;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (inputField.text.Equals(""))
                inputField.text = "Roland";

            string text = Regex.Replace(inputField.text, @"[^a-zA-Z0-9 ]", "");
            Profile profile = profileButton.CreateProfile(text);
            DAOManager.SerializeProfile(profileButton.idProfile, profile);
            profileButton.PlayGame();
            gameObject.SetActive(false);
        }
    }

}
