﻿using UnityEngine;
using Lean.Localization;

// Permite cambiar de lenguaje
public class LanguageMenuUIController : MonoBehaviour
{

    public GameObject returnMenu;

    // Actualiza el lenguaje por el lenguaje pasado.
    public void SetLanguage(string language)
    {
        GameManager.instance.GeneralSettings.Language = language;
        LeanLocalization.CurrentLanguage = language;
        ReturnMenu();
    }

    public void ReturnMenu()
    {
        returnMenu.SetActive(true);
        gameObject.SetActive(false);
    }

}
