﻿using UnityEngine;

// Controla el menu de juego
public class PlayMenuUIController : MonoBehaviour
{

    public GameObject returnMenu;

    // Vuelve al menu anterior
    public void ReturnMenu()
    {
        returnMenu.SetActive(true);
        gameObject.SetActive(false);
    }

}
