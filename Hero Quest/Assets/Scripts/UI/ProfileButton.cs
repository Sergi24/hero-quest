﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Lean.Localization;

// Controla el botón de perfil
public class ProfileButton : MonoBehaviour
{
    // Perfil
    Profile profile;
    // id de perfil
    public string idProfile;

    // Referencia al levelloader
    public LevelLoader levelLoader;
    // Referencia al botón de borrado relacionado con el botón de perfil
    public GameObject deleteProfileButton;

    // Referencia a los elementos de ui relacionados con el botón de perfil
    public Text nameText, currentZoneText, timePlayedText;
    public GameObject emptyProfileInfo, NonEmptyProfileInfo;

    // Referencia con el enter name Panel
    public GameObject enterNamePanel;

    private void Awake()
    {
        LeanLocalizedTextArgs LocalizedTextNameArgs = nameText.GetComponent<LeanLocalizedTextArgs>();
        LeanLocalizedTextArgs LocalizedTextZoneArgs = currentZoneText.GetComponent<LeanLocalizedTextArgs>();

        if (LocalizedTextNameArgs != null)
        {
            LocalizedTextNameArgs.SetArg("{0}", 0);
        }
        if (LocalizedTextZoneArgs != null)
        {
            LocalizedTextZoneArgs.SetArg("{0}", 0);
        }
    }

    private void Start()
    {
        CheckProfile();
    }

    // Inspecciona el perfil del botón, si existe pone los datos. Si no existe dice que esta vacío y propone crear uno.
    public void CheckProfile()
    {
        profile = DAOManager.UnserializeProfile(idProfile);

        if (profile != null)
        {
            emptyProfileInfo.SetActive(false);
            NonEmptyProfileInfo.SetActive(true);

            deleteProfileButton.SetActive(true);

            TimeSpan time = TimeSpan.FromSeconds(profile.TimePlayed);
            string s = time.ToString(@"d\.hh\:mm\:ss");
            string[] strings = s.Split('.');
            s = strings[0] + "d " + strings[1];

            LeanLocalizedTextArgs LocalizedTextNameArgs = nameText.GetComponent<LeanLocalizedTextArgs>();
            LeanLocalizedTextArgs LocalizedTextZoneArgs = currentZoneText.GetComponent<LeanLocalizedTextArgs>();

            LocalizedTextNameArgs.SetArg(profile.playerInfo.namePlayer, 0);
            LocalizedTextZoneArgs.SetArg(profile.CurrentZone, 0);

            timePlayedText.text = s;
        }
        else
        {
            emptyProfileInfo.SetActive(true);
            NonEmptyProfileInfo.SetActive(false);
            deleteProfileButton.SetActive(false);
        }
    }

    // Crea el perfil mediante los datos dados
    public Profile CreateProfile(string newPlayerName)
    {

        PlayerInfo playerStats = new PlayerInfo
        {
            namePlayer = newPlayerName,
            CurrentHealth = 100,
            currentMana = 100,
            Level = 1
        };

        Profile newProfile = new Profile
        {
            idProfile = idProfile,
            playerInfo = playerStats,
            CurrentZone = "",
            TimePlayed = 0,
            CurrentScene = "StartCave",
            LastScene = "MainMenu"
        };

        return newProfile;
    }

    // Llama al levelLoader para que cargue el juego con el perfil dado.
    public void PlayGame()
    {
        profile = DAOManager.UnserializeProfile(idProfile);

        if (profile == null)
        {
            enterNamePanel.SetActive(true);
            enterNamePanel.GetComponent<EnterYourNameWindow>().profileButton = this;
        }
        else
        {
            GameManager.instance.Profile = profile;

            levelLoader.gameObject.SetActive(true);
            levelLoader.LoadLevel(profile.CurrentScene);
        }

    }

}
