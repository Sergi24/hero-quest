﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Permite "fadear" (hacer oscura la pantalla o bien visible en un intervalo de tiempo)
public class Fader : MonoBehaviour
{
    // Imagen vacía con color negro con alpha manejable para fadear
    public Image imageFader;
    // Ha empezado a fadear?
    public bool startFaded = true;
    // Está fadeando
    public bool fading = false;

    // Tiempo de espera antes de fadear
    public float fadeDelay;
    // Duración del fadeo
    public float fadeDuration;

    // Coroutina que controla el fadeo para no haber más de un fader activo
    private IEnumerator coroutine;

    private void Start()
    {
        Color tempColor = imageFader.color;
        if (startFaded)
        {
            tempColor.a = 1f;
            imageFader.color = tempColor;
        }
        else
        {
            tempColor.a = 0f;
            imageFader.color = tempColor;
        }
    }

    // Fadea la pantalla
    public void Fade()
    {
        fading = true;
        imageFader.gameObject.SetActive(true);
        if (coroutine != null)
            StopCoroutine(coroutine);
        coroutine = Fading();
        StartCoroutine(coroutine);
    }

    // Desfadea la pantalla
    public void UnFade()
    {
        fading = true;
        if (coroutine != null)
            StopCoroutine(coroutine);
        coroutine = UnFading();
        StartCoroutine(coroutine);
    }

    // IEnumerator que fadea la pantalla
    IEnumerator Fading()
    {
        float counter = 0;
        Color tempColor;
        yield return new WaitForSeconds(fadeDelay);
        while (counter <= fadeDuration)
        {

            float tempAlpha = Mathf.Lerp(0, 1, counter / fadeDuration);

            tempColor = new Color(0, 0, 0, tempAlpha);
            imageFader.color = tempColor;

            counter += Time.deltaTime;

            yield return null;
        }
        tempColor = new Color(0, 0, 0, 1);
        imageFader.color = tempColor;
        counter = 0;
        fading = false;
        yield return null;
    }

    // IEnumerator que desfadea la pantalla
    IEnumerator UnFading()
    {
        float counter = 0;
        Color tempColor;

        yield return new WaitForSeconds(fadeDelay);

        while (counter <= fadeDuration)
        {

            float tempAlpha = Mathf.Lerp(1, 0, counter / fadeDuration);

            tempColor = new Color(0, 0, 0, tempAlpha);
            imageFader.color = tempColor;

            counter += Time.deltaTime;

            yield return null;
        }
        tempColor = new Color(0, 0, 0, 0);
        imageFader.color = tempColor;
        counter = 0;
        fading = false;
        imageFader.gameObject.SetActive(false);
        yield return null;
    }

    // Ha finalizado el fade?
    public bool IsFadeFinished()
    {
        return !fading;
    }
}
