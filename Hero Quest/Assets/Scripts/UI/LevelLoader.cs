﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Lean.Localization;

// Actualiza la pantalla de carga de escenario y la el cargar escenario
public class LevelLoader : MonoBehaviour
{
    // Presiona espacio para continuar requerido?
    public bool usePressSpaceToContinue;

    // Referencias a los elementos de la ui de la pantalla de carga
    public Image loaderImage;
    public Text titleText, textText, pressSpaceToContinue;

    public Slider slider;
    public Text progressText;

    // Referencia al fader
    public Fader fader;

    private void Start()
    {
        Time.timeScale = 0;
        // Si presiona space para continua no está activo muestra la escena directamente.
        if (!usePressSpaceToContinue)
        {
            StartScene();
        }
        // Si está activo carga el consejo e imagen guardados en el GameManager
        else
        {
            if (GameManager.instance.TempAdvice != null)
            {
                SetAdvice(GameManager.instance.TempAdvice);
                if (GameManager.instance.TempAdvice.preferredSprite != null)
                    SetSprite(GameManager.instance.TempAdvice.preferredSprite);
                else
                    SetSprite(GameManager.instance.TempSprite);

            }
            else
            {
                Advice advice = GetAdvice();
                SetAdvice(advice);

                if (advice.preferredSprite != null)
                    SetSprite(advice.preferredSprite);
                else
                    SetSprite(GetSprite());
            }


            pressSpaceToContinue.gameObject.SetActive(true);

        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartScene();

        }
    }

    // Pone en marcha la escena
    private void StartScene()
    {
        gameObject.SetActive(false);
        Time.timeScale = 1;
        GameManager.instance.isPlaying = true;
        GameManager.instance.isGameStarted = true;
        fader.UnFade();
    }

    // Carga un nivel mediante su nombre
    public void LoadLevel(string sceneToLoad)
    {
        pressSpaceToContinue.gameObject.SetActive(false);

        Advice advice = GetAdvice();

        if (advice.preferredSprite != null)
        {
            SetSprite(advice.preferredSprite);
        }
        else
        {
            Sprite sprite = GetSprite();
            SetSprite(sprite);
        }

        SetAdvice(advice);

        Time.timeScale = 1;

        StartCoroutine(Wait1FrameAndLoad(sceneToLoad));
    }

    // Espera hasta que se acabe el frame para cargar
    IEnumerator Wait1FrameAndLoad(string sceneToLoad)
    {
        yield return new WaitForEndOfFrame();
        StartCoroutine(LoadAsynchronously(sceneToLoad));

    }

    // Obtiene un nuevo sprite de imagen de fondo
    private Sprite GetSprite()
    {
        Object[] sprites = Resources.LoadAll("SpritesLoader", typeof(Sprite));

        int n = Random.Range(0, sprites.Length);
        return (Sprite)sprites[n];
    }

    // Obtiene un nuevo Consejo
    private Advice GetAdvice()
    {
        Object[] advices = Resources.LoadAll("Advices", typeof(Advice));

        int n = Random.Range(0, advices.Length);

        return (Advice)advices[n];
    }

    // Actualiza la imagen de fondo de la pantalla de carga
    private void SetSprite(Sprite sprite)
    {
        GameManager.instance.TempSprite = sprite;
        loaderImage.sprite = sprite;
    }

    // Actualiza el consejo
    private void SetAdvice(Advice advice)
    {
        GameManager.instance.TempAdvice = advice;

        if (LeanLocalization.CurrentLanguage.Equals("English"))
        {
            titleText.text = advice.enTitle;
            textText.text = advice.enText;
        }
        else if (LeanLocalization.CurrentLanguage.Equals("Spanish"))
        {
            titleText.text = advice.esTitle;
            textText.text = advice.esText;
        }
        else if (LeanLocalization.CurrentLanguage.Equals("Catalan"))
        {
            titleText.text = advice.caTitle;
            textText.text = advice.caText;
        }
    }

    // Carga un nuevo escenario asincronamente mostrando el progreso en la barra de carga
    IEnumerator LoadAsynchronously(string sceneToLoad)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneToLoad);

        while (!operation.isDone)
        {

            float progress = Mathf.Clamp01(operation.progress / .9f);
            //Debug.Log("LOAD PROGRESS: " + progress);
            slider.value = progress;
            progressText.text = progress * 100f + "%";

            yield return null;

        }

    }
}
