﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Muestra la inforamción de un objeto del inventario cuando el mouse está encima.
public class DisplayItemInfo : MonoBehaviour
{
    // Referencia a los Texts donde se introduce la información
    public Text itemName, itemDescription, itemProperties;

    // Muestra la información del item.
    public void ShowItemInfo(Item item)
    {
        // Obtiene el nombre del item en el idioma actual.
        itemName.text = item.GetNameByLanguage();
        // Obtiene la descripción del item en el idioma actual.
        itemDescription.text = item.GetDescriptionByLanguage();

        string s = "";
        // Si es consumible...
        if (item.GetType().Equals(typeof(Consumable)))
        {
            Consumable tempItem = item as Consumable;
            s += "Consumable\n";
            if (tempItem.healAmount != 0)
            {
                s += "Heal: " + tempItem.healAmount + "\n";
            }
            if (tempItem.manaAmount != 0)
            {
                s += "Mana: " + tempItem.manaAmount;
            }
        }
        // Si es Equipable...
        else if (item.GetType().Equals(typeof(Equipment)))
        {
            Equipment tempItem = item as Equipment;

            if (tempItem.strenghModifier != 0)
                s += "Strengh: " + tempItem.strenghModifier + "\n";
            if (tempItem.agilityModifier != 0)
                s += "Agility: " + tempItem.agilityModifier + "\n";
            if (tempItem.intelligenceModifier != 0)
                s += "Intelligence: " + tempItem.intelligenceModifier + "\n";

            if (tempItem.damageModifier != 0)
                s += "Damage: " + tempItem.damageModifier + "\n";
            if (tempItem.armorModifier != 0)
                s += "Armor: " + tempItem.armorModifier + "\n";

            if (tempItem.maxHealthModifier != 0)
                s += "Max Life: " + tempItem.maxHealthModifier + "\n";
            if (tempItem.maxManaModifier != 0)
                s += "Max Mana: " + tempItem.maxManaModifier + "\n";

            if (tempItem.lifeRegenerationModifier != 0)
                s += "Life Regeneration: " + tempItem.lifeRegenerationModifier + "\n";
            if (tempItem.manaRegenerationModifier != 0)
                s += "Mana Regeneration: " + tempItem.manaRegenerationModifier;


        }
        else if (item.GetType().Equals(typeof(QuestItem)))
            s += "Quest Item\n";

        itemProperties.text = s;
    }

}
