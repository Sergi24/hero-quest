﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Controla el comportamiento del botón de borrado de perfil del menú principal
public class DeleteProfileButton : MonoBehaviour
{

    public ProfileButton profileButton;
    public GameObject confirmationDialog;

    // Abre el dialogo de confirmación de borrado de perfil
    public void OpenConfirmationDialog()
    {
        confirmationDialog.SetActive(true);

        ConfirmationDialogController confirmationDialogController = confirmationDialog.GetComponent<ConfirmationDialogController>();
        confirmationDialogController.ProfileButton = profileButton;

    }

}
