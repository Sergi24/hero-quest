﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Controla el menu de pausa
public class PauseMenuUIController : MonoBehaviour
{

    public CanvasController canvasController;

    public GameObject SettingsMenu, howToPlayWindow;

    public GameObject SaveGameDialog;
    public LevelLoader levelLoader;

    // Reanuda el juego
    public void ResumeGame()
    {
        GameManager.instance.Pause();

    }

    // Guarda el juego
    public void Savegame()
    {
        SaveGameDialog.SetActive(true);
        StartCoroutine(Saving());
    }

    // Carga el juego
    public void LoadGame()
    {
        GameManager.instance.Pause();
        GameManager.instance.Profile = DAOManager.UnserializeProfile(GameManager.instance.Profile.idProfile);
        levelLoader.gameObject.SetActive(true);
        levelLoader.LoadLevel(GameManager.instance.Profile.CurrentScene);
    }

    // Activa el menu de configuración
    public void ActivateSettingsMenu()
    {
        SettingsMenu.SetActive(true);
        gameObject.SetActive(false);
    }

    // Activa el menú de how to play
    public void ActivateHowToPlay()
    {
        howToPlayWindow.SetActive(true);
        gameObject.SetActive(false);
    }

    // Sale al menu principal
    public void ExitToMainMenu()
    {
        GameManager.instance.Pause();
        levelLoader.gameObject.SetActive(true);
        levelLoader.LoadLevel("MainMenu");
    }

    // Guardando IEnumerator
    IEnumerator Saving()
    {
        float startTime = Time.realtimeSinceStartup;
        DAOManager.SerializeProfile(GameManager.instance.Profile.idProfile, GameManager.instance.Profile);
        while (Time.realtimeSinceStartup - startTime < 2)
        {
            yield return null;
        }
        SaveGameDialog.SetActive(false);

    }

}
