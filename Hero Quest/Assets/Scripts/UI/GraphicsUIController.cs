﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lean.Localization;

// Controla el apartado de opciones de configuración de gráficos del menú
public class GraphicsUIController : MonoBehaviour
{
    // Opciones de calidad grafica
    public Dropdown qualityDropdown;
    private string[] qualities;

    // Opciones de resolución gráfica
    public Dropdown resolutionDropDown;
    private Resolution[] resolutions;

    // Opción de pantalla completa
    public Toggle fullScreenToggle;

    // Botón de volver al menu anterior
    public GameObject returnMenu;

    // Use this for initialization
    void Start()
    {
        InitializeQualityDropdown();
        InitializeResolutionsDropdown();
        InitializeFullScreenToggle();
    }

    private void OnEnable()
    {
        // Needed to translate correctly
        InitializeQualityDropdown();
    }

    // Inicializa el dropdown de calidad gráfica
    private void InitializeQualityDropdown()
    {
        // Take qualities
        qualities = new string[5];
        LeanTranslation leanTranslation = LeanLocalization.GetTranslation("Very Low Quality");
        qualities[0] = leanTranslation.Text;
        leanTranslation = LeanLocalization.GetTranslation("Low Quality");
        qualities[1] = leanTranslation.Text;
        leanTranslation = LeanLocalization.GetTranslation("Medium Quality");
        qualities[2] = leanTranslation.Text;
        leanTranslation = LeanLocalization.GetTranslation("High Quality");
        qualities[3] = leanTranslation.Text;
        leanTranslation = LeanLocalization.GetTranslation("Very High Quality");
        qualities[4] = leanTranslation.Text;

        qualityDropdown.ClearOptions();

        List<string> options = new List<string>();

        for (int i = 0; i < qualities.Length; i++)
        {
            string option = qualities[i];
            options.Add(option);
        }

        qualityDropdown.AddOptions(options);
        qualityDropdown.value = GameManager.instance.GeneralSettings.Quality;
    }

    // Inicializa el dropdown de resolución gráfica
    private void InitializeResolutionsDropdown()
    {
        resolutions = Screen.resolutions;

        resolutionDropDown.ClearOptions();

        List<string> options = new List<string>();

        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);
        }

        resolutionDropDown.AddOptions(options);
        resolutionDropDown.value = GameManager.instance.GeneralSettings.Resolution;
    }

    // Inicializa el toggle de pantalla completa
    private void InitializeFullScreenToggle()
    {
        //fullScreenToggle.enabled = GameManager.instance.generalSettings.FullScreen;
        fullScreenToggle.isOn = GameManager.instance.GeneralSettings.FullScreen;
    }

    // Actualiza la calidad gráfica
    public void SetQuality(int qualityIndex)
    {
        GameManager.instance.GeneralSettings.SetQuality(qualityIndex);
    }

    // Actualiza la pantalla a pantalla completa o al revés
    public void SetFullScreen(bool isFullScreen)
    {
        GameManager.instance.GeneralSettings.SetFullScreen(isFullScreen);
    }

    // Actualiza la resolución gráfica
    public void SetResolution(int resolutionIndex)
    {
        GameManager.instance.GeneralSettings.SetResolution(resolutionIndex);
    }

    // Vuelve al menu anterior
    public void ReturnMenu()
    {
        DAOManager.SerializeSettings();
        returnMenu.SetActive(true);
        gameObject.SetActive(false);
    }

}
