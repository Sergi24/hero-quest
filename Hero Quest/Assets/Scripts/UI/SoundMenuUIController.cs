﻿using UnityEngine;
using UnityEngine.UI;

// Controla la ventana de controles de audio
public class SoundMenuUIController : MonoBehaviour
{

    public Slider mainVolume;
    public Slider musicVolume;
    public Slider sfxVolume;

    public GameObject returnMenu;

    public void Start()
    {
        InitializeSliders();
    }

    public void InitializeSliders()
    {
        GeneralSettings generalSettings = GameManager.instance.GeneralSettings;

        mainVolume.value = generalSettings.MainVolume;
        musicVolume.value = generalSettings.MusicVolume;
        sfxVolume.value = generalSettings.SfxVolume;

        mainVolume.onValueChanged.AddListener(delegate { SoundManager.instance.SetMainVolume(mainVolume.value); });
        musicVolume.onValueChanged.AddListener(delegate { SoundManager.instance.SetMusicVolume(musicVolume.value); });
        sfxVolume.onValueChanged.AddListener(delegate { SoundManager.instance.SetSFXVolume(sfxVolume.value); });
    }

    public void ReturnMenu()
    {
        DAOManager.SerializeSettings();
        returnMenu.SetActive(true);
        gameObject.SetActive(false);
    }

}
