﻿using UnityEngine;

// Controla la ventana de opciones de configuración
public class SettingsMenuUIController : MonoBehaviour
{

    public GameObject SoundMenu;
    public GameObject GraphicsMenu;
    public GameObject LanguageMenu;
    public GameObject returnMenu;

    public void ActivateSoundMenu()
    {
        SoundMenu.SetActive(true);
        gameObject.SetActive(false);
    }

    public void ActivateGraphicsMenu()
    {
        GraphicsMenu.SetActive(true);
        gameObject.SetActive(false);
    }

    public void ActivateLanguageMenu()
    {
        LanguageMenu.SetActive(true);
        gameObject.SetActive(false);
    }

    public void ReturnMenu()
    {
        returnMenu.SetActive(true);
        gameObject.SetActive(false);
    }

}
