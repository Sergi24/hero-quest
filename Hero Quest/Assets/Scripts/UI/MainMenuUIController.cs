﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Controla la ventana principal del menú principal
public class MainMenuUIController : MonoBehaviour
{
    public GameObject PlayMenu, ConfigurationMenu, howToPlayMenu;

    // Activa el menu de perfiles para jugar
    public void ActivatePlayMenu()
    {
        PlayMenu.SetActive(true);
        gameObject.SetActive(false);
    }

    // Activa el menu de configuración
    public void ActivateConfigurationMenu()
    {
        ConfigurationMenu.SetActive(true);
        gameObject.SetActive(false);
    }

    // Activa el menú de how to play
    public void ActivateHowToPlayMenu()
    {
        howToPlayMenu.SetActive(true);
        gameObject.SetActive(false);
    }

    // Sale del juego
    public void ExitGame()
    {
        Application.Quit();
    }

}
