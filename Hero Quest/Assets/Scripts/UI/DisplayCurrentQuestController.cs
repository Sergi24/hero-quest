﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lean.Localization;

// Muestra la información de la misión actual escogida
public class DisplayCurrentQuestController : MonoBehaviour
{
    // Elementos del canvas donde se muestra
    public Text titleQuestText, questText;

    private void Start()
    {

        UpdateQuestPanel();

        // Se añaden listeners
        EventManager.OnQuestAccepted += UpdateQuestPanel;
        EventManager.OnQuestCompleted += UpdateQuestPanel;
        EventManager.OnObjectiveUpdated += UpdateQuestPanel;

    }

    // Actualizamos el muestreo de la misión
    public void UpdateQuestPanel()
    {

        List<Quest> currentQuests = GameManager.instance.Profile.CurrentQuests;

        if (currentQuests.Count != 0)
        {
            //StartCoroutine(UpdateQuestAfterFrame());

            Quest quest = GameManager.instance.Profile.CurrentQuests[0];

            titleQuestText.text = quest.GetCurrentLanguageTitle();
            string s = quest.GetCurrentDescriptionTitle() + "\n\n";
            foreach (Objective o in quest.Objectives)
            {
                s += o.GetDescriptionByLanguage() + "\n";

                if (o.GetType().Equals(typeof(CollectionObjective)))
                {
                    s += o.CurrentAmount + "/" + o.RequiredAmount + "\n\n";
                }
                else if (o.GetType().Equals(typeof(KillObjective)))
                {
                    s += o.CurrentAmount + "/" + o.RequiredAmount + "\n\n";
                }
            }

            questText.text = s;
        }
        else
        {

            titleQuestText.text = LeanLocalization.GetTranslation("NoQuestActive").Text;
            questText.text = "";
        }
    }

    private void OnDestroy()
    {
        // Se quitan listeners
        EventManager.OnQuestAccepted -= UpdateQuestPanel;
        EventManager.OnQuestCompleted -= UpdateQuestPanel;
        EventManager.OnObjectiveUpdated -= UpdateQuestPanel;
    }

}
