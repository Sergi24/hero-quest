﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Localization;

// Muestra una conversación sin importancia si no se ha completado una misión
public class ShowConversationOnIncompleteQuestTrigger : MonoBehaviour
{
    // Misión sin completar
    public int idQuest;

    private CanvasController canvasController;

    // Nombre del orador
    public string characterName;
    // Sentencias en los distintos idiomas
    public List<string> enConversationStrings;
    public List<string> snConversationStrings;
    public List<string> caConversationStrings;

    // Se ha ejecutado la conversación?
    private bool done;

    private void Awake()
    {
        canvasController = FindObjectOfType<CanvasController>();
    }

    // Use this for initialization
    void Start()
    {
        done = false;
    }

    // Ejecuta la conversación si noo se ha completado la misión
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("trigger!");
        if (!done && other.tag.Equals("Player"))
        {
            Debug.Log("entra");
            bool isQuestCompleted = false;
            foreach (Quest q in GameManager.instance.Profile.CompletedQuests)
                if (q.idQuest == idQuest)
                    isQuestCompleted = true;

            if (!isQuestCompleted)
            {
                List<string> conversationStrings = GetConversationByCurrentLanguage();
                canvasController.ShowConversation(2, characterName, conversationStrings);
                done = true;
            }
        }
    }

    // Devuelve la conversación en el idioma actual
    public List<string> GetConversationByCurrentLanguage()
    {
        string currentLanguage = LeanLocalization.CurrentLanguage;

        if (currentLanguage.Equals("English"))
            return enConversationStrings;
        else if (currentLanguage.Equals("Spanish"))
            return snConversationStrings;
        else if (currentLanguage.Equals("Catalan"))
            return caConversationStrings;

        return null;
    }
}