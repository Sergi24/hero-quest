﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Localization;

// Ejecuta una conversación sin importancia si se tiene una misión activa
public class ShowConversationOnCurrentQuestTrigger : MonoBehaviour
{
    // id de la misión en cuestión
    public int idQuest;

    private CanvasController canvasController;
    // Nombre del orador
    public string characterName;
    // sentencias en los idiomas
    public List<string> enConversationStrings;
    public List<string> snConversationStrings;
    public List<string> caConversationStrings;

    // Se ha realizado en esta escena?
    private bool done;

    private void Awake()
    {
        canvasController = FindObjectOfType<CanvasController>();
    }

    // Use this for initialization
    void Start()
    {
        done = false;
    }

    // Ejecuta la conversación en caso de tener la misión y no haberla ejecutado aun.
    private void OnTriggerEnter(Collider other)
    {
        if (!done)
        {
            bool isQuestCompleted = true;
            foreach (Quest q in GameManager.instance.Profile.CurrentQuests)
                if (q.idQuest == idQuest)
                    isQuestCompleted = false;

            if (!isQuestCompleted)
            {
                List<string> conversationStrings = GetConversationByCurrentLanguage();
                canvasController.ShowConversation(2, characterName, conversationStrings);
                done = true;
            }
        }
    }

    // Devuelve las conversaciones en el idioma actual.
    public List<string> GetConversationByCurrentLanguage()
    {
        string currentLanguage = LeanLocalization.CurrentLanguage;

        if (currentLanguage.Equals("English"))
            return enConversationStrings;
        else if (currentLanguage.Equals("Spanish"))
            return snConversationStrings;
        else if (currentLanguage.Equals("Catalan"))
            return caConversationStrings;

        return null;
    }
}