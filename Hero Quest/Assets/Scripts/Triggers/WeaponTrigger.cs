﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// Controla los enemigos golpeados con el arma.
public class WeaponTrigger : MonoBehaviour
{
    // Referencias a las clases del personaje poseedor del arma.
    public AICharacterController aiCharacterController;
    public InputPlayerController inputPlayerController;
    public ConeCastDetector coneCastDetector;
    public CharacterStats characterStats;

    // Enemigos golpeados en el ataque actual
    private List<GameObject> enemyHits = new List<GameObject>();

    //public LayerMask layer;

    // Tags de los objetivos
    public List<string> objectiveTags;

    // Puede el arma golpear?
    public bool canHit;

    private void Start()
    {
        Init();
    }

    public void Init()
    {
        inputPlayerController = GetComponentInParent<InputPlayerController>();
        aiCharacterController = GetComponentInParent<AICharacterController>();
        coneCastDetector = GetComponentInParent<ConeCastDetector>();
        characterStats = GetComponentInParent<CharacterStats>();

    }

    // Verifica si el collider golpeado es un objetivo y si lo es lo añade en el array de objetivos golpeados y procede a ejecutar el daño.
    private void OnTriggerEnter(Collider other)
    {
        if (IsTagObjective(other.gameObject))
        {
            if (!IsHitted(other.gameObject))
            {
                if (canHit)
                {
                    // Verifica si es un personaje o el jugador
                    if (aiCharacterController != null)
                    {
                        if (aiCharacterController.CanHit())
                        {
                            //Debug.Log("New Hit: " + name);

                            // Add to array for no extra hits.
                            enemyHits.Add(other.gameObject);

                            CharacterStats objectiveStats = other.GetComponent<CharacterStats>();

                            objectiveStats.TakeDamage(characterStats);
                        }
                    }

                    else if (inputPlayerController != null)
                    {
                        if (inputPlayerController.canHit)
                        {
                            enemyHits.Add(other.gameObject);

                            CharacterStats objectiveStats = other.GetComponent<CharacterStats>();

                            objectiveStats.TakeDamage(characterStats);

                            Debug.Log("Player hit!");
                        }
                    }
                }

            }
        }
    }

    // Verifica si es un objetivo válido
    private bool IsTagObjective(GameObject otherGO)
    {
        bool isTagObjective = false;
        foreach (string tag in objectiveTags)
        {
            if (tag == otherGO.tag)
            {
                isTagObjective = true;
                break;
            }
        }
        return isTagObjective;
    }

    // Verifica si ya ha sido golpeado
    private bool IsHitted(GameObject otherGO)
    {
        bool isEnemyHitted = false;
        foreach (GameObject enemyHit in enemyHits)
        {
            if (otherGO == enemyHit)
            {
                isEnemyHitted = true;
                break;
            }
        }
        return isEnemyHitted;
    }

    // Reinicia el array de objetivos golpeados
    public void RestartEnemyHits()
    {
        enemyHits = new List<GameObject>();
    }
}
