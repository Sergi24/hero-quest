﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Localization;

// Muestra el nombre de la zona al entrar en ella
public class ShowTitleZoneOnTrigger : MonoBehaviour
{
    CanvasController canvasController;

    [LeanPhraseName]
    public string idStringTitle;


    private void Awake()
    {
        canvasController = FindObjectOfType<CanvasController>();
    }

    // Muestra el nombre de la zona si el personaje ha entrado en ella
    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            string title = LeanLocalization.GetTranslation(idStringTitle).Text;

            if (Extns.IsInFront(transform, other.transform))
            {
                canvasController.ShowTitle(2, title);
            }

        }
    }

}
