﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Cameras;
using UnityEngine.SceneManagement;

// Maneja el personaje de jugador a lo Zelda Ocarine of time cuando cambia de escena.
public class LeaveSceneTrigger : MonoBehaviour
{
    // Nueva escena que carga
    public string levelToLoad;

    // Referencias a los controladores de la camara
    private FreeLookCam freeLookCam;
    private ProtectCameraFromWallClip protectCameraFromWallClip;

    // Referencia a la cámara
    private Camera mainCamera;
    // Punto al que irá el jugador automaticamente mientras se hace el fade.
    public Transform leavePoint;

    // Referencia al fader y levelloader
    private Fader fader;
    private LevelLoader levelLoader;

    // Tiempo extra de espera después de q se complete el fader
    [SerializeField]
    private float waitSecondsBeforeLoad;

    private void Start()
    {
        fader = FindObjectOfType<Fader>();
        levelLoader = FindObjectOfType<LevelLoader>();
    }

    // Configura el jugador y la cámara para el cambio de escenario.
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            mainCamera = Camera.main;

            mainCamera.GetComponentInParent<FreeLookCam>().enabled = false;
            mainCamera.GetComponentInParent<ProtectCameraFromWallClip>().enabled = false;
            LookAtTarget lookAt = mainCamera.gameObject.AddComponent<LookAtTarget>();
            lookAt.target = other.transform;

            InputPlayerController inputPlayerController = other.GetComponent<InputPlayerController>();
            inputPlayerController.enabled = false;

            NavMeshAgent agent = other.GetComponent<NavMeshAgent>();
            agent.enabled = true;

            AIPlayerController aiPlayerController = other.GetComponent<AIPlayerController>();
            aiPlayerController.enabled = true;
            aiPlayerController.SetTarget(leavePoint);

            // Actualizamos datos de perfil para no perder el equipamiento equipado
            GameManager.instance.Profile.UpdateProfile();
            GameManager.instance.Profile.LastScene = SceneManager.GetActiveScene().name;

            GameManager.instance.isGameStarted = false;

            fader.Fade();


            StartCoroutine(IsFadingWait());

        }
    }

    // Verifica si está haciendose el fading y si ha acabado espera el tiempo definido y cambia de escena.
    IEnumerator IsFadingWait()
    {
        yield return new WaitUntil(fader.IsFadeFinished);
        yield return new WaitForSeconds(waitSecondsBeforeLoad);
        levelLoader.gameObject.SetActive(true);
        levelLoader.LoadLevel(levelToLoad);
    }

}
