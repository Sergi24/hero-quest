﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Localization;

// Abre una conversación de poca importancia si una misión ha sido completada.
public class ConversationOnCompletedQuestTrigger : MonoBehaviour
{
    // id de la misión completada
    public int idQuest;

    private CanvasController canvasController;

    // Nombre del personaje que dice la acción
    public string characterName;
    // sentencias en los respectivos idiomas
    public List<string> enConversationStrings;
    public List<string> snConversationStrings;
    public List<string> caConversationStrings;

    // si ya se ha ejecutado en esta escena
    private bool done;

    private void Awake()
    {
        canvasController = FindObjectOfType<CanvasController>();
    }

    // Use this for initialization
    void Start()
    {
        done = false;
    }

    // Ejecuta la conversación sin importancia si la misión ha sido completada y aun no se ha ejecutado.
    private void OnTriggerEnter(Collider other)
    {
        if (!done)
        {
            bool isQuestCompleted = false;
            foreach (Quest q in GameManager.instance.Profile.CompletedQuests)
                if (q.idQuest == idQuest)
                    isQuestCompleted = true;

            if (isQuestCompleted)
            {
                List<string> conversationStrings = GetConversationByCurrentLanguage();
                canvasController.ShowConversation(2, characterName, conversationStrings);
                done = true;
            }
        }
    }

    // Obtiene la lista de sentencias en el idioma actual
    public List<string> GetConversationByCurrentLanguage()
    {
        string currentLanguage = LeanLocalization.CurrentLanguage;

        if (currentLanguage.Equals("English"))
            return enConversationStrings;
        else if (currentLanguage.Equals("Spanish"))
            return snConversationStrings;
        else if (currentLanguage.Equals("Catalan"))
            return caConversationStrings;

        return null;
    }
}