﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Localization;

// Muestra el nombre de la subzona al entrar en ella
public class ShowTitleSubZoneOnTrigger : MonoBehaviour
{
    CanvasController canvasController;

    [LeanPhraseName]
    public string idStringTitle;

    private void Awake()
    {
        canvasController = FindObjectOfType<CanvasController>();
    }

    // Muestra el nombre de la subzona si ha entrado el personaje en esta.
    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            string title = LeanLocalization.GetTranslation(idStringTitle).Text;

            if (Extns.IsInFront(transform, other.transform))
            {
                canvasController.ShowTitle(2, title);
            }

        }
    }

}
