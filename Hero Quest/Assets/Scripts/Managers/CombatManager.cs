﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Se encarga de centralizar tipos de daños y formulas de combate. Al ser simple no se usa mucho pero tendria más métodos
public class CombatManager : MonoBehaviour
{
    #region Singleton

    public static CombatManager instance;

    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);


    }

    #endregion

    // Obtiene el daño final del ataque aplicando una formula de armadura para la redución del daño en porcentaje. (Cortesia de: http://wowwiki.wikia.com/wiki/Damage_reduction)
    public int GetFinalDamage(CharacterStats Attacker, CharacterStats receiber)
    {

        float armor = receiber.armor.GetValue();
        float damage = Attacker.damage.GetValue();

        damage *= GetTypeDamageMultiplier(Attacker.typeDamage, receiber.typeDamage);

        float percentage = (armor / ((85 * Attacker.Level) + armor + 400)) * 100;

        int finalDamage = (int)(damage - damage * (percentage / 100));
        //Debug.Log("FinalDamage(): " + finalDamage + ", Reduction:" + Attacker.damage.GetValue() * (percentage / 100));

        return finalDamage;
    }

    // No aplicado, da el multiplicador del daño mediante si este es de un tipo concreto y el tipo mágico del receptor.
    private float GetTypeDamageMultiplier(MagicTypes attackerType, MagicTypes receiberType)
    {
        if (attackerType == MagicTypes.Normal)
            return ConstantMultipliers.normalOverEverything;


        if (attackerType == MagicTypes.Fire && receiberType == MagicTypes.Fire)
            return ConstantMultipliers.fireOverFireMultiplier;
        else if (attackerType == MagicTypes.Fire && receiberType == MagicTypes.Life)
            return ConstantMultipliers.fireOverLifeMultiplier;
        else if (attackerType == MagicTypes.Fire && receiberType == MagicTypes.Water)
            return ConstantMultipliers.fireOverWaterMultiplier;
        else if (attackerType == MagicTypes.Fire)
            return ConstantMultipliers.fireOverOthersMultiplier;

        else if (attackerType == MagicTypes.Life && receiberType == MagicTypes.Fire)
            return ConstantMultipliers.lifeOverFireMultiplier;
        else if (attackerType == MagicTypes.Life && receiberType == MagicTypes.Life)
            return ConstantMultipliers.lifeOverLifeMultiplier;
        else if (attackerType == MagicTypes.Life && receiberType == MagicTypes.Water)
            return ConstantMultipliers.lifeOverWaterMultiplier;
        else if (attackerType == MagicTypes.Life)
            return ConstantMultipliers.lifeOverOthersMultiplier;

        else if (attackerType == MagicTypes.Water && receiberType == MagicTypes.Fire)
            return ConstantMultipliers.waterOverFireMultiplier;
        else if (attackerType == MagicTypes.Water && receiberType == MagicTypes.Life)
            return ConstantMultipliers.waterOverLifeMultiplier;
        else if (attackerType == MagicTypes.Water && receiberType == MagicTypes.Water)
            return ConstantMultipliers.waterOverWaterMultiplier;
        else if (attackerType == MagicTypes.Water)
            return ConstantMultipliers.waterOverOthersMultiplier;

        else if (attackerType == MagicTypes.Light && receiberType == MagicTypes.Light)
            return ConstantMultipliers.lightOverLightMultiplier;
        else if (attackerType == MagicTypes.Light && receiberType == MagicTypes.Shadows)
            return ConstantMultipliers.lightOverShadowsMultiplier;
        else if (attackerType == MagicTypes.Light)
            return ConstantMultipliers.lightOverOthersMultiplier;

        else if (attackerType == MagicTypes.Shadows && receiberType == MagicTypes.Light)
            return ConstantMultipliers.shadowsOverLightMultiplier;
        else if (attackerType == MagicTypes.Shadows && receiberType == MagicTypes.Shadows)
            return ConstantMultipliers.shadowsOverShadowsMultiplier;
        else if (attackerType == MagicTypes.Shadows)
            return ConstantMultipliers.shadowsOverOthersMultiplier;

        throw new System.Exception("GettypeDamageMultiplier not found.");
    }

}
