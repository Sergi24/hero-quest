﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Cameras;

// Manager que centraliza todos los datos importantes entre escenas
public class GameManager : MonoBehaviour
{
    #region Singleton

    public static GameManager instance;

    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);

        if (isTesting)
            Testing();
        else
            StartProfile();
    }

    #endregion

    // Guarda toda la información serializable
    public Profile Profile;
    // Guarda todos los datos generales
    public GeneralSettings GeneralSettings;

    // Guarda datos de la pantalla de carga
    public Advice TempAdvice;
    public Sprite TempSprite;

    // Mira si el juego está en pausa o no y sigue contando los segundos para añadirlos al tiempo de juego.
    public bool isPlaying = false;
    float timePLayedAux = 0;

    // Referncias al controlador de la cámara.
    public FreeLookCam freeLockCam;

    public CanvasController canvasController;

    public bool isGameStarted = false;
    public bool isMainMenu = true;

    // //////////////////////// TESTING ////////////////////////////////
    // Booleano para testear con un profile creado al inicializar la escena.
    public bool isTesting = false;

    public Item[] ItemsTest;
    public bool bItemsTest;

    public Quest TestQuest;



    private void Testing()
    {
        Debug.Log(Application.persistentDataPath);
        // called on Awake
        //Debug.Log("Testing Game Manager");
        Profile = DAOManager.UnserializeProfile("TestProfile");

        // Por algún motivo que desconozco, salen 2 objetivos en la misión
        /*if (Profile.CurrentQuests != null)
            if (Profile.CurrentQuests.Count > 0)
                Debug.LogError(Profile.CurrentQuests[0].Objectives.Count);*/

        if (Profile == null)
        {

            List<Quest> currentQuests = new List<Quest>();
            List<Quest> completedQuests = new List<Quest>();

            TestQuest = QuestFactory.GetQuestByName("LarryQuest") as Quest;

            completedQuests.Add(TestQuest);

            PlayerInfo playerInfo = new PlayerInfo
            {
                namePlayer = "TestPlayer",
                CurrentHealth = 300,
                currentMana = 300,
                Level = 1
            };

            InventoryInfo newInventory = new InventoryInfo();
            newInventory.items.Add(new ItemInfo() { idItem = 1 });
            newInventory.items.Add(new ItemInfo() { idItem = 2 });
            newInventory.items.Add(new ItemInfo() { idItem = 3 });
            newInventory.items.Add(new ItemInfo() { idItem = 4 });
            newInventory.items.Add(new ItemInfo() { idItem = 5 });
            newInventory.items.Add(new ItemInfo() { idItem = 6 });



            Profile = new Profile
            {
                idProfile = "TestProfile",
                playerInfo = playerInfo,
                CurrentQuests = currentQuests,
                CompletedQuests = completedQuests,
                inventory = newInventory,
                LastScene = "Town"

            };

            foreach (Quest q in currentQuests)
            {
                q.InitObjectives();
            }
            Debug.Log("TestProfile created.");
        }
        else
            Debug.Log("Profile found.");



    }

    // //////////////////////// END TESTING ////////////////////////////////

    // Inicializa el perfil si está vacío
    private void StartProfile()
    {
        Debug.Log(Application.persistentDataPath);

        if (Profile != null)
        {
            Profile = DAOManager.UnserializeProfile(Profile.idProfile);
        }
        else
        {
            Debug.LogError("Profile not found, Error?");
        }

        GeneralSettings = DAOManager.UnserializeSettings();
        if (GeneralSettings == null)
        {
            {
                GeneralSettings = new GeneralSettings
                {
                    Quality = 3,
                    FullScreen = false,
                    MainVolume = 0,
                    MusicVolume = 0,
                    SfxVolume = -5
                };
            }
        }

        canvasController = FindObjectOfType<CanvasController>();

    }

    // Pausa el juego
    public void Pause()
    {
        Debug.Log("Pause()");
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            isPlaying = false;
            LockMouse(false);
        }
        else
        {
            Time.timeScale = 1;
            isPlaying = true;
            LockMouse(true);
        }

        canvasController.Pause();
        SoundManager.instance.Pause();
    }

    // Bloque/Desbloquea el mouse.
    public void LockMouse(bool b)
    {
        if (b)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }
    }

    private void Update()
    {
        // Si no estamos en el menu principal...
        if (!isMainMenu)
        {
            // Inicia la escena
            if (Input.GetKeyDown(KeyCode.Escape) && isGameStarted)
            {
                Pause();
            }

            // Si está jugando aumentamos los segundos jugados y los guardamos en el perfil del jugador
            if (isPlaying)
            {


                timePLayedAux += Time.deltaTime;
                if (timePLayedAux > 1)
                {
                    timePLayedAux--;
                    Profile.TimePlayed++;
                }
            }
        }
    }
}

