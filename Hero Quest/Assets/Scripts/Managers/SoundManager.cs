﻿using System.Collections;
using UnityEngine;
using UnityEngine.Audio;

// Controla el sonido de música y audiomixers así como algunos efectos de sonido del juego a nivel general.
public class SoundManager : MonoBehaviour
{

    #region singleton

    public static SoundManager instance = null;

    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }

    #endregion

    // AudioMixers
    public AudioMixer audioMixer;
    public AudioMixerSnapshot paused;
    public AudioMixerSnapshot unpaused;

    // Audiosources generales
    public AudioSource musicAudioSource;
    public AudioSource sfxAudioSource;

    // Base de datos de musica
    public MusicDatabase musicDatabase;
    public CombatSoundsDatabase combatsoundsDatabase;

    // Variables para controlar la musica
    private int lastMusicId;
    private bool isCombatMusicActive = false;
    public int charactersAttackingPlayer;
    public float transitionDuration = 2;

    // Referencia al IEnumerator de transición de musica para que no se duplique
    IEnumerator playMusicEnumerator;

    public void Init(int idMusic)
    {
        Debug.Log("SoundManagerInit()");
        StartCoroutine(UpdateAudioMixersAfterFrame(idMusic));
    }

    // Actualiza los audiomixers y mediante la id de música se ejecuta la música correspondiente.
    IEnumerator UpdateAudioMixersAfterFrame(int idMusic)
    {
        yield return new WaitForEndOfFrame();
        UpdateAudioMixers();
        SoundManager.instance.PlayMusicClip(idMusic);
    }

    private void Update()
    {
        // Si hay alguien atacando al jugador...
        if (charactersAttackingPlayer > 0)
        {
            // Si la música de combate no está activa se para con transitionDuration la musica actual y se activa musica de combate.
            if (!isCombatMusicActive)
            {

                if (playMusicEnumerator != null)
                    StopCoroutine(playMusicEnumerator);

                int n = Random.Range(0, musicDatabase.combatClips.Count);

                playMusicEnumerator = PlayCombatMusic(n);
                StartCoroutine(playMusicEnumerator);

                isCombatMusicActive = true;
            }
        }
        // Si no hay nadie atacando al jugador...
        else
        {
            // Si la musica de combate está activa se desactiva con el tiempo de TransitionDuration.
            if (isCombatMusicActive)
            {
                if (playMusicEnumerator != null)
                    StopCoroutine(playMusicEnumerator);

                playMusicEnumerator = PlayMusic(lastMusicId);
                StartCoroutine(playMusicEnumerator);

                isCombatMusicActive = false;
            }
        }
    }

    // Ejecuta un clip musical
    public void PlayMusicClip(int id)
    {
        lastMusicId = id;

        if (!musicAudioSource.isPlaying)
        {
            musicAudioSource.clip = musicDatabase.musicClips[id];
            musicAudioSource.Play();
        }
        else
        {
            if (playMusicEnumerator != null)
                StopCoroutine(playMusicEnumerator);
            playMusicEnumerator = PlayMusic(id);
            StartCoroutine(playMusicEnumerator);
        }
    }

    // Para un clip musical
    public void StopMusicClip()
    {
        musicAudioSource.Stop();
    }

    // Ejecuta una música en el tiempo de transición definido.
    IEnumerator PlayMusic(int id)
    {
        Debug.Log("PlayMusic()");

        float counter = 0;

        float currentMusicValue = GameManager.instance.GeneralSettings.MusicVolume;
        float i = 0;

        while (i != -20)
        {
            i = Mathf.Lerp(currentMusicValue, -20, counter / transitionDuration);
            audioMixer.SetFloat("Music", i);
            counter += Time.deltaTime;
            yield return null;
        }

        musicAudioSource.clip = musicDatabase.musicClips[id];
        musicAudioSource.Play();
        audioMixer.SetFloat("Music", currentMusicValue);

        yield return null;
    }

    // Ejecuta una música de combate en el tiempo de transición definido
    IEnumerator PlayCombatMusic(int id)
    {
        Debug.Log("PlayCombatMusic()");

        float counter = 0;

        float startMusicValue = GameManager.instance.GeneralSettings.MusicVolume;
        float i = 0;
        while (i != -20)
        {
            i = Mathf.Lerp(startMusicValue, -20, counter / transitionDuration);
            audioMixer.SetFloat("Music", i);
            counter += Time.deltaTime;
            yield return null;
        }

        musicAudioSource.clip = musicDatabase.combatClips[id];
        musicAudioSource.Play();
        audioMixer.SetFloat("Music", startMusicValue);

        yield return null;
    }

    /// /////////////////////////////////////////////////////////////////////////

    // Llama a la función LowPass para hacer una transición de snapshot.
    public void Pause()
    {
        Lowpass();
    }

    // Activa / Desactiva el snapshot paused/nonPaused. Este toca el lowPass para hacer un efecto de "musica pausada"
    void Lowpass()
    {
        if (Time.timeScale == 0)
        {
            paused.TransitionTo(.01f);
        }
        else
        {
            unpaused.TransitionTo(.01f);
        }
    }

    // Actualiza los audiomixers
    public void UpdateAudioMixers()
    {
        GeneralSettings generalSettings = GameManager.instance.GeneralSettings;

        SetMainVolume(generalSettings.MainVolume);
        SetMusicVolume(generalSettings.MusicVolume);
        SetSFXVolume(generalSettings.SfxVolume);
    }

    // Actualiza el volumen general
    public void SetMainVolume(float volume)
    {
        GeneralSettings generalSettings = GameManager.instance.GeneralSettings;

        if (volume == -20)
        {
            audioMixer.SetFloat("Master", -80);
            generalSettings.MainVolume = -80;
        }
        else
        {
            audioMixer.SetFloat("Master", volume);
            generalSettings.MainVolume = volume;
        }
    }

    // Actualiza el volumen de la música.
    public void SetMusicVolume(float volume)
    {
        GeneralSettings generalSettings = GameManager.instance.GeneralSettings;

        if (volume == -20)
        {
            audioMixer.SetFloat("Music", -80);
            generalSettings.MusicVolume = -80;
        }
        else
        {
            audioMixer.SetFloat("Music", volume);
            generalSettings.MusicVolume = volume;
        }
    }

    // Actualiza el volumen de los efectos especiales
    public void SetSFXVolume(float volume)
    {
        GeneralSettings generalSettings = GameManager.instance.GeneralSettings;

        if (volume == -20)
        {
            audioMixer.SetFloat("SFX", -80);
            generalSettings.SfxVolume = -80;
        }
        else
        {
            audioMixer.SetFloat("SFX", volume);
            generalSettings.SfxVolume = volume;
        }
    }

}
