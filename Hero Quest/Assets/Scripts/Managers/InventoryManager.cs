﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    #region Singleton

    public static InventoryManager instance;

    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }

    #endregion
    // Referencia al inventario del perfil
    public Inventory inventory;

    private void Start()
    {
        // Obtiene el inventario del perfil
        inventory = GameManager.instance.Profile.GetInventory();
    }

    // Añade items al inventario
    public void Add(Item item)
    {
        if (item.showInInventory)
        {
            if (inventory.items.Count >= inventory.space)
            {
                Debug.Log("Not enough room.");
                return;
            }

            inventory.items.Add(item);

            EventManager.ItemChanged(item);

        }
    }

    // Borra items del inventario
    public void Remove(Item item)
    {
        inventory.items.Remove(item);

        EventManager.ItemChanged(item);
    }



}
