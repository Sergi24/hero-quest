﻿using UnityEngine;

// Maneja todos los eventos del juego.
public class EventManager : MonoBehaviour
{
    // Player Events

    public delegate void OnPlayerDeadEventHandler(CharacterStats newCharacterGameObject);
    public static event OnPlayerDeadEventHandler OnPlayerdead;

    public static void PlayerDead(PlayerStats playerStats)
    {
        Debug.Log("Eventmanager player dead: " + playerStats.nameCharacter);
        if (OnPlayerdead != null)
            OnPlayerdead(playerStats);
    }


    // World Events

    public delegate void OnCharacterNewInWorldEventHandler(CharacterStats newCharacterGameObject);
    public static event OnCharacterNewInWorldEventHandler OnCharacterNewInWorld;

    public static void NewCharacter(CharacterStats newCharacterStats)
    {
        Debug.Log("Eventmanager new Character: " + newCharacterStats.nameCharacter);
        if (OnCharacterNewInWorld != null)
            OnCharacterNewInWorld(newCharacterStats);
    }

    // Battle Events

    public delegate void OnCharacterDeadEventHandler(GameObject character, CharacterStats killerCharacterStats);
    public static event OnCharacterDeadEventHandler OnCharacterDead;

    public static void CharacterDead(GameObject character, CharacterStats killerCharacterStats)
    {
        Debug.Log("Eventmanager Enemy: " + character.GetComponent<CharacterStats>());
        if (OnCharacterDead != null)
            OnCharacterDead(character, killerCharacterStats);
    }

    // Inventory Events

    public delegate void OnItemCollectedEventHandler(Item item);
    public static event OnItemCollectedEventHandler OnItemCollected;

    public static void ItemCollected(Item item)
    {
        Debug.Log("Eventmanager Collect item[ID=" + item.idItem + "]: " + item.name);
        if (OnItemCollected != null)
        {
            OnItemCollected(item);
        }
    }

    public delegate void OnItemChangedEventHandler(Item item);
    public static event OnItemChangedEventHandler OnItemChanged;

    public static void ItemChanged(Item item)
    {
        Debug.Log("Eventmanager Change: item[ID=" + item.idItem + "], " + item.name);
        if (OnItemChanged != null)
        {
            OnItemChanged(item);
        }
    }

    // Equipment Events

    public delegate void OnEquipmentChangedEventHandler(Equipment newItem, Equipment oldItem);
    public static event OnEquipmentChangedEventHandler OnEquipmentChanged;

    public static void EquipmentChanged(Equipment newEquipment, Equipment oldEquipment)
    {
        if (OnEquipmentChanged != null)
            OnEquipmentChanged(newEquipment, oldEquipment);
    }

    // Quest Events

    public delegate void OnQuestAcceptedEventHandler();
    public static event OnQuestAcceptedEventHandler OnQuestAccepted;

    public static void AcceptQuest()
    {
        Debug.Log("Eventmanager quest accepted");
        if (OnQuestAccepted != null)
            OnQuestAccepted();
    }

    public delegate void OnQuestCompletedEventHandler();
    public static event OnQuestCompletedEventHandler OnQuestCompleted;

    public static void QuestCompleted()
    {
        Debug.Log("Eventmanager quest completed");
        if (OnQuestCompleted != null)
            OnQuestCompleted();
    }

    public delegate void OnObjectiveUpdatedEventHandler();
    public static event OnObjectiveUpdatedEventHandler OnObjectiveUpdated;

    public static void ObjectiveUpdated()
    {
        Debug.Log("Eventmanager objective updated");
        if (OnObjectiveUpdated != null)
            OnObjectiveUpdated();
    }

}
