﻿using UnityEngine;
using System.Xml.Serialization;
using System.IO;


// Controla el acceso a los archivos que hacen la persistencia del juego.
public static class DAOManager
{
    // id de la configuración general del juego (volumen de sonido, calidad gráfica...)
    private static string idSettings = "idSettings";

    // ////////////////////// PROFILE ///////////////////////////////////////

    // Serializa el perfil
    public static void SerializeProfile(string idProfile, Profile profile)
    {
        //Debug.Log("SerializeProfile()");
        // Si no está en el menu principal, actualiza el perfil.
        if (EquipmentManager.instance != null && InventoryManager.instance != null)
            GameManager.instance.Profile.UpdateProfile();

        XmlSerializer xmlSerializer = new XmlSerializer(typeof(Profile));
        FileStream file;

        if (File.Exists(Application.persistentDataPath + "/" + idProfile + ".xml"))
        {

            file = File.Open(Application.persistentDataPath + "/" + idProfile + ".xml", FileMode.Open);
        }
        else
        {
            file = new FileStream(Application.persistentDataPath + "/" + idProfile + ".xml", FileMode.Create);
        }

        xmlSerializer.Serialize(file, profile);

        file.Close();

    }

    // Deserializa el perfil
    public static Profile UnserializeProfile(string idProfile)
    {
        if (File.Exists(Application.persistentDataPath + "/" + idProfile + ".xml"))
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Profile));
            FileStream file = File.Open(Application.persistentDataPath + "/" + idProfile + ".xml", FileMode.Open);
            Profile profile = xmlSerializer.Deserialize(file) as Profile;
            file.Close();
            return profile;
        }
        return null;
    }

    // Borra el perfil
    public static void RemoveProfile(string idProfile)
    {
        if (File.Exists(Application.persistentDataPath + "/" + idProfile + ".xml"))
        {
            File.Delete(Application.persistentDataPath + "/" + idProfile + ".xml");
        }

    }

    // ////////////////////// SETTINGS ///////////////////////////////////////

    // Serializa los ajustes generales
    public static void SerializeSettings()
    {
        XmlSerializer xmlSerializer = new XmlSerializer(typeof(GeneralSettings));
        FileStream file;

        if (File.Exists(Application.persistentDataPath + "/" + idSettings + ".xml"))
        {

            file = File.Open(Application.persistentDataPath + "/" + idSettings + ".xml", FileMode.Open);
        }
        else
        {
            file = new FileStream(Application.persistentDataPath + "/" + idSettings + ".xml", FileMode.Create);
        }

        xmlSerializer.Serialize(file, GameManager.instance.GeneralSettings);

        file.Close();
    }

    // Deserializa los ajustes generales
    public static GeneralSettings UnserializeSettings()
    {
        if (File.Exists(Application.persistentDataPath + "/" + idSettings + ".xml"))
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(GeneralSettings));
            FileStream file = File.Open(Application.persistentDataPath + "/" + idSettings + ".xml", FileMode.Open);
            GeneralSettings generalSettings = xmlSerializer.Deserialize(file) as GeneralSettings;
            file.Close();
            return generalSettings;
        }

        return null;

    }
}
