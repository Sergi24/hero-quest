﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Controla el equipamiento del personaje
public class EquipmentManager : MonoBehaviour
{

    #region Singleton

    public static EquipmentManager instance;

    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }

    #endregion

    // Equipamiento actual del personaje
    public Equipment[] currentEquipment;
    // Numero de slots disponibles para equipar en los slots de equipamiento del personaje.
    public GameObject[,] currentEquipmentSlots;

    // Personaje a manejar el equipamiento
    public GameObject targetCharacter;

    public void Init()
    {
        // Debug.Log("Init EquipmentManager");
        // Obtenemos el nº de slots de equipamiento del personaje mediante el enum.
        int numSlots = System.Enum.GetNames(typeof(EquipmentSlot)).Length;
        currentEquipment = new Equipment[numSlots];
        currentEquipmentSlots = new GameObject[numSlots, 2];

        // Equipamos el equipo que tiene equipado.
        EquipAllDefault();
    }

    // Equipa un equipable
    public void Equip(Equipment newItem)
    {
        //Debug.Log("Equipando...");

        // Antiguo item equipado del slot de equipamiento.
        Equipment oldItem = null;

        int slotIndex = (int)newItem.equipSlot;

        if (currentEquipment[slotIndex] != null)
        {
            // Obtenemos el antiguo equipable
            oldItem = currentEquipment[slotIndex];
            // Añadimos el antiguo equipable al inventario
            InventoryManager.instance.Add(oldItem);

        }
        // Informamos de que el equip ha cambiado
        EventManager.EquipmentChanged(newItem, oldItem);

        // Equipamos el nuevo equipable.
        currentEquipment[slotIndex] = newItem;

        // Inicializamos el prefab del nuevo equipable
        if (newItem.prefab)
        {
            ActivatePrefab(newItem.prefab, slotIndex);
        }

    }

    // Desequipa un equipable. Al no tener ventana del personaje no se usa.
    void Unequip(int slotIndex)
    {
        if (currentEquipment[slotIndex] != null)
        {
            Equipment oldItem = currentEquipment[slotIndex];
            InventoryManager.instance.Add(oldItem);

            currentEquipment[slotIndex] = null;
            if (currentEquipmentSlots[slotIndex, 0] != null)
            {
                Destroy(currentEquipmentSlots[slotIndex, 0].gameObject);
            }

            EventManager.EquipmentChanged(null, oldItem);

        }


    }

    // Se equipa todo lo que hay serializado
    void EquipAllDefault()
    {
        Equipment[] currentEquipment = GameManager.instance.Profile.GetcurrentEquipment();

        foreach (Equipment e in currentEquipment)
        {
            if (e != null)
                Equip(e);
        }
    }

    // Activa el prefab del equipable equipado
    void ActivatePrefab(GameObject newEquipment, int slotIndex)
    {
        // Borra el equipamiento actual para que no se dupliquen items
        if (currentEquipmentSlots[slotIndex, 0] != null)
        {
            Destroy(currentEquipmentSlots[slotIndex, 0]);
            Destroy(currentEquipmentSlots[slotIndex, 1]);
        }

        // se obtiene los CharacterStats del objetivo
        CharacterStats characterStats = targetCharacter.GetComponent<CharacterStats>();

        // Se inicializan en la posición de armado y desarmado.
        GameObject newEquipmentGameObject = Instantiate(newEquipment);
        GameObject newEquipmentUnarmedGameObject = Instantiate(newEquipment);

        // Si el objeto es un arma, va a las posiciones de armamento.
        if (slotIndex == 0)
        {
            newEquipmentGameObject.transform.position = characterStats.armedWeapon.transform.position;
            newEquipmentGameObject.transform.rotation = characterStats.armedWeapon.transform.rotation;
            newEquipmentGameObject.transform.parent = characterStats.armedWeapon.transform;

            newEquipmentUnarmedGameObject.transform.position = characterStats.unarmedWeapon.transform.position;
            newEquipmentUnarmedGameObject.transform.rotation = characterStats.unarmedWeapon.transform.rotation;
            newEquipmentUnarmedGameObject.transform.parent = characterStats.unarmedWeapon.transform;
        }
        // Si el objeto es un escudo, va a las posiciones de escudo.
        else if (slotIndex == 1)
        {
            newEquipmentGameObject.transform.position = characterStats.armedShield.transform.position;
            newEquipmentGameObject.transform.rotation = characterStats.armedShield.transform.rotation;
            newEquipmentGameObject.transform.parent = characterStats.armedShield.transform;

            newEquipmentUnarmedGameObject.transform.position = characterStats.unarmedShield.transform.position;
            newEquipmentUnarmedGameObject.transform.rotation = characterStats.unarmedShield.transform.rotation;
            newEquipmentUnarmedGameObject.transform.parent = characterStats.unarmedShield.transform;
        }

        // se agregan a la matriz de equipables del manager de equipamiento.
        currentEquipmentSlots[slotIndex, 0] = newEquipmentGameObject;
        currentEquipmentSlots[slotIndex, 1] = newEquipmentUnarmedGameObject;

        // Si hay WeaponTrigger lo añade al controlador del jugador (InputPlayerController).
        if (newEquipmentGameObject.GetComponent<WeaponTrigger>() != null)
        {
            newEquipmentGameObject.GetComponentInParent<InputPlayerController>().weaponTriggers = new List<WeaponTrigger>();
            newEquipmentGameObject.GetComponentInParent<InputPlayerController>().weaponTriggers.Add(newEquipmentGameObject.GetComponent<WeaponTrigger>()); ;
            newEquipmentGameObject.GetComponent<WeaponTrigger>().objectiveTags.Add("Enemy");
        }

        // Si hay ShieldStats lo añade al controlador del jugador (InputPlayerController).
        if (newEquipmentGameObject.GetComponent<ShieldStats>() != null)
            newEquipmentGameObject.GetComponentInParent<InputPlayerController>().shieldStats = newEquipmentGameObject.GetComponent<ShieldStats>();

    }

}
