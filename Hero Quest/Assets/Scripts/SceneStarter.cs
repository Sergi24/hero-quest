﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Lean.Localization;
using UnityStandardAssets.Cameras;

// Controla la inicialización de la escena cuando no es el menú principal
public class SceneStarter : MonoBehaviour
{
    public GameObject playerPrefab;

    private StartPosition[] startingPositions;

    private CanvasController canvasController;

    [LeanPhraseName]
    public string idZoneName;

    public int idMusicScene;

    public GameObject debugLoadPosition;

    // Prepara los managers y el escenario para jugarlo
    private void Start()
    {
        GameManager.instance.isMainMenu = false;
        GameManager.instance.GeneralSettings.Init();
        SoundManager.instance.Init(idMusicScene);

        //////////////////////////////////////////////////////////////
        // ARREGLO: no sé xk salen 2 objetivos al deserializar...
        if (GameManager.instance.Profile != null)
            if (GameManager.instance.Profile.CurrentQuests.Count > 0)
            {
                if (GameManager.instance.Profile.CurrentQuests[0].Objectives.Count > 1)
                {
                    for (int i = 0; i < GameManager.instance.Profile.CurrentQuests[0].Objectives.Count; i++)
                        if (i != 0)
                            GameManager.instance.Profile.CurrentQuests[0].Objectives.Remove(GameManager.instance.Profile.CurrentQuests[0].Objectives[i]);
                }
            }
        //////////////////////////////////////////////////////////////

        RestartCurrentQuestEventListeners();

        GameManager.instance.canvasController = FindObjectOfType<CanvasController>();
        canvasController = FindObjectOfType<CanvasController>();


        GameManager.instance.Profile.CurrentScene = SceneManager.GetActiveScene().name;

        GameManager.instance.Profile.CurrentZone = LeanLocalization.GetTranslation(idZoneName).Text;

        GameObject player = Instantiate(playerPrefab);

        Init(player);

        if (debugLoadPosition != null)
        {
            player.transform.position = debugLoadPosition.transform.position;
            player.transform.rotation = debugLoadPosition.transform.rotation;
            player.gameObject.SetActive(true);
            EventManager.NewCharacter(player.GetComponentInChildren<CharacterStats>());
        }
        else
        {

            startingPositions = transform.GetComponentsInChildren<StartPosition>();

            foreach (StartPosition startPosition in startingPositions)
            {
                Debug.Log(GameManager.instance.Profile.LastScene + "   " + startPosition.GetComponent<StartPosition>().sceneName);
                if (startPosition.sceneName.Equals(GameManager.instance.Profile.LastScene))
                {
                    player.transform.position = startPosition.transform.position;
                    player.transform.rotation = startPosition.transform.rotation;
                    player.gameObject.SetActive(true);
                    EventManager.NewCharacter(player.GetComponentInChildren<CharacterStats>());
                    break;
                }
            }
        }

        canvasController.ShowTitle(4, GameManager.instance.Profile.CurrentZone);

    }

    // actualiza información necesaria para funcionar correctamente el juego
    private void Init(GameObject player)
    {
        GameManager.instance.freeLockCam = player.transform.GetComponentInChildren<FreeLookCam>();
        EquipmentManager.instance.targetCharacter = player.GetComponentInChildren<PlayerStats>().gameObject;
        //EquipmentManager.instance.Init();
    }

    // Reinicia los listeners de los objetivos de las misiones actuales
    private void RestartCurrentQuestEventListeners()
    {
        foreach (Quest q in GameManager.instance.Profile.CurrentQuests)
            q.InitObjectives();
    }

}
