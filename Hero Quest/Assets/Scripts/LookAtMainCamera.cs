﻿using UnityEngine;

// Controla que el canvas de los personajes en world space esten mirando a la cámara
public class LookAtMainCamera : MonoBehaviour
{
    // Camara
    private Camera cam;
    // Necesario rotar 180 grados?
    public bool rotate180 = false;

    // Update is called once per frame
    void Update()
    {
        cam = Camera.main;

        if (cam != null)
        {
            transform.LookAt(cam.transform);
            if (rotate180)
                transform.rotation *= Quaternion.Euler(0, 180, 0);
        }
        else
            if (Camera.main != null)
            cam = Camera.main;
    }
}
