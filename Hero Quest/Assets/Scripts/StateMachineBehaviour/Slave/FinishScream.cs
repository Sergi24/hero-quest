﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishScream : StateMachineBehaviour
{

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        // Al finalizar la animación del grito amenazador actualiza el booleano del animator
        animator.SetBool("IsScreaming", false);
    }
}
