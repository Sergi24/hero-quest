﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlaveAttackStateMachineController : StateMachineBehaviour
{
    AISlaveCharacterController aiSlave;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        aiSlave = animator.GetComponent<AISlaveCharacterController>();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        float currentPercentage = stateInfo.normalizedTime;
        //Debug.Log(currentPercentage);

        // Si esta en el porcentaje de la animación que puede hacer daño se activa el canHit.
        if (currentPercentage < .25 && currentPercentage > .20)
        {
            aiSlave.canHit = true;
            //Debug.Log("isattacking = true");
        }
        else if (currentPercentage < .50 && currentPercentage > .45)
        {
            aiSlave.canHit = false;
            //Debug.Log("isattacking = false");
        }

    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        // Actualizamos animator
        animator.SetBool("IsAttacking", false);
    }
}
