﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack2StateMachineController : StateMachineBehaviour
{
    InputPlayerController inputPlayerController;
    AudioCharacterController audioController;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        inputPlayerController = animator.GetComponent<InputPlayerController>();

        // Actualizamos el animator
        animator.SetBool("IsAttacking", true);

        // sfx de ataque, Lo he quitado porque no me gustaba como quedaba
        //audioController = animator.GetComponent<AudioCharacterController>();
        //audioController.PlayAttackClip();

    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        float currentPercentage = stateInfo.normalizedTime;
        //Debug.Log(currentPercentage);

        // Porcentaje de la animación que puede hacer daño
        if (currentPercentage < .40 && currentPercentage > .35)
        {
            inputPlayerController.canHit = true;
            //Debug.Log("isattacking = true");
        }
        else if (currentPercentage < .70 && currentPercentage > .65)
        {
            inputPlayerController.canHit = false;
            //Debug.Log("isattacking = false");
        }

        // Next attack
        if (currentPercentage > .90)
        {
            animator.SetBool("NextAttack", true);
        }

    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        // Reiniciamos el ataque
        if (inputPlayerController.weaponTriggers.Count > 0)
            inputPlayerController.weaponTriggers[0].RestartEnemyHits();
        // Actualizamos animator
        animator.SetBool("IsAttacking", false);
        animator.SetBool("NextAttack", false);
    }
}
