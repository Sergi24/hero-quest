﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack3StateMachineController : StateMachineBehaviour
{
    InputPlayerController inputPlayerController;
    AudioCharacterController audioController;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        inputPlayerController = animator.GetComponent<InputPlayerController>();

        // sfx de ataque, Lo he quitado porque no me gustaba como quedaba
        //audioController = animator.GetComponent<AudioCharacterController>();
        //audioController.PlayAttackClip();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        float currentPercentage = stateInfo.normalizedTime;
        //Debug.Log(currentPercentage);

        // Porcentaje de la animación que puede hacer daño
        if (currentPercentage < .50 && currentPercentage > .45)
        {
            inputPlayerController.canHit = true;
            //Debug.Log("isattacking = true");
        }
        else if (currentPercentage < .70 && currentPercentage > .65)
        {
            inputPlayerController.canHit = false;
            //Debug.Log("isattacking = false");
        }

    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        // Actualizamos el animator
        animator.SetBool("IsAttacking", false);
    }
}
