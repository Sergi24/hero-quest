﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordShieldAttack1StateMachineController : StateMachineBehaviour
{
    AICharacterController aiCharacterController;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        aiCharacterController = animator.GetComponent<AICharacterController>();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        float currentPercentage = stateInfo.normalizedTime;
        //Debug.Log(currentPercentage);

        // Porcentaje de la animación que puede golpear

        if (currentPercentage < .35 && currentPercentage > .30)
        {
            aiCharacterController.canHit = true;
            //Debug.Log("isattacking = true");
        }
        else if (currentPercentage < .60 && currentPercentage > .55)
        {
            aiCharacterController.canHit = false;
            //Debug.Log("isattacking = false");
        }

    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        animator.SetBool("IsAttacking", false);
    }
}
