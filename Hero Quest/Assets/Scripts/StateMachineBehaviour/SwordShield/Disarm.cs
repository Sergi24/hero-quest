﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disarm : StateMachineBehaviour
{

    // Arma al objetivo desactivando el arma armada y activando la desarmada.
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        CharacterStats characterStats = animator.GetComponent<CharacterStats>();

        if (characterStats.unarmedWeapon != null && characterStats.armedWeapon != null)
        {
            characterStats.unarmedWeapon.SetActive(true);
            characterStats.armedWeapon.SetActive(false);
        }
        if (characterStats.unarmedShield != null && characterStats.armedShield != null)
        {
            characterStats.unarmedShield.SetActive(true);
            characterStats.armedShield.SetActive(false);
        }

    }

}
