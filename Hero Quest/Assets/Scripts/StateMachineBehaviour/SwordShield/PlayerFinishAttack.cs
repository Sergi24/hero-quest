﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFinishAttack : StateMachineBehaviour
{

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Actualiza el animator
        animator.SetBool("Attack1", false);
        animator.SetBool("Attack2", false);
        animator.SetBool("Attack3", false);

        InputPlayerController inputPlayerController = animator.GetComponent<InputPlayerController>();

        // Actualiza el isAttacking del InputPlayerController
        if (inputPlayerController != null)
            inputPlayerController.isAttacking = false;

    }
}
