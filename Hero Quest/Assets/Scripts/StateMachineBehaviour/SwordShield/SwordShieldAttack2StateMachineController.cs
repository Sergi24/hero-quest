﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordShieldAttack2StateMachineController : StateMachineBehaviour
{
    AICharacterController aiCharacterController;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        aiCharacterController = animator.GetComponent<AICharacterController>();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        float currentPercentage = stateInfo.normalizedTime;
        //Debug.Log(currentPercentage);

        // Porcentaje de la aninmación que puede golpear
        if (currentPercentage < .40 && currentPercentage > .35)
        {
            aiCharacterController.canHit = true;
            //Debug.Log("isattacking = true");
        }
        else if (currentPercentage < .70 && currentPercentage > .65)
        {
            aiCharacterController.canHit = false;
            //Debug.Log("isattacking = false");
        }

    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        // Actualizamos animator
        animator.SetBool("IsAttacking", false);
    }
}
