﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arm : StateMachineBehaviour
{
    // Arma al objetivo desactivando el arma desarmada y activando la armada.
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        CharacterStats characterStats = animator.GetComponent<CharacterStats>();

        if (characterStats.unarmedWeapon != null && characterStats.armedWeapon != null)
        {
            characterStats.unarmedWeapon.SetActive(false);
            characterStats.armedWeapon.SetActive(true);
        }

        if (characterStats.unarmedShield != null && characterStats.armedShield != null)
        {
            characterStats.unarmedShield.SetActive(false);
            characterStats.armedShield.SetActive(true);
        }

    }
}
