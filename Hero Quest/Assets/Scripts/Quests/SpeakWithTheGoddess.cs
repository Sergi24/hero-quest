﻿using System.Collections.Generic;

// Misión 1 - Habla con la diosa
public class SpeakWithTheGoddess : Quest
{

    public SpeakWithTheGoddess()
    {

        idQuest = 1;

        EnTitle = "Speak with the Goddess of hope";
        EnDescription = "You are appeared on a strange cave, something that claims to be a goddess wants to talk with you.";

        EsTitle = "Habla con la diosa de la esperanza";
        EsDescription = "Has aparecido en una extraña cueva, algo que afirma ser una diosa quiere hablar contigo.";

        CaTitle = "Parla amb la deessa";
        CaDescription = "Has aparegut en una estranya cova, quelcom que afirma ser una deessa vol parlar amb tu.";

        ExperienceReward = 100;

        ItemsReward = new List<ItemInfo>();

        // Basic sword and shield items
        ItemInfo itemInfo1 = new ItemInfo
        {
            idItem = 3
        };

        ItemInfo itemInfo2 = new ItemInfo
        {
            idItem = 4
        };

        ItemsReward.Add(itemInfo1);
        ItemsReward.Add(itemInfo2);


        Objectives = new List<Objective>
        {
            new Objective(this.idQuest, 0,
            "Speak with the Goddess",
            "Habla con la diosa",
            "Parla amb la deessa", false, 0, 1)
        };

    }
}