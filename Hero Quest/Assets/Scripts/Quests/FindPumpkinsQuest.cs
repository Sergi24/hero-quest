﻿using System.Collections.Generic;

// Misión 2 - Busqueda de calabazas
public class FindPumpkinsQuest : Quest
{

    public FindPumpkinsQuest()
    {

        idQuest = 2;

        EnTitle = "Pumpkins!";
        EnDescription = "Find the lost pumpkins.";

        EsTitle = "¡Calabazas!";
        EsDescription = "Busca las calabazas perdidas.";

        CaTitle = "Carabasses!";
        CaDescription = "Busca les carabasses perdudes.";

        ExperienceReward = 100;

        ItemsReward = new List<ItemInfo>();

        idQuests = new List<int>
        {
            1
        };

        Objectives = new List<Objective>
        {

            new CollectionObjective(this.idQuest, 0, 2,
            "4 pumpkins have disappeared from the field of pumpkins... Find them!\nSpeak with Jack on the pumpkings camp when you recovered them.",
            "4 calabazas han desaparecido del campo de calabazas... ¡Búscalas!\nHabla con Jack en el campo de calabazas cuando las hayas recuperado.",
            "4 carabasses han desaparegut del camp de carabasses... Has de trobar-les!\nParla amb en Jack al camp de carabasses quan les hagis recuperat.", false, 0, 4)

        };
    }



}
