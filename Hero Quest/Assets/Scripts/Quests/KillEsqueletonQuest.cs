﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Misión 4 - Matar 3 esqueletos
public class KillEsqueletonQuest : Quest
{

    public KillEsqueletonQuest()
    {
        // Debug.Log("KillEsqueleton initialized Contructor");

        idQuest = 4;


        EnTitle = "The undead plague";
        EnDescription = "The undead are \"living\" in the swamp, they want to expand their territory, help us to annihilate them!";

        EsTitle = "La plaga no muerta.";
        EsDescription = "Los no muertos están \"viviendo\" en el pantano, ellos quieren expandir su territorio, ayudanos a aniquilarlos!";

        CaTitle = "La plaga no morta";
        CaDescription = "Els no morts estan \"vivint\" al pantà, ells volen expandir el seu territori, ajúdens a aniquilar-los!";

        ExperienceReward = 100;

        ItemsReward = new List<ItemInfo>();

        idQuests = new List<int>
        {
            3
        };

        Objectives = new List<Objective>
        {
            new KillObjective(this.idQuest, 0, 1,
            "Kill 4 skeletons\nSpeak with Stuard at the entrance of the Swamp when you finished the quest.",
            "Mata 4 esqueletos\nHabla con Stuard en la entrada del pantano cuando hayas finalizado la misión.",
            "Mata 4 esquelets\nParla amb Stuard a la entrada del pantà quan finalitzis la misió.", false, 0, 4)
        };

    }

}
