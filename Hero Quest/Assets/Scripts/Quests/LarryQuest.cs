﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Misión 3 - Llevarle 4 setas a Larry
public class LarryQuest : Quest
{

    public LarryQuest()
    {

        idQuest = 3;

        EnTitle = "La necesidad de Larry";
        EnDescription = "Everybody needs to eat, Larry has run out of mushrooms, take him 4 mushrooms. The bandits should have some of them or maybe are in the way.";

        EsTitle = "La necesidad de Larry";
        EsDescription = "Todo el mundo necesita comer, larry se ha quedado sin setas, llévale 4 setas. Los bandidos deberian tener algunas o quizá haya por el camino.";

        CaTitle = "La plaga no morta";
        CaDescription = "Tot el mòn necessita menjar, Larry s'ha quedat sense bolets, porta-li 4 bolets.Els bandits haurien de tindre alguns, o potser hi ha pel camí.";

        ExperienceReward = 100;

        ItemsReward = new List<ItemInfo>();

        idQuests = new List<int>
        {
            2
        };

        Objectives = new List<Objective>
        {
            new CollectionObjective(this.idQuest, 0, 1,
            "Bring 4 mushrooms to Larry.\nSpeak with Larry on the house of the lake when you finished the quest.",
            "Entrégale 4 champiñones a Larry.\nHabla con Larry en la casa del lago cuando hayas completado la misión.",
            "Porta 4 bolets a Larry.\nParla amb Larry a la casa del llac quan hagis finalitzat la missió.", false, 0, 4)
        };

    }

}
