﻿using System;

// Proporciona misiones
public static class QuestFactory
{
    // Proporciona una misión concreta mediante el nombre de la clase de esta.
    public static object GetQuestByName(string strTypeClass)
    {
        Type t = Type.GetType(strTypeClass);
        return Activator.CreateInstance(t);
    }

}
