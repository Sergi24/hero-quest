﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

// Controla el slot de un item del inventario
public class InventorySlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    // Imagen del item
    public Image icon;
    // Boton de borrar el item
    public Button removeButton;
    // ScriptableObject del item
    private Item item;  // Current item in the slot

    // Controlador de muestreo de información del item
    public DisplayItemInfo displayItemInfo;

    // Añade el item al slot.
    public void AddItem(Item newItem)
    {

        item = newItem;

        icon.enabled = true;
        icon.sprite = item.icon;
        removeButton.interactable = true;
    }

    // Limpia el slot.
    public void ClearSlot()
    {
        item = null;

        icon.sprite = null;
        icon.enabled = false;
        removeButton.interactable = false;
    }

    // Borra el item e informa de ello al EventManager
    public void RemoveItemFromInventory()
    {
        Debug.Log("RemoveItem()");
        InventoryManager.instance.Remove(item);
    }

    // Utiliza el item
    public void UseItem()
    {
        if (item != null)
        {
            item.Use();
        }
    }

    // Muestra la información del item si se pone el ratón encima.
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (item != null)
        {
            displayItemInfo.gameObject.SetActive(true);
            displayItemInfo.ShowItemInfo(item);
        }
    }

    // Esconde la información del item si se mueve fuera el mouse.
    public void OnPointerExit(PointerEventData eventData)
    {
        displayItemInfo.gameObject.SetActive(false);
    }

}
