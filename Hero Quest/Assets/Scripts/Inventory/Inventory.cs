﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
[SerializeField]
public class Inventory
{
    // Numero de espacios del inventario.
    public int space = 20;

    // Lista de items del inventario.
    public List<Item> items = new List<Item>();

    public Inventory()
    {
        // Inicializamos la lista de items.
        items = new List<Item>();
        this.space = 20;
    }
}
