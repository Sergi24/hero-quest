﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New QuestItem", menuName = "Items/QuestItem")]
// ScriptableObject con un item de misión, no tiene nada importante, solo es un item de misión.
public class QuestItem : Item
{
    // Usa el item de misión, al no poderse usar no ocurre nada.
    public override void Use()
    {
        base.Use();

        Debug.Log("You can't use a quest item.");
    }

}
