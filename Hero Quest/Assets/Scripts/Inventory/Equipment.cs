﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Equipment", menuName = "Items/Equipment")]
public class Equipment : Item
{
    // Slot en el que se equipa el item equipable
    public EquipmentSlot equipSlot;

    // Modificadores de estats
    public int maxHealthModifier;
    public int maxManaModifier;

    public int strenghModifier;
    public int agilityModifier;
    public int intelligenceModifier;

    public int lifeRegenerationModifier;
    public int manaRegenerationModifier;

    public int armorModifier;
    public int damageModifier;

    // Equipa el item equipable
    public override void Use()
    {
        base.Use();
        // Lo equipa
        EquipmentManager.instance.Equip(this);
        // Lo borra del inventario (Está equipado no en el inventario)
        RemoveFromInventory();
    }

}

