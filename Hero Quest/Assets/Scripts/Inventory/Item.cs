﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Lean.Localization;

[Serializable]
[SerializeField]
public class Item : ScriptableObject
{
    // id del item
    public int idItem;

    // traducción de nombres del item
    public string enName = "New Item";
    public string esName = "Nuevo objeto";
    public string caName = "Nou objecte";

    // Traducción de descripcciones del item.
    [TextArea]
    public string enDescription;
    [TextArea]
    public string esDescription;
    [TextArea]
    public string caDescription;

    // Imagen del item en el inventario.
    public Sprite icon = null;
    // Si se muestra o no en el inventario.
    public bool showInInventory = true;

    // Prefab físico del item
    public GameObject prefab;

    // Usa el item
    public virtual void Use()
    {

        Debug.Log("Using " + name);
    }

    // Borra el item del inventario.
    public void RemoveFromInventory()
    {
        InventoryManager.instance.Remove(this);
    }

    // Obtiene la traducción actual del nombre del item.
    public string GetNameByLanguage()
    {
        string language = LeanLocalization.CurrentLanguage;

        if (language.Equals("English"))
            return enName;
        else if (language.Equals("Spanish"))
            return esName;
        else if (language.Equals("Catalan"))
            return caName;

        return null;
    }

    // Obtiene la traducción actual de la descripción del item.
    public string GetDescriptionByLanguage()
    {
        string language = LeanLocalization.CurrentLanguage;

        if (language.Equals("English"))
            return enDescription;
        else if (language.Equals("Spanish"))
            return esDescription;
        else if (language.Equals("Catalan"))
            return caDescription;

        return null;
    }

}
