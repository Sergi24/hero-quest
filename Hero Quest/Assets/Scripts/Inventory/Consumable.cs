﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Consumable", menuName = "Items/Consumable")]
// Tipo de ScriptableObject Item que al consumirse produce un efecto.
public class Consumable : Item
{
    public int healAmount;
    public int manaAmount;

    // Buffs, debuffs
    // Consume el item.
    public override void Use()
    {
        base.Use();
        // Busca al jugador y consume el item.
        PlayerStats playerStats = FindObjectOfType<PlayerStats>();
        playerStats.Consume(this);

        Debug.Log("Drink potion: " + healAmount + "HP, " + manaAmount + "MP");
        // Borra el item del inventario
        RemoveFromInventory();
    }

}
