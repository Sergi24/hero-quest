﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

/* This object manages the inventory UI. */

public class InventoryUIController : MonoBehaviour
{
    // Referencia a la ventana del inventario
    public GameObject inventoryUI;
    // Referencia a la ventanita de muestra de información de item.
    public GameObject showItemStats;

    // Referencia al padre de todos los itemSlots.
    public Transform itemsParent;

    // ItemSlots
    private InventorySlot[] slots;

    private void Awake()
    {
        // Obtenemos todos los slots del inventario.
        slots = GetComponentsInChildren<InventorySlot>(true);
    }

    void Start()
    {
        // Añadimos el evento para que cuando un item cambie se actualize el muestreo del inventario.
        EventManager.OnItemChanged += UpdateUI;
    }

    // Muestra el inventario.
    public void ShowInventory()
    {
        if (inventoryUI.activeSelf)
        {
            inventoryUI.SetActive(false);
            showItemStats.SetActive(false);
            GameManager.instance.LockMouse(false);
        }
        else
        {
            inventoryUI.SetActive(true);
            GameManager.instance.LockMouse(true);
        }

        GameManager.instance.freeLockCam.enabled = !GameManager.instance.freeLockCam.enabled;

        InputPlayerController inputPlayerController = FindObjectOfType<InputPlayerController>();

        inputPlayerController.canMoveCharacter = !inputPlayerController.canMoveCharacter;

        UpdateUI(null);
    }

    // Actualiza el inventario
    public void UpdateUI(Item item)
    {
        Inventory inventory = InventoryManager.instance.inventory;

        for (int i = 0; i < slots.Length; i++)
        {
            if (i < inventory.items.Count)
            {
                slots[i].AddItem(inventory.items[i]);
            }
            else
            {
                slots[i].ClearSlot();
            }
        }
    }


    private void OnDestroy()
    {
        // Desactiva el EventManager para que no avise a una clase destruida.
        EventManager.OnItemChanged -= UpdateUI;
    }

}
