﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Controla que el efecto mágico de un arma solo se aplique al momento de atacar (no usado)
public class MagicWeaponEffectController : MonoBehaviour
{
    public PlayerAnimationController playerController;
    public GameObject pSystem;

    public void Update()
    {
        if (playerController.IsAttacking())
            pSystem.SetActive(true);
        else
            pSystem.SetActive(false);


    }

}
