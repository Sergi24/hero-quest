﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Controla la rotación de nubes del pueblo
public class RotateClouds : MonoBehaviour
{
    public float speed;

    private void Update()
    {
        transform.Rotate(Vector3.up * Time.deltaTime * speed);
    }

}
