﻿using UnityEngine;
using UnityEditor;
public class EditorShortCutKeys : ScriptableObject
{
    [MenuItem("GameObject/Deselect all %#D")]
    // Deselecciona todo gameobject del escenario, muy útil si hay muchos gameobjects.
    static void DeselectAll()
    {
        Selection.objects = new Object[0];
    }
}