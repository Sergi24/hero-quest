﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lean.Localization;

public class InputToArg : MonoBehaviour
{

    public InputField inputField;

    [Tooltip("The LeanLocalizedTextArgs we will send arguments to")]
    public LeanLocalizedTextArgs LocalizedTextArgs;

    protected void Awake()
    {
        LocalizedTextArgs.SetArg("{0}", 0);
        LocalizedTextArgs.SetArg("{1}", 1);
    }

    public void Update()
    {
        if (LocalizedTextArgs != null && Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
        {
            LocalizedTextArgs.SetArg(inputField.text, 0);
            LocalizedTextArgs.SetArg(inputField.text, 1);
        }
    }

}
